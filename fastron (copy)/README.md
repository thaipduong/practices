**Implement Fastron in Python**

1) Implement Fastron with linear kernel and gaussian kernel with

     a) One-Step Weight Correction and Conditional Biasing
     
     b) Margin-Based Prioritization
     
     c) Redundant Support Point Removal

     Note: Active Learning section is not implemented as we will have new data from laser scan instead of actively relabeling the data points.

The Fastron paper can be found here: https://arxiv.org/abs/1709.02316

2) Tested on linear data and data from environment with obstacles. Data points can be generated as a grid or randomly

3) Visualize the environment, the support points and the boundary.

**Command**

*python main.py*

**Examples**

**Note that Fastron with linear kernel can't find a good boundary for inseparable data (e.g. environments with obstacles) as expected while Gaussian kernel can**

**Fastron with linear kernel + grid data points**

![picture](figures/fastron1.png)

![picture](figures/fastron2.png)

![picture](figures/fastron3.png)

**Fastron with gaussian kernel + grid data points**

![picture](figures/fastron4.png)

![picture](figures/fastron5.png)

Environment changes and Fastron is able to adapt to the new obstacles.

![picture](figures/fastron6.png)

![picture](figures/fastron13.png)

**Fastron with linear kernel + random data points**

![picture](figures/fastron7.png)

![picture](figures/fastron8.png)

![picture](figures/fastron9.png)

**Fastron with gaussian kernel + random data points**

![picture](figures/fastron10.png)

![picture](figures/fastron11.png)

Environment changes and Fastron is able to adapt to the new obstacles.

![picture](figures/fastron12.png)
