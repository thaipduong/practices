import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np
#
# This module generates figures describing the environment and path returned by A*
#


#
# Plot grid environment
# white = free, black = blocked, blue = start, red = goal, green = path, yellow = frontier, magenta = visited
#
def plot_grid(grid, start = None, goal = None, history = None, fig = None, ax = None):
    if fig is None or ax is None:
        fig, ax = plt.subplots()
    # create discrete colormap
    #cmap = colors.ListedColormap(['white', 'black', 'blue', 'red', 'yellow', 'magenta', 'green'])
    #bounds = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5]
    #norm = colors.BoundaryNorm(bounds, cmap.N)

    print(grid)
    norm = colors.Normalize(vmin=-0, vmax=1)
    ax.imshow(grid, cmap='gray_r', norm=norm)
    ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=2)
    ax.set_xticks(np.arange(-0.5, grid.shape[0], 1));
    ax.set_yticks(np.arange(-0.5, grid.shape[1], 1));

    if start is not None and goal is not None:
        current = goal
        while current is not None and current != start:
            current = current.parent
            if current is not None and grid[current.x, current.y] != 1:
                plt.plot(current.x, current.y, marker='o', markersize=3, color="b")
        ax.plot(goal.x, goal.y, marker='o', markersize=3, color="r")
        ax.plot(start.x, start.y, marker='o', markersize=3, color="r")

    if history is not None:
        for node in history:
            ax.plot(node.x, node.y, marker='^', markersize=6, color="g")




#
# Plot grid environment by assigning value according to color map in plot_grid() above.
#
def viz_path(grid, graph, start, end):
    new_grid = np.zeros(grid.shape)
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            new_grid[i, j] = graph[j][i].blocked_prob
            #if graph[i][j].inqueue and grid[i, j] != 1:
            #    new_grid[i, j] = 0.3
            #if graph[i][j].visited and grid[i, j] != 1:
            #    grid[i, j] = 5

    current = end
    while current != start and current is not None:
        current = current.parent
        if current is not None:
            print(current.x, current.y)

    return new_grid
