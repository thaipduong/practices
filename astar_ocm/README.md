**Occupancy Grid Mapping and Replanning**

1) Randomly generate grid environment with obstacles (blocked = black, free = white)

2) Update grid map using observation

3) Run A* to replan the shortest path between start and goal cell based on current occupancy grid map.

4) Jump to next node. Go to step 2.

5) Visualize the grid map, A* path at each time step.

**Command**

*python main.py*

**Examples**

![picture](figures/groundtruth.png)

![picture](figures/step1.png)

![picture](figures/step2.png)

![picture](figures/step4.png)

![picture](figures/step7.png)

![picture](figures/step10.png)

![picture](figures/step12.png)
