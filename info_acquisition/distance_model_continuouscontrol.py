import numpy as np
import viz
import pylab as mp
import matplotlib.pyplot as plt


def observation(x, y):
    return np.linalg.norm((x-y).flatten()) + 0.1*np.random.rand(1)

def target_transition(mu):
    # Target doesn't move
    G = np.identity(len(mu))
    # No noise
    R = np.zeros(len(mu)).reshape(len(mu), 1)
    return G, R

def linearized_observation(x, mu_y):
    h_mu = np.linalg.norm((x-mu_y).flatten())
    H = (1/(np.linalg.norm((mu_y - x).flatten())))*(mu_y - x).reshape(1,2)
    Q = 0.1
    return H, h_mu, Q

def dlogdet_dx(idx, x, mu_y, cov_y, H, Q):
    dHdx = dH_dx(mu_y, x)
    print("dHdx[idx].reshape(1,2) = " + str(dHdx[idx].reshape(1,2)))
    H_transpose = H.reshape(len(mu_y), 1)
    M = np.linalg.inv(np.matmul(H, np.matmul(cov_y, H_transpose)) + Q)
    print("M = " + str(M))
    dMdx = dM_dx(idx, x, mu_y, cov_y, H, Q)
    term1 = np.matmul(np.matmul(dHdx[idx].reshape(2,1), M), H)
    print("term1 = " + str(term1))
    term2 = np.matmul(H_transpose, np.matmul(dMdx, H))
    print("term2 = " + str(term2))
    term3 = np.matmul(np.matmul(H_transpose,M), dHdx[idx].reshape(1,2))
    print("term3 = " + str(term3))
    dCovdx = (-1)*np.matmul(cov_y,np.matmul(term1 + term2 + term3, cov_y))
    print("dCovdx = " + str(dCovdx))

    dLDdx = np.trace(np.matmul(np.linalg.inv(cov_y),dCovdx))
    print("dLDdx = " + str(dLDdx))
    return dLDdx

# Derivative of (H*cov_y*H^T + Q)^-1
def dM_dx(idx, x, mu_y, cov_y, H, Q):
    H_transpose = H.reshape(len(mu_y), 1)
    #M = np.linalg.inv(np.matmul(H, np.matmul(cov_y, H_transpose)) + Q)
    M_inv = np.matmul(H, np.matmul(cov_y, H_transpose)) + Q
    dHdx = dH_dx(mu_y, x)
    temp1 = np.matmul(np.matmul(dHdx[idx], cov_y), H_transpose)
    temp2 = np.matmul(np.matmul(H, cov_y),dHdx[idx].reshape(2,1))
    dMdx = (-1)*np.matmul(M_inv,np.matmul((temp1 + temp2),M_inv))
    print("dMdx = " + str(dMdx))
    return dMdx


def dH_dx(mu_y, x):
    distance = np.linalg.norm((mu_y - x).flatten())
    diff = mu_y-x
    diff_transpose = diff.reshape(1,2)
    print(diff)
    print(diff_transpose)
    dH =  (1/(distance**3))*np.matmul(diff, diff_transpose)-(1/distance)*np.identity(2)
    print("dHdx = " + str(dH))
    return dH




def ekf(x, y, mu_y, cov_y):
    # Real target
    #y = np.array([1, 2]).reshape(2, 1)
    # mu and cov at time t+1

    z = observation(x, y)

    print("Observation")
    print(x)
    print(y)
    print((x-y).flatten())
    print(z)

    G,R = target_transition(mu_y)
    H, h_mu, Q = linearized_observation(x, mu_y)
    H_transpose = H.reshape(len(mu_y),1)

    print("Linearization:")
    print(H)
    print(H_transpose)
    print(h_mu)

    mu_y_predicted = np.matmul(G, mu_y) + R
    cov_y_predicted = np.matmul(G, np.matmul(cov_y, np.transpose(G))) + R

    print("Predicted")
    print(mu_y_predicted)
    print(cov_y_predicted)

    temp = np.linalg.inv(np.matmul(H, np.matmul(cov_y_predicted, H_transpose)) + Q)

    K =np.matmul(cov_y_predicted, np.matmul(H_transpose, temp))

    print("K gain:")
    print(K)
    mu_y_updated = mu_y_predicted + np.matmul(K, z-h_mu).reshape(len(mu_y), 1)
    cov_y_updated = np.matmul(np.identity(len(mu_y)) - np.matmul(K, H), cov_y_predicted)


    print("Updated:")
    print(mu_y_updated)
    print(cov_y_updated)
    return mu_y_updated, cov_y_updated



fig = mp.figure()
ax = fig.add_subplot(111,aspect='equal')
ax.set_xlabel('x axis',fontsize=20)
ax.set_ylabel('y axis',fontsize=20)
ax.set_xlim(-5,5)
ax.set_ylim(-2,6)
ax.grid(True)


color = ['r', 'b', 'g', 'm', 'c']

x = np.array([0, 0]).reshape(2, 1)
y = np.array([0, 5]).reshape(2, 1)
mu_y = np.array([0, 4]).reshape(2, 1)
cov_y = np.array([[4, 1], [1, 9]])

for i in range(5):
    x = x.reshape(2,1)
    y = y.reshape(2,1)
    mu_y = mu_y.reshape(2,1)

    mu_y_updated, cov_y_updated = ekf(x,y,mu_y,cov_y)

    H, h_mu, Q = linearized_observation(x, mu_y)
    dLD_dx0 = dlogdet_dx(0, x, mu_y_updated, cov_y_updated, H, Q)
    dLD_dx1 = dlogdet_dx(1, x, mu_y_updated, cov_y_updated, H, Q)
    #print("dLD_dx0 = " + str(dLD_dx0))
    #print("dLD_dx1 = " + str(dLD_dx1))
    print("mu_y_updated = " + str(mu_y_updated))
    print("cov_y_updated = " + str(cov_y_updated))
    # min problem, x1 = x0 -delta(f(x))
    delta_x = -np.array([[dLD_dx0], [dLD_dx1]])
    print("delta_x = " + str(delta_x))
    if np.linalg.norm(delta_x) > 0:
        delta_x = delta_x/np.linalg.norm(delta_x)
    print("delta_x = " + str(delta_x))

    # mu and cov at time t
    plot_kwargs = {'color':color[i],'linestyle':'-','linewidth':3,'alpha':0.5}
    fill_kwargs = {'color':color[i],'alpha':0.3}
    viz.plot_ellipse(x_cent=mu_y[0], y_cent=mu_y_updated[1], cov=cov_y_updated, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)

    print("dot product:" + str(np.inner(delta_x.flatten(),(x-mu_y_updated).flatten())))
    x = x.flatten()
    delta_x = delta_x.flatten()
    plt.plot(x[0], x[1], marker='o', markersize=6, color="k")
    #plt.plot(x[0] + delta_x[0], x[1] + delta_x[1], marker='o', markersize=6, color="k")
    plt.arrow(x[0], x[1], delta_x[0], delta_x[1], head_width=0.3, head_length=0.5, fc=color[i], ec=color[i])

    #Next point:
    x = x + delta_x
    mu_y = mu_y_updated
    cov_y = cov_y_updated







mp.show()