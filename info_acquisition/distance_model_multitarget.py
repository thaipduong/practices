import numpy as np
import viz
import pylab as mp
import matplotlib.pyplot as plt


def observation(x, y):
    return np.linalg.norm(x-y) + 0.1*np.random.rand(1)

def target_transition(mu):
    # Target doesn't move
    G = np.identity(len(mu))
    # No noise
    R = np.zeros(len(mu)).reshape(len(mu), 1)
    return G, R

def linearized_observation(x, mu_y):
    h_mu = np.linalg.norm(x-mu_y)
    H = (1/(observation(x, mu_y)))*(mu_y - x).reshape(1,2)
    Q = 0.1
    return H, h_mu, Q


def ekf(x, y, mu_y, cov_y):
    # Real target
    #y = np.array([1, 2]).reshape(2, 1)
    # mu and cov at time t+1

    z = observation(x, y)

    print("Observation")
    print(x)
    print(y)
    print((x-y).flatten())
    print(z)

    G,R = target_transition(mu_y)
    H, h_mu, Q = linearized_observation(x, mu_y)
    H_transpose = H.reshape(len(mu_y),1)

    print("Linearization:")
    print(H)
    print(H_transpose)
    print(h_mu)

    mu_y_predicted = np.matmul(G, mu_y) + R
    cov_y_predicted = np.matmul(G, np.matmul(cov_y, np.transpose(G))) + R

    print("Predicted")
    print(mu_y_predicted)
    print(cov_y_predicted)

    temp = np.linalg.inv(np.matmul(H, np.matmul(cov_y_predicted, H_transpose)) + Q)

    K =np.matmul(cov_y_predicted, np.matmul(H_transpose, temp))

    print("K gain:")
    print(K)
    mu_y_updated = mu_y_predicted + np.matmul(K, z-h_mu).reshape(len(mu_y), 1)
    cov_y_updated = np.matmul(np.identity(len(mu_y)) - np.matmul(K, H), cov_y_predicted)


    print("Updated:")
    print(mu_y_updated)
    print(cov_y_updated)
    return mu_y_updated, cov_y_updated


target_count = 3
y_array = np.array([[0, 5], [6,1], [0, -4]])
mu_y_array = np.array([[-1, 4], [5,-1], [1, -5]])
cov_y_array = np.array([[[2, 1], [1, 4]],
               [[1, 0], [0, 3]],
               [[3, 0], [0, 1]]])
fig = mp.figure()
ax = fig.add_subplot(111, aspect='equal')
ax.set_xlabel('x axis', fontsize=20)
ax.set_ylabel('y axis', fontsize=20)
ax.grid(True)
logdet_sum = np.array([0, 0, 0, 0])
color_action = np.array(['b', 'y', 'm', 'g'])
step = 2
x = np.array([0, 0]).reshape(2, 1)
plt.plot(x[0], x[1], marker='o', markersize=6, color="k")
x_array = np.array([[x[0] + step, x[1]],
                    [x[0] - step, x[1]],
                    [x[0], x[1] + step],
                    [x[0], x[1] - step]])
for i in range(target_count):
    # Target
    y = y_array[i].reshape(2,1)
    mu_y = mu_y_array[i].reshape(2,1)
    cov_y = cov_y_array[i]
    print("Original:")
    print(mu_y)
    print(cov_y)
    #viz.plot_ellipse(x_cent=y[0], y_cent=y[1], cov=cov_y)




    # mu and cov at time t
    plot_kwargs = {'color':'r','linestyle':'-','linewidth':3,'alpha':0.5}
    fill_kwargs = {'color':'r','alpha':0.3}
    viz.plot_ellipse(x_cent=mu_y[0], y_cent=mu_y[1], cov=cov_y, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)

    x1 = x_array[0].reshape(2, 1)
    x2 = x_array[1].reshape(2, 1)
    x3 = x_array[2].reshape(2, 1)
    x4 = x_array[3].reshape(2, 1)
    mu_y_updated1, cov_y_updated1 = ekf(x1, y, mu_y, cov_y)
    mu_y_updated2, cov_y_updated2 = ekf(x2, y, mu_y, cov_y)
    mu_y_updated3, cov_y_updated3 = ekf(x3, y, mu_y, cov_y)
    mu_y_updated4, cov_y_updated4 = ekf(x4, y, mu_y, cov_y)
    plot_kwargs = {'color':'b','linestyle':'-','linewidth':3,'alpha':0.8}
    fill_kwargs = {'color':'b','alpha':0.7}
    viz.plot_ellipse(x_cent=mu_y_updated1[0], y_cent=mu_y_updated1[1], cov=cov_y_updated1, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)
    plt.plot(x1[0], x1[1], marker='o', markersize=6, color="b")

    plot_kwargs = {'color':'y','linestyle':'-','linewidth':3,'alpha':0.8}
    fill_kwargs = {'color':'y','alpha':0.7}
    viz.plot_ellipse(x_cent=mu_y_updated2[0], y_cent=mu_y_updated2[1], cov=cov_y_updated2, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)
    plt.plot(x2[0], x2[1], marker='o', markersize=6, color="y")

    plot_kwargs = {'color':'m','linestyle':'-','linewidth':3,'alpha':0.8}
    fill_kwargs = {'color':'m','alpha':0.7}
    viz.plot_ellipse(x_cent=mu_y_updated3[0], y_cent=mu_y_updated3[1], cov=cov_y_updated3, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)
    plt.plot(x3[0], x3[1], marker='o', markersize=6, color="m")

    plot_kwargs = {'color':'g','linestyle':'-','linewidth':3,'alpha':0.8}
    fill_kwargs = {'color':'g','alpha':0.7}
    viz.plot_ellipse(x_cent=mu_y_updated4[0], y_cent=mu_y_updated4[1], cov=cov_y_updated4, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)
    plt.plot(x4[0], x4[1], marker='o', markersize=6, color="g")
    logdet = np.array([np.linalg.slogdet(cov_y_updated1),
                      np.linalg.slogdet(cov_y_updated2),
                      np.linalg.slogdet(cov_y_updated3),
                      np.linalg.slogdet(cov_y_updated4)])

    logdet = logdet[:,1]

    logdet_sum = logdet_sum + logdet
    #np.concatenate(logdet_array, logdet)

print("logdet = " + str(logdet_sum))
idx = np.argmin(logdet_sum)
print("argmin: " + str(x_array[idx].flatten()))
#plt.plot(x_array[idx], x, marker = 'o', color=color_action[idx])
plt.arrow(x[0,0], x[1,0], x_array[idx,0,0] - x[0,0] +0.5, x_array[idx,1,0] - x[1,0], head_width=0.3, head_length=0.5, fc='k', ec='k')
mp.show()