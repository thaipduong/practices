import numpy as np
import numpy.linalg as linalg
import matplotlib.pyplot as plt
import pylab as mp
from mpl_toolkits.mplot3d import Axes3D

# your ellispsoid and center in matrix form

# plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

def plot_ellipsoid(x_cent=0, y_cent=0, z_cent=0, cov=None, color='b'):
    center = [x_cent, y_cent, z_cent]
    # find the rotation matrix and radii of the axes
    U, s, rotation = linalg.svd(cov)
    radii = 1.0/np.sqrt(s)

    # now carry on with EOL's answer
    u = np.linspace(0.0, 2.0 * np.pi, 100)
    v = np.linspace(0.0, np.pi, 100)
    x = radii[0] * np.outer(np.cos(u), np.sin(v))
    y = radii[1] * np.outer(np.sin(u), np.sin(v))
    z = radii[2] * np.outer(np.ones_like(u), np.cos(v))
    for i in range(len(x)):
        for j in range(len(x)):
            [x[i,j],y[i,j],z[i,j]] = np.dot([x[i,j],y[i,j],z[i,j]], rotation) + center
    ax.plot_wireframe(x,y,z, rstride=4, cstride=4, color=color, alpha=0.2)


#A = np.array([[1,0,0],[0,2,0],[0,0,2]])
#B = np.array([[1,0,0],[0,1,0],[0,0,1]])
#center = [0,0,0]
#plot_ellipsoid(0,0,0, A, color='b')
#plot_ellipsoid(0,0,0, B, color='r')

#mp.show()