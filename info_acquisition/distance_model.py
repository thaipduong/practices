import numpy as np
import viz
import pylab as mp


def observation(x, y):
    return np.linalg.norm((x-y).flatten()) + 0.1*np.random.rand(1)

def target_transition(mu):
    # Target doesn't move
    G = np.identity(len(mu))
    # No noise
    R = np.zeros(len(mu)).reshape(len(mu), 1)
    return G, R

def linearized_observation(x, mu_y):
    h_mu = np.linalg.norm((x - mu_y).flatten())
    H = (1/(observation(x, mu_y)))*(mu_y - x).reshape(1,2)
    Q = 0.1
    return H, h_mu, Q


def dlogdet_dx(idx, x, mu_y, cov_y, H, Q):
    dHdx = dH_dx(mu_y, x)
    H_transpose = H.reshape(len(mu_y), 1)
    M = np.linalg.inv(np.matmul(H, np.matmul(cov_y, H_transpose)) + Q)
    dMdx = dM_dx(idx, x, mu_y, cov_y, H, Q)
    term1 = np.matmul(np.matmul(dHdx[idx].reshape(2,1), M), H)
    print("term1 = " + str(term1))
    term2 = np.matmul(H_transpose, np.matmul(dMdx, H))
    print("term2 = " + str(term2))
    term3 = np.matmul(np.matmul(H_transpose,M), dHdx[idx])
    print("term3 = " + str(term3))
    dCovdx = -np.matmul(cov_y,np.matmul(term1 + term2 + term3, cov_y))
    print("dCovdx = " + str(dCovdx))
    dLDdx = np.trace(np.matmul(np.linalg.inv(cov_y),dCovdx))
    print("dLDdx = " + str(dLDdx))
    return dLDdx

# Derivative of (H*cov_y*H^T + Q)^-1
def dM_dx(idx, x, mu_y, cov_y, H, Q):
    H_transpose = H.reshape(len(mu_y), 1)
    M = np.linalg.inv(np.matmul(H, np.matmul(cov_y, H_transpose)) + Q)
    M_inv = np.linalg.inv(M)
    dHdx = dH_dx(mu_y, x)
    temp1 = np.matmul(np.matmul(dHdx[idx], cov_y), H_transpose)
    temp2 = np.matmul(H_transpose, cov_y),np.matmul(dHdx[idx] )
    dMdx = (-1)*np.matmul(M_inv,np.matmul((temp1 + temp2),M_inv))
    print("dMdx = " + str(dMdx))
    return dMdx


def dH_dx(mu_y, x):
    distance = np.linalg.norm((mu_y - x).flatten())
    diff = mu_y-x
    diff_transpose = diff.reshape(1,2)
    print(diff)
    print(diff_transpose)
    dH =  (1/(distance**3))*np.matmul(diff, diff_transpose)-(1/distance)*np.identity(2)
    print("dHdx = " + str(dH))
    return dH

# Real target
y = np.array([1,2]).reshape(2,1)
# Target
mu_y = np.array([2,4]).reshape(2,1)
cov_y = np.array([[2, 1], [1, 4]])
print("Original:")
print(mu_y)
print(cov_y)
#viz.plot_ellipse(x_cent=y[0], y_cent=y[1], cov=cov_y)

fig = mp.figure()
ax = fig.add_subplot(111,aspect='equal')
ax.set_xlabel('x axis',fontsize=20)
ax.set_ylabel('y axis',fontsize=20)
ax.grid(True)

# mu and cov at time t
plot_kwargs = {'color':'r','linestyle':'-','linewidth':3,'alpha':0.5}
fill_kwargs = {'color':'r','alpha':0.3}
viz.plot_ellipse(x_cent=mu_y[0], y_cent=mu_y[1], cov=cov_y, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)

# mu and cov at time t+1
x = np.array([5,3]).reshape(2, 1)
z = observation(x, y)

print("Observation")
print(x)
print(y)
print((x-y).flatten())
print(z)

G,R = target_transition(mu_y)
H, h_mu, Q = linearized_observation(x, mu_y)
H_transpose = H.reshape(len(mu_y),1)

print("Linearization:")
print(H)
print(H_transpose)
print(h_mu)

mu_y_predicted = np.matmul(G, mu_y) + R
cov_y_predicted = np.matmul(G, np.matmul(cov_y, np.transpose(G))) + R

print("Predicted")
print(mu_y_predicted)
print(cov_y_predicted)

temp = np.linalg.inv(np.matmul(H, np.matmul(cov_y_predicted, H_transpose)) + Q)

K =np.matmul(cov_y_predicted, np.matmul(H_transpose, temp))

print("K gain:")
print(K)
mu_y_updated = mu_y_predicted + np.matmul(K, z-h_mu).reshape(len(mu_y), 1)
cov_y_updated = np.matmul(np.identity(len(mu_y)) - np.matmul(K, H), cov_y_predicted)


print("Updated:")
print(mu_y_updated)
print(cov_y_updated)


plot_kwargs = {'color':'b','linestyle':'-','linewidth':3,'alpha':0.8}
fill_kwargs = {'color':'b','alpha':0.7}
viz.plot_ellipse(x_cent=mu_y_updated[0], y_cent=mu_y_updated[1], cov=cov_y_updated, ax=ax,plot_kwargs=plot_kwargs,fill=True,fill_kwargs=fill_kwargs)

mp.show()