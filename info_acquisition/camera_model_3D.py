import numpy as np
import viz3D
import pylab as mp

K = np.array([[5, 0, 320],
             [0, 0, 240],
             [0, 0, 1]])
dim = 3

def dpi_dx(x):
    x = x.flatten()
    return (1/(x[2]**2))*np.array([[x[2], 0, -x[0]],
                                  [0, x[2], -x[1]],
                                  [0, 0, 0]])

def pi_x(x):
    return (1/x[2])*x

def rand_R():
    yaw = 2 * np.pi * np.random.rand(1)
    print("yaw = " + str(yaw))
    pitch = 2 * np.pi * np.random.rand(1)
    print("pitch = " + str(pitch))
    roll = 2 * np.pi * np.random.rand(1)
    print("roll = " + str(roll))

    R_yaw = [[np.cos(yaw), -np.sin(yaw), 0],
             [np.sin(yaw), np.cos(yaw), 0],
             [0, 0, 1]]

    R_pitch = [[np.cos(pitch), 0, np.sin(pitch)],
               [0, 1, 0],
               [-np.sin(pitch), 0, np.cos(pitch)]]

    R_roll = [[1, 0, 0],
              [0, np.cos(roll), -np.sin(roll)],
              [0, np.sin(roll), np.cos(roll)]]

    R = np.matmul(R_yaw, np.matmul(R_pitch, R_roll))
    return R

def observation(p, R, y):
    #print("pi_x")
    #print(np.matmul(np.transpose(R),y-p))
    #print(pi_x(np.matmul(np.transpose(R),y-p)))
    #print(R)
    #print(y-p)
    z = np.matmul(K, pi_x(np.matmul(np.transpose(R),y-p)))
    z = z[0:2]
    #print("z_nonoise = " + str(z))
    noise = np.random.rand(len(z)).reshape(len(z), 1)
    #print("noise" + str(noise))
    return z + noise

def target_transition(mu):
    # Target doesn't move
    G = np.identity(len(mu))
    # No noise
    W = np.zeros(len(mu)).reshape(len(mu), 1)
    return G, W

def linearized_observation(p, R, mu_y):
    h_mu = observation(p, R, mu_y)
    temp = dpi_dx(np.matmul(np.transpose(R),mu_y-p))
    #print("h_mu = " + str(h_mu))
    H = np.matmul(K, np.matmul(temp,np.transpose(R)))
    H = H[0:2, 0:3]
    #print("H = " + str(H))
    Q = 1*np.identity(len(h_mu))
    #print("Q = " + str(Q))
    return H, h_mu, Q

# Real target
y = np.array([1,2, 3]).reshape(dim,1)
# Target
mu_y = np.array([2,4,1]).reshape(dim,1)
cov_y = np.array([[2, 0, 1], [0, 3, 0], [1, 0, 1]])
print("Original Estimate:")
print("mu_y = " + str(mu_y))
print("cov_y = " + str(cov_y))

# mu and cov at time t
center = mu_y.flatten()
viz3D.plot_ellipsoid(x_cent=center[0], y_cent=center[1], z_cent=center[2], cov=np.linalg.inv(cov_y), color='b')

R_cw = rand_R()
print("R_cw = " + str(R_cw))
R_oc = np.array([[0, -1, 0],
                [0, 0, -1],
                [1, 0, 0]])
R = np.matmul(R_cw,np.transpose(R_oc))
print("R = " + str(R))
# mu and cov at time t+1
p = np.array([5,3,2]).reshape(dim, 1)
z = observation(p, R, y)

print("Observation")
print("z = " + str(z))

G,W = target_transition(mu_y)
H, h_mu, Q = linearized_observation(p, R, mu_y)
H_transpose = np.transpose(H)

print("Linearization:")
print("H = " + str(H))
print("h(mu_y) = " + str(h_mu))

mu_y_predicted = np.matmul(G, mu_y) + W
cov_y_predicted = np.matmul(G, np.matmul(cov_y, np.transpose(G))) + W

print("Predicted")
print("mu_y_predicted = " + str(mu_y_predicted))
print("cov_y_predicted = " + str(cov_y_predicted))

temp = np.linalg.inv(np.matmul(H, np.matmul(cov_y_predicted, H_transpose)) + Q)
K =np.matmul(cov_y_predicted, np.matmul(H_transpose, temp))

print("K gain:")
print("K = " + str(K))
mu_y_updated = mu_y_predicted + np.matmul(K, z-h_mu).reshape(len(mu_y), 1)
cov_y_updated = np.matmul(np.identity(len(mu_y)) - np.matmul(K, H), cov_y_predicted)

print("Updated:")
print("mu_y_updated = " + str(mu_y_updated))
print("cov_y_updated= " + str(cov_y_updated))
print("eigenvalues: " + str(np.linalg.eigvals(cov_y_updated)))



center = mu_y_updated.flatten()
viz3D.plot_ellipsoid(x_cent=center[0], y_cent=center[1], z_cent=center[2], cov=np.linalg.inv(cov_y_updated), color='r')

mp.show()
