import numpy as np

yaw = 2*np.pi*np.random.rand(1)
print("yaw = " + str(yaw))
pitch = 2*np.pi*np.random.rand(1)
print("pitch = " + str(pitch))
roll = 2*np.pi*np.random.rand(1)
print("roll = " + str(roll))

R_yaw = [[np.cos(yaw), -np.sin(yaw), 0],
         [np.sin(yaw), np.cos(yaw), 0],
         [0, 0, 1]]

R_pitch = [[np.cos(pitch), 0, np.sin(pitch)],
           [0, 1, 0],
           [-np.sin(pitch), 0, np.cos(pitch)]]

R_roll = [[1, 0, 0],
          [0, np.cos(roll), -np.sin(roll)],
          [0, np.sin(roll), np.cos(roll)]]

R = np.matmul(R_yaw, np.matmul(R_pitch, R_roll))

print("R = " + str(R))

R_inv = np.linalg.inv(R)

print("R_inv = " + str(R_inv))

R_transpose = np.transpose(R)

print("R_transpose = " + str(R_transpose))

R_oc = [[0, -1, 0],
        [0, 0, -1],
        [1, 0, 0]]

R_ow = np.matmul(R_oc, np.transpose(R))
R_ow_inv = np.linalg.inv(R_ow)
R_ow_transpose = np.transpose(R_ow)

print("R_ow= " + str(R_ow))
print("R_ow_inv R = " + str(R_ow_inv))
print("R_ow_trans R = " + str(R_ow_transpose))