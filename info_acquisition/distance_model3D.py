import numpy as np
import viz3D
import pylab as mp

dim = 3

def observation(x, y):
    return np.linalg.norm((x-y).flatten()) + 0.1*np.random.rand(1)

def target_transition(mu):
    # Target doesn't move
    G = np.identity(dim)
    # No noise
    R = np.zeros(dim).reshape(dim, 1)
    return G, R

def linearized_observation(x, mu_y):
    h_mu = observation(x, mu_y)
    H = (1/(observation(x, mu_y)))*(mu_y - x).reshape(1,dim)
    Q = 0.1
    return H, h_mu, Q

# Real target
y = np.array([1,2, 3]).reshape(dim,1)
# Target
mu_y = np.array([2,4,1]).reshape(dim,1)
cov_y = np.array([[2, 0, 1], [0, 3, 0], [1, 0, 1]])
print("Original:")
print(mu_y)
print(cov_y)

# mu and cov at time t
center = mu_y.flatten()
viz3D.plot_ellipsoid(x_cent=center[0], y_cent=center[1], z_cent=center[2], cov=np.linalg.inv(cov_y), color='b')


# mu and cov at time t+1
x = np.array([5,3,2]).reshape(dim, 1)
z = observation(x, y)

print("Observation")
print(x)
print(y)
print((x-y).flatten())
print(z)

G,R = target_transition(mu_y)
H, h_mu, Q = linearized_observation(x, mu_y)
H_transpose = H.reshape(dim,1)

print("Linearization:")
print(H)
print(H_transpose)
print(h_mu)

mu_y_predicted = np.matmul(G, mu_y) + R
cov_y_predicted = np.matmul(G, np.matmul(cov_y, np.transpose(G))) + R

print("Predicted")
print(mu_y_predicted)
print(cov_y_predicted)

temp = np.linalg.inv(np.matmul(H, np.matmul(cov_y_predicted, H_transpose)) + Q)

K =np.matmul(cov_y_predicted, np.matmul(H_transpose, temp))

print("K gain:")
print(K)
mu_y_updated = mu_y_predicted + np.matmul(K, z-h_mu).reshape(len(mu_y), 1)
cov_y_updated = np.matmul(np.identity(len(mu_y)) - np.matmul(K, H), cov_y_predicted)


print("Updated:")
print(mu_y_updated)
print(cov_y_updated)


center = mu_y_updated.flatten()
viz3D.plot_ellipsoid(x_cent=center[0], y_cent=center[1], z_cent=center[2], cov=np.linalg.inv(cov_y_updated), color='r')

mp.show()
