from skrvm import RVR
X = [[0, 0], [2, 2]]
y = [0.5, 2.5 ]
clf = RVR(kernel='linear')
clf.fit(X, y)
RVR(alpha=1e-06, beta=1e-06, beta_fixed=False, bias_used=True, coef0=0.0,
coef1=None, degree=3, kernel='linear', n_iter=3000,
threshold_alpha=1000000000.0, tol=0.001, verbose=False)
print(clf.predict([[1, 1]]))

