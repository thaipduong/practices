import numpy as np
import datagen
import viz
from skrvm import RVC, RVC2, RVC3
# Parameters
# Environment Size
x_min = [-6, -6]
x_max = [6, 6]
# Training test size
data_points_count = 400
# Generate grid environment
grid = True
grid_size = [10, 10]
obs_list = np.array([[-4, -3, -4, 2], [-4, 0, 1, 3], [1, 5, 2, 4]])

X, Y = datagen.gen_obstacle_data(obs_list, x_min=x_min, x_max=x_max,
                                 data_points_count = data_points_count,
                                 grid = grid, grid_size = grid_size)

viz.plot_data(X,Y)

clf = RVC()
clf.fit(X, Y.flatten())
#print(clf.score(X,Y))
viz.plot_boundary(X,Y, clf)

Y2 = clf.predict(X)

viz.plot_data(X,Y2)