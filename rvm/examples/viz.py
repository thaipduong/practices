import matplotlib.pyplot as plt
import numpy as np

# Plotting decision boundary
def plot_data(X, Y, x_min= [-6, -6], x_max=[6, 6]):
    x0 = np.linspace(x_min[0], x_max[0])
    x1 = np.linspace(x_min[1], x_max[1])
    x0mesh, x1mesh = np.meshgrid(x0, x1)
    x0mesh_flattened = x0mesh.flatten()
    x1mesh_flattened = x1mesh.flatten()
    X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
    X_grid = np.transpose(X_grid)
    f, ax = plt.subplots(figsize=(5,5))
    #colors = ['red' if l == 1. else 'green' for l in Y]
    occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
    free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
    label = ['occupied', 'free']
    print(occupied.shape)

    ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
    ax.scatter(free[:, 0], free[:, 1], color="green", s=20, label="free")
    ax.legend(loc=4, framealpha=1)
    plt.show()


# Plotting decision boundary
def plot_boundary(X, Y, classifier, x_min= [-6, -6], x_max=[6, 6]):
    x0 = np.linspace(x_min[0], x_max[0], 200)
    x1 = np.linspace(x_min[1], x_max[1], 200)
    x0mesh, x1mesh = np.meshgrid(x0, x1)
    x0mesh_flattened = x0mesh.flatten()
    x1mesh_flattened = x1mesh.flatten()
    X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
    X_grid = np.transpose(X_grid)
    f, ax = plt.subplots(figsize=(5,5))
    #colors = ['red' if l == 1. else 'green' for l in Y]
    occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
    free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
    label = ['occupied', 'free']
    print(occupied.shape)
    #s = [0 if a == 0 else 60 for a in alpha]
    relevance = classifier.relevance_
    ax.scatter(relevance[:, 0], relevance[:, 1], color='b', s=60)

    ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
    ax.scatter(free[:, 0], free[:, 1], color="green", s=20, label="free")
    ax.legend(loc=4, framealpha=1)
    y_predict = classifier.predict_proba(X_grid)[:,1]
    CS = ax.contour(x0mesh, x1mesh,
               y_predict.reshape(x0mesh.shape), levels=[0.5])
    f2, ax2 = plt.subplots(figsize=(6, 5))
    y_predict2 = classifier.predict_proba(X_grid, mode=True)[:, 1]
    CS2 = ax2.pcolor(x0mesh, x1mesh,
                     y_predict2.reshape(x0mesh.shape))
    plt.colorbar(CS2)
    f3, ax3 = plt.subplots(figsize=(6, 5))
    CS3 = ax3.pcolor(x0mesh, x1mesh,
               y_predict.reshape(x0mesh.shape))
    plt.colorbar(CS3)
    #ax.clabel(CS, inline=1, fontsize=10)
    #print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
    #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
    plt.show()