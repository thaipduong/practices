"""skrvm implements RVM models."""

from .rvm import RVR, RVC, RVC2, RVC3

__all__ = ['RVR', 'RVC', 'RVC2', 'RVC3']

__version__ = '0.1.0a1'
