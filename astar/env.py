import numpy as np
#
# This module generates environment as a matrix and converts it into a graph of nodes accordingly.
#


#
# Generate grid environment randomly:
# 0 = free, 1 = blocked, 2 = start, 3 = goal
# The probability of a cell being blocked in p
#
def grid_env_gen(p, x_size, y_size, fixed_start_goal = True):
    data = np.random.choice([0, 1], size=(x_size, y_size), p=[1-p, p])
    if not fixed_start_goal:
        while True:
            x = np.random.randint(0, x_size)
            y = np.random.randint(0, y_size)
            if data[x, y] == 0:
                start_cell = [x, y]
                data[x, y] = 2
                break
        while True:
            x = np.random.randint(0, x_size)
            y = np.random.randint(0, y_size)
            if data[x, y] == 0:
                goal_cell = [x, y]
                data[x, y] = 3
                break
    else:
        data[0,0] = 2
        data[x_size - 1, y_size - 1] = 3
        start_cell = [0,0]
        goal_cell = [x_size - 1, y_size - 1]
    print(data)
    return data, start_cell, goal_cell

#
# class Node for cell. Each node contains information for A* to work.
#
class Node:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None
        self.neighbors = []
        self.visited = False
        self.inqueue = False
        self.g = float('inf')
        self.f = float('inf')
        self.blocked = False

    def cost(self, next_node):
        if self.neighbors.__contains__(next_node):
            return 1
        else:
            return float('inf')

    def h(self, goal):
        return np.sqrt((self.x - goal.x) ** 2 + (self.y - goal.y) ** 2)

    def __lt__(self, other):
        return self.f < other.f


#
# Generate graph based on the env matrix. Edges are added between neighboring nodes according to the environment.
#
def build_graph(grid_env):
    V = [[Node(i, j) for j in range(grid_env.shape[1])] for i in range(grid_env.shape[0])]
    for i in range(grid_env.shape[0]):
        for j in range(grid_env.shape[1]):
            if grid_env[i,j] == 1:
                V[i][j].blocked = True
            for k in [-1, 0, 1]:
                for l in [-1, 0, 1]:
                    if 0 <= i+k < grid_env.shape[0] and 0 <= j+l < grid_env.shape[1] and (k != 0 or l != 0):
                        if grid_env[i][j] !=1 and grid_env[i+k][j+l] != 1:
                            V[i][j].neighbors.append(V[i+k][j+l])
    return V
