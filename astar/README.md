**A* for Grid Environment**

1) Randomly generate grid environment with obstacles (blocked = black, free = white)

2) Run A* to find the shortest path between start and goal cell

3) Visualize the environment, the frontier (yellow) and path path from A* (green).

**Command**

*python main.py*

**Examples**

![picture](figures/nopathfound.png)

![picture](figures/pathfound1.png)

![picture](figures/pathfound2.png)