import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np
#
# This module generates figures describing the environment and path returned by A*
#


#
# Plot grid environment
# white = free, black = blocked, blue = start, red = goal, green = path, yellow = frontier, magenta = visited
#
def plot_grid(grid):
    fig, ax = plt.subplots()
    # create discrete colormap
    cmap = colors.ListedColormap(['white', 'black', 'blue', 'red', 'yellow', 'magenta', 'green'])
    bounds = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5]
    norm = colors.BoundaryNorm(bounds, cmap.N)

    ax.imshow(grid, cmap=cmap, norm=norm)
    ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=2)
    ax.set_xticks(np.arange(-0.5, grid.shape[0], 1));
    ax.set_yticks(np.arange(-0.5, grid.shape[1], 1));
    plt.show()


#
# Plot grid environment by assigning value according to color map in plot_grid() above.
#
def viz_path(grid, graph, start, end):
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            if graph[i][j].inqueue and grid[i, j] != 1:
                grid[i, j] = 4
            #if graph[i][j].visited and grid[i, j] != 1:
            #    grid[i, j] = 5

    current = end
    while current != start and current is not None:
        current = current.parent
        if current is not None and grid[current.x, current.y] != 1:
            grid[current.x, current.y] = 6

    grid[start.x, start.y] = 2
    grid[end.x, end.y] = 3

    return grid
