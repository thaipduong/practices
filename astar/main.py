import astar
import env
import viz

# Generate random grid environment and visualize it.
grid_env, start_cell, goal_cell = env.grid_env_gen(0.3, 10, 10)
viz.plot_grid(grid_env)

# Build graph of nodes based on the environment.
V = env.build_graph(grid_env)
start_node = V[start_cell[0]][start_cell[1]]
goal_node = V[goal_cell[0]][goal_cell[1]]

# Run A*
if astar.astar(start_node, goal_node):
    print("Found path!")
else:
    print("Path not found!")

# Visualize the environment after A*
grid_env_with_path = viz.viz_path(grid_env, V, start_node, goal_node)
viz.plot_grid(grid_env_with_path)
