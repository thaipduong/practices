import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from rtree import index
import kernels


"""
class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max, G = None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)
        print("alpha: \n" + str(alpha))
        if G is None:
            G = kernels.gram_matrix(X, kernel=self.kernel)
        for iter in range(iter_max):
            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)

            for i in ind_list:
                y_predict = np.sign(np.sum(np.multiply(np.multiply(alpha, Y), G[i].reshape([X.shape[0], 1]))))
                # print(y_predict)
                if y_predict == 0.0:
                    y_predict = 1.0
                # print(y_predict)
                # print(Y[i][0])
                if y_predict != Y[i][0]:
                    alpha[i][0] += 1.0

        return alpha

    def predict(self, X, Y, alpha, X1, X2):
        y_predict = np.zeros([X2.shape[0], X1.shape[0]])
        for j in range(X1.shape[0]):
            for k in range(X2.shape[0]):
                x_test = np.array([X1[j], X2[k]])
                G_row = np.zeros([X.shape[0], 1])

                for i in range(X.shape[0]):
                    G_row[i] = self.kernel(X[i, :], x_test)
                G_row.reshape([X.shape[0], 1])
                y_predict[j][k] = np.sum(np.multiply(np.multiply(alpha, Y), G_row))

        return y_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha):
        x1 = np.linspace(-6, 6)
        x2 = np.linspace(-6, 6)
        # plot_data(X,Y)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]
        ax.scatter(X[:, 0], X[:, 1], color=colors)
        ax.contour(x1, x2,
                   self.predict_y(X, Y, alpha, x1, x2), levels=[0], cmap="Greys_r")
        plt.show()
"""


class Fastron(object):
    def __init__(self, kernel = kernels.rbf_kernel, update_argminF=True, remove_redundant=True):
        self.kernel = kernel
        self.update_argminF = update_argminF
        self.remove_redundant = remove_redundant
        self.rtree_index = index.Index()
        self.gamma = 5
    def G(self, x, y):
        y = np.array([y[0], y[1]])
        x = np.array([x[0], x[1]])
        return self.kernel(x,y, gamma=self.gamma)

    def train(self, iter_max=100, alpha=None, local_neighbors = None):
        if alpha is None:
            alpha = {} #

        previous_F = True
        F = {} #np.zeros([X.shape[0], 1], dtype=np.float64)#np.matmul(G, alpha)
        for nb in local_neighbors:
            F[nb] = 0
            nearest_alpha = self.rtree_index.nearest((nb[0], nb[1], nb[0], nb[1]), 10, objects=True)
            #print("alpha len: ", len(alpha))
            na_len = 0
            for na in nearest_alpha:
                na_len = na_len +1
                #print("nearest alpha:", na.bbox)
                a = (na.bbox[0], na.bbox[1])
                print("a = ", a, "local nb", alpha)
                F[nb] = F[nb] + self.G(nb,a)*alpha[a]

            #for a in alpha:
            #	#print("a = ", a)
           # 	F[nb] = F[nb] + self.G(nb,a)*alpha[a]

            #previous_F = False
            #print("na len: ", na_len)
        #print("F: ", F)
        #print("iter_max = " + str(iter_max))

        for iter in range(iter_max):
            r_plus = 1.5
            r_minus = 1
            #print("local_neighbors: ", local_neighbors)
            if local_neighbors is not None:
                if self.update_argminF and previous_F:

                    #local_F = F[local_neighbors]
                    #local_Y = Y[local_neighbors]
                    #print("local_F", local_F)
                    #print("local_Y", local_Y)
                    min = None
                    local_min_idx = None
                    for nb in local_neighbors:
                        nb_fy = F[nb]*local_neighbors[nb]
                        f_sign = np.sign(F[nb])
                        if f_sign == 0.0:
                            f_sign = 1.0
                        #print("nb_fy", nb_fy)
                        if min is None:
                            min = nb_fy
                            local_min_idx = nb
                            #print("min1",min,local_min_idx, nb_fy)
                        elif min > nb_fy or (min == nb_fy and f_sign*local_neighbors[nb] < 0):
                            min = nb_fy
                            local_min_idx = nb
                            print("min2",min, local_min_idx, nb_fy)
                    ind_list = [local_min_idx]
                    #print("fastron update min: ", ind_list)
                else:
                    ind_list = local_neighbors
                #print("fastron update: ", ind_list)
            else:
                return

            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i])
                if y_predict == 0.0:
                    y_predict = 1.0
                #print("iter", iter, "i", i, "y_predict", y_predict, local_neighbors[i])
                if y_predict != local_neighbors[i]:

                    correct_prediction = False
                    r = r_plus if local_neighbors[i] == 1 else r_minus
                    delta_alpha = r * local_neighbors[i] - y_predict

                    if i in alpha:
                        alpha[i] = alpha[i] + delta_alpha
                    else:
                        alpha[i] = delta_alpha
                        self.rtree_index.insert(i[0]*1000 + i[1], (i[0], i[1], i[0], i[1]))
                        print("Insert from TREE", i[0], i[1], i[0], i[1])
                    #print("alpha", alpha, "F", F)
                    #print("local_neighbors", local_neighbors)
                    for nb in local_neighbors:
                        #print("idx", i, nb)
                        F[nb] = F[nb] + delta_alpha * self.G(nb,i)
                    #print("i = " + str(i))

            tree_nodes = self.rtree_index.intersection((0.0, 0.0, 10.0, 10.0), objects=True)
            for l in tree_nodes:
                print("rtree added:", l.bbox)

            # Update support points
            if self.remove_redundant:
                #margin = Y * (F - alpha) * np.int64(alpha != 0)

                for m in alpha.copy():
                    if m not in local_neighbors:
                        continue
                    margin_m = local_neighbors[m]*(F[m] - alpha[m])
                    #print(type(margin_m))
                    if margin_m > 0:
                        #print("Remove m = " + str(m) + ", margin = " + str(margin[m]) + ", alpha = " + str(
                        #    alpha[m][0]) + ", F = " + str(F[m][0]) + ", Y = " + str(Y[m][0]))
                        for nb in local_neighbors:
                            F[nb] = F[nb] - alpha[m] * self.G(nb, m)
                        tree_nodes = list(self.rtree_index.intersection((-100000.0, -100000.0, 100000.0, 100000.0)))
                        print("RTREE len BEFORE delete: ", len(tree_nodes))
                        alpha.pop(m)
                        self.rtree_index.delete(m[0]*1000 + m[1], (float(m[0]), float(m[1]), float(m[0]), float(m[1])))
                        print("Delete from TREE", float(m[0]), float(m[1]), float(m[0]), float(m[1]))
                        tree_nodes = list(self.rtree_index.intersection((-100000.0, -100000.0, 100000.0, 100000.0)))
                        print("RTREE len AFTER delete: ", len(tree_nodes))
                        tree_nodes = self.rtree_index.intersection((0.0, 0.0, 10.0, 10.0), objects=True)
                        for l in tree_nodes:
                            print("rtree remained:", l.bbox)
                        print("Delete!")

                #tree_nodes = self.rtree_index.intersection((0.0, 0.0, 10.0, 10.0), objects=True)
                #for l in tree_nodes:
                #    print("rtree remained:", l.bbox)
            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break

        tree_nodes = self.rtree_index.intersection((0.0, 0.0, 10.0, 10.0), objects=True)
        tree_len = 0
        for l in tree_nodes:
            #print("rtree final:", l.bbox)
            tree_len = tree_len + 1

        #print("rtree len:", tree_len)
        #Print out support points
        #margin = Y * (F - alpha) * np.int64(alpha != 0)
        #for m in alpha:
                #print("Support points m = " + str(m) + ", alpha = " + str(alpha[m]) + ", X = " + str(m) + ", Y = " + str(1 if alpha[m] >0 else -1))

        return alpha, F

    def calculate_score(self, alpha, x_test):
        score = 0
        #print("a = ", alpha)
        for a in alpha:
            #print("a = ", a)
            score = score + alpha[a]*self.G(x_test, a)

        return score

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            #for j in range(X.shape[0]):
            #    G_row[j] = self.kernel(X[j, :], x_test)
            f_predict[i][0] = self.calculate_score(alpha, x_test)

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    def check_free_radius(self, X, alpha, x_test, tighter_bound = False):
        G_row = np.zeros([X.shape[0], 1])
        dist = np.zeros([X.shape[0], 1])
        for j in range(X.shape[0]):
            G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            #print(X[j, :] - x_test)
            dist[j] = np.linalg.norm(X[j, :] - x_test) ** 2


        #print(x_test, t)

        min_dist_minus = None
        min_idx_minus = None
        for j in range(X.shape[0]):
            if alpha[j] >= 0:
                continue
            if min_dist_minus == None:
                min_dist_minus = dist[j][0]
                min_idx_minus = j
            elif min_dist_minus > dist[j]:
                min_dist_minus = dist[j][0]
                min_idx_minus = j

        alpha_minus = np.abs(alpha[min_idx_minus])
        total_plus = np.sum(alpha[alpha > 0])
        #print("alpha,beta",x_test, alpha_minus, beta_plus, min_idx_minus, min_dist_minus, min_idx_plus, min_dist_plus)
        t2 = None
        for j in range(X.shape[0]):
            if alpha[j] <= 0:
                continue

            if tighter_bound:
                temp_max = None
                for k in range(X.shape[0]):
                    if alpha[k] >= 0:
                        continue
                    beta_plus = (np.log(np.abs(alpha[k])) - np.log(total_plus)) / self.gamma
                    temp1 = beta_plus + dist[j] - dist[k]
                    temp2 = 2 * np.linalg.norm(X[k, :] - X[j, :])
                    temp = temp1 / temp2
                    #print(x_test, X[j, :], X[k, :], temp, t2)
                    if temp_max == None:
                        temp_max = temp
                    elif temp_max < temp:
                        temp_max = temp
                if t2 == None:
                    t2 = temp_max
                elif t2 > temp_max:
                    t2 = temp_max
            else:
                beta_plus = (np.log(np.abs(alpha_minus)) - np.log(total_plus)) / self.gamma
                temp1 = beta_plus + dist[j] - dist[min_idx_minus]
                temp2 = 2*np.linalg.norm(X[min_idx_minus, :] - X[j, :])
                temp = temp1/temp2
                #print(x_test, X[j, :], X[min_idx_minus, :], temp, t2)
                if t2 == None:
                    t2 = temp
                elif t2 > temp:
                    t2 = temp


        return t2



    def collision_checking(self, alpha_dict, X_test, check_radius = False):
        X = np.array(list(alpha_dict.keys()), dtype=np.float32)

        alpha = np.zeros(X.shape[0], dtype=np.float32)
        for i in range(X.shape[0]):
            alpha[i] = alpha_dict[(X[i, 0], X[i, 1])]
        f_predict_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_upperbound = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        minus_apha = alpha[alpha < 0]
        plus_apha = alpha[alpha > 0]
        plus_apha_total = np.sum(plus_apha)
        minus_alpha_total = np.abs(np.sum(minus_apha))
        radius = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            total_dist = 0
            for j in range(X.shape[0]):
                if alpha[j] >= 0:
                    continue
                dist = np.linalg.norm(X[j, :] - x_test)**2
                total_dist = total_dist + dist*np.abs(alpha[j])
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
                #G_row[j] = self.kernel(X[j, :], x_test, gamma=alpha[j]/plus_alpha_total) if alpha[j] > 0 else 0
            total_dist = total_dist / minus_alpha_total
            #print("total_dist", total_dist)
            f_predict_minus[i][0] = np.max(G_row)#minus_alpha_total*np.exp(-self.gamma*total_dist)
            minus_idx_min = np.argmax(G_row)
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                if alpha[j] <= 0:
                    continue
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            f_predict_plus[i][0] = np.max(G_row)
            plus_idx_min = np.argmax(G_row)
            f_predict_upperbound[i][0] = plus_apha_total*f_predict_plus[i][0] + alpha[minus_idx_min]*f_predict_minus[i][0]
            if check_radius:
                radius[i] = self.check_free_radius(X, alpha, x_test)



        y_predict = np.sign(f_predict_upperbound)
        print(np.max(f_predict_upperbound), np.min(f_predict_upperbound))
        print(np.max(f_predict_plus), np.min(f_predict_plus), plus_apha_total)
        print(np.max(f_predict_minus), np.min(f_predict_minus), minus_alpha_total)
        #print("radius:", radius)
        return y_predict, f_predict_upperbound, radius

    def check_line(self, alpha_dict, A, v, tighter_bound = True):
        X = np.array(list(alpha_dict.keys()))

        alpha = np.zeros(X.shape[0], dtype=np.float32)
        for i in range(X.shape[0]):
            alpha[i] = alpha_dict[(X[i,0], X[i,1])]
        x_test = A

        G_row = np.zeros([X.shape[0], 1])
        dist = np.zeros([X.shape[0], 1])
        for j in range(X.shape[0]):
            G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            # print(X[j, :] - x_test)
            dist[j] = np.linalg.norm(X[j, :] - x_test) ** 2

        # print(x_test, t)

        min_dist_minus = None
        min_idx_minus = None
        for j in range(X.shape[0]):
            if alpha[j] >= 0:
                continue
            if min_dist_minus == None:
                min_dist_minus = dist[j][0]
                min_idx_minus = j
            elif min_dist_minus > dist[j]:
                min_dist_minus = dist[j][0]
                min_idx_minus = j

        alpha_minus = np.abs(alpha[min_idx_minus])
        total_plus = np.sum(alpha[alpha > 0])
        # print("alpha,beta",x_test, alpha_minus, beta_plus, min_idx_minus, min_dist_minus, min_idx_plus, min_dist_plus)
        t2 = None
        i_star = None
        j_star = None
        for j in range(X.shape[0]):
            if alpha[j] <= 0:
                continue

            if tighter_bound:
                temp_max = None
                for k in range(X.shape[0]):
                    if alpha[k] >= 0:
                        continue
                    beta_plus = (np.log(np.abs(alpha[k])) - np.log(total_plus))/self.gamma
                    temp1 =  beta_plus + dist[j] - dist[k]
                    #if temp1 <= 0:
                    #    continue
                    x_i = X[j, :]
                    x_j = X[k, :]
                    term3 = -X[k, :] + X[j, :]
                    temp2 = 2* np.matmul(v, np.transpose(term3))
                    #print("abc1",x_test, X[j, :], X[k, :], temp1, term3, temp2, alpha[k], alpha[j])
                    if temp2 <= 0:
                        temp2 = 0.0000000001

                    temp = temp1 / temp2
                    #print("abc2",i_star, j_star, temp)
                    if temp_max == None:
                        temp_max = temp
                        j_star = k
                    elif temp_max <= temp:
                        temp_max = temp
                        j_star = k
                    #print("abc3",i_star, j_star, temp, temp1, temp2, temp_max)
                if t2 == None:
                    t2 = temp_max
                    i_star = j
                elif t2 >= temp_max:
                    t2 = temp_max
                    i_star = j
            else:
                beta_plus = (np.log(np.abs(alpha_minus)) - np.log(total_plus)) / self.gamma
                temp1 = beta_plus + dist[j] - dist[min_idx_minus]
                temp2 = 2 * np.linalg.norm(X[min_idx_minus, :] - X[j, :])
                term3 = -X[min_idx_minus, :] + X[j, :]
                temp2 = 2* np.matmul(v, np.transpose(term3))
                # If term2 <=0, there is no limit on t
                if temp2 <=0:
                    temp2 = 0.00000000001
                temp = temp1 / temp2
                #print(x_test, X[j, :], X[min_idx_minus, :], temp, t2)
                if t2 == None:
                    t2 = temp
                    i_star = j
                elif t2 >= temp:
                    t2 = temp
                    i_star = j
        if t2 == None:
            t2 = 1
        #print("i_star:", X[i_star,:])
        #print("j_star:", X[j_star, :])
        return t2

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data = False):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        colors = ['green' if l == -1. else 'red' for l in Y]
        if show_data:
            s = [60 if a in alpha else 0 for a in range(len(X[:, 0]))]
            ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
            ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="cool_r")
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")

    def plot_decision_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data=False):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        #colors = ['red' if l == 1. else 'green' for l in Y]
        if show_data:
            occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
            free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
            label = ['occupied', 'free']
            print(occupied.shape)
            s = [60 if (X[i,0], X[i,1]) in alpha else 0 for i in range(X.shape[0])]
            c = ['b' if Y[i,0] < 0 else 'r' for i in range(X.shape[0])]
            ax.scatter(X[:, 0], X[:, 1], color=c, s=s)

            #ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
            #ax.scatter(free[:, 0], free[:, 1], color="green", s=20, label="free")

        y_predict, f_predict, radius = self.collision_checking(alpha, X_grid)
        CS = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape),levels=[0],  cmap="cool_r")
        ax.clabel(CS, inline=1, fontsize=10)


    def predict_probability(self, X, Y, alpha, X_test, scale=1):
        f_predict_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)

            f_vec = np.multiply(alpha, G_row)
            f_predict_plus[i][0] = np.sum(np.multiply(f_vec, np.int64(alpha > 0)))
            f_predict_minus[i][0] = -np.sum(np.multiply(f_vec, np.int64(alpha < 0)))
        print(sum(np.int64(alpha > 0 )))
        print(sum(np.int64(alpha < 0)))
        f_predict = 1/ (1 + scale*f_predict_minus / f_predict_plus)
        print(f_predict.max())
        print(f_predict.min())
        return f_predict

    # Plotting decision boundary
    def plot_probability(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data = True):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        colors = ['green' if l == -1. else 'red' for l in Y]
        if show_data:
            s = [0 if a == 0 else 60 for a in alpha]
            ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
            ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        f_predict = self.predict_probability(X, Y, alpha, X_grid)
        #print(f_predict)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), cmap="Greys_r")
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")




class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max=100, G=None, alpha=None, F=None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)

        if G is None:
            G = kernels.gram_matrix(X, kernel = self.kernel)
        if F is None:
            F = np.matmul(G, alpha)

        F = np.matmul(G, alpha)
        for iter in range(iter_max):

            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)
            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i][0])
                if y_predict == 0.0:
                    y_predict = 1.0

                if y_predict != Y[i][0]:
                    correct_prediction = False
                    alpha[i][0] += Y[i][0]
                    F = F + Y[i][0] * (G[:, i].reshape([X.shape[0], 1]))

            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break

        return alpha, F

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test)
            f_predict[i][0] = np.sum(np.multiply(alpha, G_row))

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6]):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]

        s = [0 if a == 0 else 60 for a in alpha]
        ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
        ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r")
        ax.set_title("Kernel Perceptron (" + str(self.kernel.__name__) + ")")
        plt.show()







