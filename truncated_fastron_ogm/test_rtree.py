from rtree import index
import kernels

rtree_index = index.Index()

for x in range(3):
    for y in range(3):
        i = (float(x),float(y))
        rtree_index.insert(0, (i[0], i[1], i[0], i[1]))
        print("Insert from TREE", i[0], i[1], i[0], i[1])

tree_nodes = list(rtree_index.intersection((-100000.0, -100000.0, 100000.0, 100000.0)))
print("RTREE len before: ", len(tree_nodes))
for x in range(2):
    for y in range(2):
        i = (float(x),float(y))
        rtree_index.delete(0, (i[0], i[1], i[0], i[1]))
        print("Delete from TREE", i[0], i[1], i[0], i[1])

tree_nodes = list(rtree_index.intersection((-100000.0, -100000.0, 100000.0, 100000.0)))
print("RTREE len after: ", len(tree_nodes))