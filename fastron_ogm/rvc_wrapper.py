import fast_rvm2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle

class RVCWrapper(object):
    def __init__(self):
        self.rvm = fast_rvm2.RVC2(kernel='rbf', gamma=3)

    def train(self, X, Y, iter_max=100, G=None, alpha=None, F=None, local_neighbors = None):
        Y2 = np.copy(Y)
        Y2[Y2<0] = 0
        Y2 = Y2.flatten().astype('int')
        self.rvm.fit(X,Y2)
        print("Done")

    def predict(self, X_test):
        rv_grid, var_grid = self.rvm.predict_proba(X_test)
        return rv_grid[:,1]

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data = True):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        colors = ['red' if l == -1. else 'green' for l in Y]
        if show_data:
            s = [0 if a == 0 else 60 for a in alpha]
            ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
            ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        y_predict = self.predict(X_grid)
        ax.contour(x0mesh, x1mesh,
                   y_predict.reshape(x0mesh.shape), levels=[0.5], cmap="cool_r")
        # ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")

    #def predict_probability(self, X, Y, alpha, X_test, scale=1):

    # Plotting decision boundary
    #def plot_probability(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data = True):




