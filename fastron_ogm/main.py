import astar
import env
import numpy as np
import matplotlib.pyplot as plt
import viz

# Generate random grid environment and visualize it.
grid_env, start_cell, goal_cell = env.grid_env_gen(0.1, 10, 10, fixed=True)
viz.plot_grid(np.transpose(grid_env))
plt.show()

# Build graph of nodes based on the environment.
graph = env.Graph(grid_env, allow_diag=False)
start_node = graph.V[start_cell[0]][start_cell[1]]
goal_node = graph.V[goal_cell[0]][goal_cell[1]]



N = 100
current_node = start_node
history = list([current_node])
fig, ax = plt.subplots()
for i in range(N):



    graph.current_node = current_node
    graph.observe_and_update_map(activation="sign")

    print(str(i) + "th node: (" + str(current_node.x) + "," + str(current_node.y) + str(")"))
    # Run A*
    graph.reset_graph()
    if astar.astar(current_node, goal_node):
        print("Found path!")
    else:
        print("Path not found!")

    # Visualize the environment after A*
    ax.clear()
    grid_env_with_path = viz.viz_path(grid_env, graph.V, start_node, goal_node)
    print(grid_env_with_path)


    #graph.classifier.plot_boundary(graph.X, graph.Y, graph.alpha, x_min=[-0.5, -0.5],
    #                               x_max=[graph.x_size - 0.5, graph.y_size - 0.5], fig=fig, ax=ax, show_data=False)
    graph.classifier2.plot_boundary(graph.X, graph.Y, graph.alpha, x_min=[-0.5, -0.5],
                                   x_max=[graph.x_size - 0.5, graph.y_size - 0.5], fig=fig, ax=ax, show_data=False)
    viz.plot_grid(grid_env_with_path, start_node, goal_node, history, fig=fig, ax=ax)

    if current_node == goal_node:
        break
    current_node = current_node.child
    history.append(current_node)
    plt.pause(0.5)



plt.show()