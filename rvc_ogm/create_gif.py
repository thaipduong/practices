import imageio
images = []
img_count = 27
for i in range(0,img_count):
    images.append(imageio.imread(("./figures/fastron_mapping_" + str(i) + ".png")))
imageio.mimsave("./figures/fastron_nav.mp4", images, fps=0.67)
