import numpy as np
import perceptron
import kernels
#
# This module generates environment as a matrix and converts it into a graph of nodes accordingly.
#


#
# Generate grid environment randomly:
# 0 = free, 1 = blocked, 2 = start, 3 = goal
# The probability of a cell being blocked in p
#
def grid_env_gen(p, x_size, y_size, fixed_start_goal = True, fixed = True):

    if fixed:
        """
        data = np.array([[0., 0., 0., 0., 1., 1., 0., 0., 0., 0.],
                         [0., 0., 1., 1., 1., 0., 0., 1., 1., 1.],
                         [1., 0., 1., 0., 1., 0., 0., 0., 0., 0.],
                         [0., 0., 0., 1., 0., 0., 1., 0., 1., 0.],
                         [1., 0., 0., 1., 0., 0., 1., 1., 1., 0.],
                         [1., 1., 1., 1., 1., 1., 1., 0., 0., 1.],
                         [0., 0., 0., 0., 0., 1., 0., 0., 1., 0.],
                         [0., 0., 1., 1., 0., 0., 0., 0., 0., 0.],
                         [1., 0., 0., 0., 0., 1., 1., 1., 0., 0.],
                         [0., 0., 1., 1., 0., 0., 0., 0., 0., 0.]])
        """
        data = np.array([[0., 1., 1., 0., 0., 0., 0., 0., 0., 1.,],
                         [0., 0., 1., 0., 0., 0., 1., 1., 0., 0.,],
                         [0., 0., 0., 1., 0., 1., 1., 0., 1., 0.,],
                         [1., 0., 0., 0., 0., 1., 0., 0., 0., 0.,],
                         [1., 0., 0., 0., 0., 1., 0., 0., 1., 0.,],
                         [1., 1., 1., 0., 0., 0., 0., 1., 0., 0.,],
                         [1., 0., 1., 0., 0., 1., 1., 1., 0., 1.,],
                         [0., 1., 0., 0., 0., 0., 0., 1., 0., 0.,],
                         [0., 0., 0., 0., 1., 0., 0., 0., 1., 0.,],
                         [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,]])
        """
        data = np.array([[0., 1., 1., 0., 0., 0., 0., 0., 0., 1.,],
                         [0., 1., 0., 0., 0., 0., 1., 1., 0., 0.,],
                         [0., 0., 0., 0., 1., 1., 1., 0., 1., 0.,],
                         [1., 1., 0., 0., 0., 1., 0., 0., 0., 0.,],
                         [1., 0., 0., 0., 0., 1., 0., 0., 1., 0.,],
                         [1., 1., 1., 0., 0., 0., 0., 0., 0., 0.,],
                         [1., 0., 1., 0., 0., 1., 0., 1., 0., 1.,],
                         [0., 1., 0., 0., 0., 0., 0., 1., 1., 0.,],
                         [0., 0., 0., 0., 1., 0., 0., 0., 1., 0.,],
                         [0., 0., 0., 0., 0., 1., 1., 0., 0., 0.,]])
        
        """
    else:
        data = np.random.choice([0, 1.], size=(x_size, y_size), p=[1-p, p])

    #data = np.array([[0., 0., 0., 0., 1., 0., 0., 1., 0., 0.],
    #                 [0., 1., 1., 1., 1., 0., 0., 0., 0., 0.],
    #                 [0., 1., 1., 1., 0., 1., 0., 1., 0., 1.],
    #                 [0., 0., 1., 1., 1., 0., 0., 0., 1., 0.],
    #                 [0., 0., 0., 1., 0., 0., 0., 0., 0., 1.],
    #                 [1., 1., 0., 0., 0., 0., 0., 1., 0., 1.],
    #                 [0., 0., 1., 0., 0., 0., 0., 0., 0., 1.],
    #                 [0., 0., 1., 0., 1., 0., 0., 0., 0., 0.],
    #                 [0., 1., 0., 0., 1., 1., 0., 0., 0., 0.],
    #                 [0., 1., 0., 0., 0., 0., 0., 0., 0., 0.]])

    if not fixed_start_goal:
        while True:
            x = np.random.randint(0, x_size)
            y = np.random.randint(0, y_size)
            if data[x, y] == 0:
                start_cell = [x, y]
                data[x, y] = 0
                break
        while True:
            x = np.random.randint(0, x_size)
            y = np.random.randint(0, y_size)
            if data[x, y] == 0:
                goal_cell = [x, y]
                data[x, y] = 0
                break
    else:
        data[0,0] = 0
        data[x_size - 1, y_size - 1] = 0
        start_cell = [0,0]
        goal_cell = [x_size - 1, y_size - 1]
    print(data)
    return data, start_cell, goal_cell
#
# class Node for cell. Each node contains information for A* to work.
#
class Node:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None
        self.child = None
        self.next_cell = None
        self.neighbors = []
        self.observation_nb = []
        self.collided = []
        self.visited = False
        self.inqueue = False
        self.g = float('inf')
        self.f = float('inf')
        self.prior = 0.0
        self.blocked_prob = 0.0
        self.penalty = 100
        self.step_cost = 1
        self.observed = False
        self.observation_confidence = 0.9
        self.data_idx = None

    def cost(self, next_node, alpha = None, classifier = None):

        if alpha is not None and classifier is not None:
            t_uAB = classifier.check_line(alpha, [self.x, self.y],
                                        [next_node.x - self.x, next_node.y - self.y],
                                        tighter_bound=True)
            if t_uAB >= 1:
                return self.step_cost
            else:
                return float('inf')

        if self.neighbors.__contains__(next_node):
            if self.collided.__contains__(next_node):
                return self.step_cost + (np.exp(10) - 1)* self.penalty
            else:
                return self.step_cost + (np.exp(10*next_node.blocked_prob) - 1)* self.penalty
        else:
            return float('inf')

    def h(self, goal):
        return np.sqrt((self.x - goal.x) ** 2 + (self.y - goal.y) ** 2)

    def __lt__(self, other):
        return self.f < other.f

    def reset(self):
        self.parent = None
        self.child = None
        self.visited = False
        self.inqueue = False
        self.g = float('inf')
        self.f = float('inf')



class Graph:
    def __init__(self, grid_env, start_node = None, allow_diag = True):
        self.grid_env = grid_env
        self.x_size = grid_env.shape[0]
        self.y_size = grid_env.shape[1]
        self.V = self.build_graph(allow_diag)
        if start_node is None:
            self.current_node = self.V[0][0]
        else:
            self.current_node = start_node
        # Variables for Fastron
        x0 = np.linspace(0, self.x_size - 1, self.x_size)
        x1 = np.linspace(0, self.y_size - 1, self.y_size)
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh = x0mesh.flatten()
        x1mesh = x1mesh.flatten()
        self.X = np.vstack((x0mesh, x1mesh))
        self.X = np.transpose(self.X)

        for i in range(self.X.shape[0]):
            x = self.X[i, :].astype(int)
            self.V[x[0]][x[1]].data_idx = i

        print("X shape: " + str(self.X.shape))
        self.Y = -np.ones([self.X.shape[0], 1])
        self.alpha = None
        self.F = None
        # Fastron with gaussian kernel
        self.classifier = perceptron.Fastron(kernel=kernels.rbf_kernel, update_argminF=True, remove_redundant=True)

    def calculate_score(self, X, alpha, kernel = kernels.rbf_kernel):
        score = np.zeros([X.shape[0], 1], dtype=np.float64)
        for cell in range(len(score)):
            for a in alpha:
                a_vec = [a[0], a[1]]
                score[cell] = score[cell] + alpha[a]*kernel(X[cell,:],a_vec, self.classifier.gamma)
        return score
    def observe_and_update_map(self, activation="sigmoid"):
        local_neighbors = {}
        local_neighbors[(self.current_node.x, self.current_node.y)] = self.Y[self.current_node.data_idx]

        for observed_neighbor in self.current_node.observation_nb:
            observed_neighbor.observed = True

            if self.grid_env[observed_neighbor.x, observed_neighbor.y] > 0.5:
                self.Y[observed_neighbor.data_idx] = 1

            local_neighbors[(observed_neighbor.x, observed_neighbor.y)] = self.Y[observed_neighbor.data_idx][0]
            for neighbor2 in observed_neighbor.observation_nb:
                local_neighbors[(neighbor2.x, neighbor2.y)] = self.Y[neighbor2.data_idx][0]
        print("local neighbor: ", local_neighbors)

        self.alpha, self.F = self.classifier.train(iter_max=200, alpha=self.alpha, local_neighbors=local_neighbors)
        print("local coordinates: ")
        for nb in local_neighbors:
            print(nb, self.F[nb])
        #if activation is "ratio":
        #    self.f_predict = self.classifier.predict_probability(self.X, self.Y, self.alpha, self.X, scale=1)
        #self.classifier.plot_boundary(self.X, self.Y, self.alpha, x_min= [0, 0], x_max=[self.x_size, self.y_size])
        print(self.alpha)
        score = self.calculate_score(self.X, self.alpha)
        print("score size: ", score.shape)
        for i in range(len(score)):
            x = self.X[i, :].astype(int)
            if activation is "sigmoid":
                self.V[x[0]][x[1]].blocked_prob = 1 / (1 + np.exp(-score[i]))
            elif activation is "sign":
                self.V[x[0]][x[1]].blocked_prob = 0 if score[i] < 0 else 1
            elif activation is "ratio":
                self.V[x[0]][x[1]].blocked_prob = self.f_predict[i]

        print("alpha: ", self.alpha)
        print("F = " + str(self.F))
        return 0

    #
    # Generate graph based on the env matrix. Edges are added between neighboring nodes according to the environment.
    #
    def build_graph(self, allow_diag=True):
        observation_depth = 3
        V = [[Node(i, j) for j in range(self.grid_env.shape[1])] for i in range(self.grid_env.shape[0])]
        for i in range(self.grid_env.shape[0]):
            for j in range(self.grid_env.shape[1]):
                #if grid_env[i,j] == 1:
                #    V[i][j].blocked = True
                for k in range(-observation_depth,observation_depth + 1):
                    for l in range(-observation_depth,observation_depth + 1):
                        #if 0 <= i+k < self.grid_env.shape[0] and 0 <= j+l < self.grid_env.shape[1] and (k != 0 or l != 0):
                        if 0 <= i + k < self.grid_env.shape[0] and 0 <= j + l < self.grid_env.shape[1] and (k != 0 or l != 0) and ((k**2 + l**2)<2 or allow_diag):
                            #if grid_env[i][j] !=1 and grid_env[i+k][j+l] != 1:
                            V[i][j].neighbors.append(V[i+k][j+l])
                        if 0 <= i + k < self.grid_env.shape[0] and 0 <= j + l < self.grid_env.shape[1] and (k != 0 or l != 0):
                            #if grid_env[i][j] !=1 and grid_env[i+k][j+l] != 1:
                            V[i][j].observation_nb.append(V[i+k][j+l])
        return V

    def reset_graph(self):
        for i in range(self.x_size):
            for j in range(self.y_size):
                self.V[i][j].reset()

    def cost_matrix(self):
        cost_mtx = np.zeros([self.x_size, self.y_size])
        for i in range(self.x_size):
            for j in range(self.y_size):
                if self.V[i][j].observed:
                    cost_mtx[i,j] = (1-self.V[i][j].blocked_prob) * 1 + 1000 *  (self.V[i][j].blocked_prob)
                else:
                    cost_mtx[i, j] = 1
        return cost_mtx

