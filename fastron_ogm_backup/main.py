import astar
import env
import viz
import numpy as np
import matplotlib.pyplot as plt

# Generate random grid environment and visualize it.
grid_env, start_cell, goal_cell = env.grid_env_gen(0.3, 10, 10)
viz.plot_grid(np.transpose(grid_env))
plt.show()

# Build graph of nodes based on the environment.
graph = env.Graph(grid_env)
start_node = graph.V[start_cell[0]][start_cell[1]]
goal_node = graph.V[goal_cell[0]][goal_cell[1]]



N = 20
current_node = start_node
history = list([current_node])
fig, ax = plt.subplots()
for i in range(N):

    if current_node == goal_node:
        break

    current_node.observe(grid_env)
    print(str(i) + "th node: (" + str(current_node.x) + "," + str(current_node.y) + str(")"))
    # Run A*
    graph.reset_graph()
    if astar.astar(current_node, goal_node):
        print("Found path!")
    else:
        print("Path not found!")

    # Visualize the environment after A*
    grid_env_with_path = viz.viz_path(grid_env, graph.V, start_node, goal_node)
    print(grid_env_with_path)

    viz.plot_grid(grid_env_with_path, start_node, goal_node, history, fig=fig, ax=ax)
    current_node = current_node.child
    history.append(current_node)
    plt.pause(1)
    ax.clear()

