import numpy as np
import perceptron
#
# This module generates environment as a matrix and converts it into a graph of nodes accordingly.
#


#
# Generate grid environment randomly:
# 0 = free, 1 = blocked, 2 = start, 3 = goal
# The probability of a cell being blocked in p
#
def grid_env_gen(p, x_size, y_size, fixed_start_goal = True):
    data = np.random.choice([0, 1.], size=(x_size, y_size), p=[1-p, p])

    #data = np.array([[0., 0., 0., 0., 1., 0., 0., 1., 0., 0.],
    #                 [0., 1., 1., 1., 1., 0., 0., 0., 0., 0.],
    #                 [0., 1., 1., 1., 0., 1., 0., 1., 0., 1.],
    #                 [0., 0., 1., 1., 1., 0., 0., 0., 1., 0.],
    #                 [0., 0., 0., 1., 0., 0., 0., 0., 0., 1.],
    #                 [1., 1., 0., 0., 0., 0., 0., 1., 0., 1.],
    #                 [0., 0., 1., 0., 0., 0., 0., 0., 0., 1.],
    #                 [0., 0., 1., 0., 1., 0., 0., 0., 0., 0.],
    #                 [0., 1., 0., 0., 1., 1., 0., 0., 0., 0.],
    #                 [0., 1., 0., 0., 0., 0., 0., 0., 0., 0.]])

    if not fixed_start_goal:
        while True:
            x = np.random.randint(0, x_size)
            y = np.random.randint(0, y_size)
            if data[x, y] == 0:
                start_cell = [x, y]
                data[x, y] = 0
                break
        while True:
            x = np.random.randint(0, x_size)
            y = np.random.randint(0, y_size)
            if data[x, y] == 0:
                goal_cell = [x, y]
                data[x, y] = 0
                break
    else:
        data[0,0] = 0
        data[x_size - 1, y_size - 1] = 0
        start_cell = [0,0]
        goal_cell = [x_size - 1, y_size - 1]
    print(data)
    return data, start_cell, goal_cell
#
# class Node for cell. Each node contains information for A* to work.
#
class Node:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None
        self.child = None
        self.next_cell = None
        self.neighbors = []
        self.visited = False
        self.inqueue = False
        self.g = float('inf')
        self.f = float('inf')
        self.prior = 0.5
        self.blocked_prob = 0.5
        self.log_prob = np.log(1-self.blocked_prob) - np.log(self.blocked_prob)
        self.penalty = 100
        self.step_cost = 1
        self.observed = False
        self.observation_confidence = 0.9

    def observe(self, grid_env):
        if grid_env[self.x, self.y] > 0.5:
            self.update_belief(self.observation_confidence)
            self.observed = True
        else:
            self.update_belief(1 - self.observation_confidence)
        for neighbor in self.neighbors:
            neighbor.observed = True
            if grid_env[neighbor.x, neighbor.y] > 0.5:
                neighbor.update_belief(self.observation_confidence)
            else:
                neighbor.update_belief(1-self.observation_confidence)

    def update_belief(self, prob):
        delta_log_prob = np.log(1-prob) - np.log(prob) - (np.log(1-self.prior) - np.log(self.prior))
        self.log_prob += delta_log_prob
        self.blocked_prob = 1/(1+np.exp(self.log_prob))

    def cost(self, next_node):
        if self.neighbors.__contains__(next_node):
                return self.step_cost + (np.exp(next_node.blocked_prob) - 1)* self.penalty
        else:
            return float('inf')

    def h(self, goal):
        return np.sqrt((self.x - goal.x) ** 2 + (self.y - goal.y) ** 2)

    def __lt__(self, other):
        return self.f < other.f

    def reset(self):
        self.parent = None
        self.child = None
        self.visited = False
        self.inqueue = False
        self.g = float('inf')
        self.f = float('inf')


class Graph:
    def __init__(self, grid_env):
        self.grid_env = grid_env
        self.x_size = grid_env.shape[0]
        self.y_size = grid_env.shape[1]
        self.V = self.build_graph()
        # Variables for Fastron
        x0 = np.linspace(0, self.x_size, self.x_size)
        x1 = np.linspace(0, self.y_size, self.y_size)
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh = x0mesh.flatten()
        x1mesh = x1mesh.flatten()
        self.X = np.vstack((x0mesh, x1mesh))
        self.X = np.transpose(self.X)
        print("X shape: " + str(self.X.shape))

    #
    # Generate graph based on the env matrix. Edges are added between neighboring nodes according to the environment.
    #
    def build_graph(self):
        V = [[Node(i, j) for j in range(self.grid_env.shape[1])] for i in range(self.grid_env.shape[0])]
        for i in range(self.grid_env.shape[0]):
            for j in range(self.grid_env.shape[1]):
                #if grid_env[i,j] == 1:
                #    V[i][j].blocked = True
                for k in [-1, 0, 1]:
                    for l in [-1, 0, 1]:
                        if 0 <= i+k < self.grid_env.shape[0] and 0 <= j+l < self.grid_env.shape[1] and (k != 0 or l != 0):
                            #if grid_env[i][j] !=1 and grid_env[i+k][j+l] != 1:
                            V[i][j].neighbors.append(V[i+k][j+l])
        return V

    def reset_graph(self):
        for i in range(self.x_size):
            for j in range(self.y_size):
                self.V[i][j].reset()

    def cost_matrix(self):
        cost_mtx = np.zeros([self.x_size, self.y_size])
        for i in range(self.x_size):
            for j in range(self.y_size):
                if self.V[i][j].observed:
                    cost_mtx[i,j] = (1-self.V[i][j].blocked_prob) * 1 + 1000 *  (self.V[i][j].blocked_prob)
                else:
                    cost_mtx[i, j] = 1
        return cost_mtx