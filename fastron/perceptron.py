import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
import kernels
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

"""
class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max, G = None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)
        print("alpha: \n" + str(alpha))
        if G is None:
            G = kernels.gram_matrix(X, kernel=self.kernel)
        for iter in range(iter_max):
            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)

            for i in ind_list:
                y_predict = np.sign(np.sum(np.multiply(np.multiply(alpha, Y), G[i].reshape([X.shape[0], 1]))))
                # print(y_predict)
                if y_predict == 0.0:
                    y_predict = 1.0
                # print(y_predict)
                # print(Y[i][0])
                if y_predict != Y[i][0]:
                    alpha[i][0] += 1.0

        return alpha

    def predict(self, X, Y, alpha, X1, X2):
        y_predict = np.zeros([X2.shape[0], X1.shape[0]])
        for j in range(X1.shape[0]):
            for k in range(X2.shape[0]):
                x_test = np.array([X1[j], X2[k]])
                G_row = np.zeros([X.shape[0], 1])

                for i in range(X.shape[0]):
                    G_row[i] = self.kernel(X[i, :], x_test)
                G_row.reshape([X.shape[0], 1])
                y_predict[j][k] = np.sum(np.multiply(np.multiply(alpha, Y), G_row))

        return y_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha):
        x1 = np.linspace(-6, 6)
        x2 = np.linspace(-6, 6)
        # plot_data(X,Y)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]
        ax.scatter(X[:, 0], X[:, 1], color=colors)
        ax.contour(x1, x2,
                   self.predict_y(X, Y, alpha, x1, x2), levels=[0], cmap="Greys_r")
        plt.show()
"""


class Fastron(object):
    def __init__(self, kernel = kernels.rbf_kernel, update_argminF=False, remove_redundant=False):
        self.kernel = kernel
        self.update_argminF = update_argminF
        self.remove_redundant = remove_redundant
        self.gamma = 2

    def train(self, X, Y, iter_max=100, G=None, alpha=None, F=None):
        if alpha is None:
            alpha = np.zeros([X.shape[0], 1], dtype=np.float64)

        previous_F = True
        if G is None:
            G = kernels.gram_matrix(X, kernel=self.kernel, gamma=self.gamma)
        if F is None:
            F = np.matmul(G, alpha)
            previous_F = False

        print("iter_max = " + str(iter_max))

        for iter in range(iter_max):
            r_plus = 1.5
            r_minus = 1

            if self.update_argminF and previous_F:
                ind_list = [np.argmin(F * Y)]
            else:
                ind_list = [i for i in range(X.shape[0])]
            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i][0])
                if y_predict == 0.0:
                    y_predict = -1.0

                if y_predict != Y[i][0]:
                    correct_prediction = False
                    r = r_plus if Y[i][0] == 1 else r_minus
                    delta_alpha = r * Y[i][0] - y_predict
                    alpha[i][0] += delta_alpha
                    F = F + delta_alpha * (G[:, i].reshape([X.shape[0], 1]))
                    #print("i = " + str(i))

            # Update support points
            if self.remove_redundant:
                margin = Y * (F - alpha) * np.int64(alpha != 0)

                for m in range(len(margin)):
                    if margin[m] > -1e-20:
                        #print("Remove m = " + str(m) + ", margin = " + str(margin[m]) + ", alpha = " + str(
                        #    alpha[m][0]) + ", F = " + str(F[m][0]) + ", Y = " + str(Y[m][0]))
                        F = F - alpha[m][0] * (G[:, m].reshape([X.shape[0], 1]))
                        alpha[m][0] = 0
            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break
        #Print out support points
        margin = Y * (F - alpha) * np.int64(alpha != 0)
        for m in range(len(alpha)):
            if alpha[m][0] != 0:
                print("Support points m = " + str(m) + ", margin = " + str(margin[m]) + ", alpha = " + str(alpha[m][0]) + ", F = " + str(F[m][0]) + ", X = " + str(X[m, :]) + ", Y = " + str(Y[m][0]))

        return alpha, F

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            f_predict[i][0] = np.sum(np.multiply(alpha, G_row))

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    def check_free_radius(self, X, alpha, x_test, tighter_bound = True):
        G_row = np.zeros([X.shape[0], 1])
        dist = np.zeros([X.shape[0], 1])
        for j in range(X.shape[0]):
            G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            #print(X[j, :] - x_test)
            dist[j] = np.linalg.norm(X[j, :] - x_test) ** 2


        #print(x_test, t)

        min_dist_minus = None
        min_idx_minus = None
        for j in range(X.shape[0]):
            if alpha[j] >= 0:
                continue
            if min_dist_minus == None:
                min_dist_minus = dist[j][0]
                min_idx_minus = j
            elif min_dist_minus > dist[j]:
                min_dist_minus = dist[j][0]
                min_idx_minus = j

        alpha_minus = np.abs(alpha[min_idx_minus][0])
        total_plus = np.sum(alpha[alpha > 0])
        #print("alpha,beta",x_test, alpha_minus, beta_plus, min_idx_minus, min_dist_minus, min_idx_plus, min_dist_plus)
        t2 = None
        for j in range(X.shape[0]):
            if alpha[j] <= 0:
                continue

            if tighter_bound:
                temp_max = None
                for k in range(X.shape[0]):
                    if alpha[k] >= 0:
                        continue
                    beta_plus = (np.log(np.abs(alpha[k])) - np.log(total_plus)) / self.gamma
                    temp1 = beta_plus + dist[j] - dist[k]
                    temp2 = 2 * np.linalg.norm(X[k, :] - X[j, :])
                    temp = temp1 / temp2
                    #print(x_test, X[j, :], X[k, :], temp, t2)
                    if temp_max == None:
                        temp_max = temp
                    elif temp_max < temp:
                        temp_max = temp
                if t2 == None:
                    t2 = temp_max
                elif t2 > temp_max:
                    t2 = temp_max
            else:
                beta_plus = (np.log(np.abs(alpha_minus)) - np.log(total_plus)) / self.gamma
                temp1 = beta_plus + dist[j] - dist[min_idx_minus]
                temp2 = 2*np.linalg.norm(X[min_idx_minus, :] - X[j, :])
                temp = temp1/temp2
                #print(x_test, X[j, :], X[min_idx_minus, :], temp, t2)
                if t2 == None:
                    t2 = temp
                elif t2 > temp:
                    t2 = temp


        return t2



    def collision_checking(self, X, Y, alpha, X_test, check_radius = True, tight_bound=True):
        f_predict_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_upperbound = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        minus_apha = alpha[alpha < 0]
        plus_apha = alpha[alpha > 0]
        plus_apha_total = np.sum(plus_apha)
        minus_alpha_total = np.abs(np.sum(minus_apha))
        radius = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i, :]
            G_row = np.zeros([X.shape[0], 1])
            total_dist = 0
            for j in range(X.shape[0]):
                if alpha[j] >= 0:
                    continue
                dist = np.linalg.norm(X[j, :] - x_test) ** 2
                total_dist = total_dist + dist * np.abs(alpha[j])
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
                # G_row[j] = self.kernel(X[j, :], x_test, gamma=alpha[j]/plus_alpha_total) if alpha[j] > 0 else 0
            total_dist = total_dist / minus_alpha_total
            # print("total_dist", total_dist)
            f_predict_minus[i][0] = np.max(G_row)  # minus_alpha_total*np.exp(-self.gamma*total_dist)
            minus_idx_min = np.argmax(G_row)
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                if alpha[j] <= 0:
                    continue
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            f_predict_plus[i][0] = np.max(G_row)
            plus_idx_min = np.argmax(G_row)
            f_predict_upperbound[i][0] = plus_apha_total * f_predict_plus[i][0] + alpha[minus_idx_min] * \
                                         f_predict_minus[i][0]
            if check_radius:
                radius[i] = self.check_free_radius(X, alpha, x_test, tighter_bound=tight_bound)

        y_predict = np.sign(f_predict_upperbound)
        print(np.max(f_predict_upperbound), np.min(f_predict_upperbound))
        print(np.max(f_predict_plus), np.min(f_predict_plus), plus_apha_total)
        print(np.max(f_predict_minus), np.min(f_predict_minus), minus_alpha_total)
        # print("radius:", radius)
        return y_predict, f_predict_upperbound, radius


    def check_line(self, X, Y, alpha, A, v, tighter_bound = True):

        x_test = A

        G_row = np.zeros([X.shape[0], 1])
        dist = np.zeros([X.shape[0], 1])
        for j in range(X.shape[0]):
            G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            # print(X[j, :] - x_test)
            dist[j] = np.linalg.norm(X[j, :] - x_test) ** 2

        # print(x_test, t)

        min_dist_minus = None
        min_idx_minus = None
        for j in range(X.shape[0]):
            if alpha[j] >= 0:
                continue
            if min_dist_minus == None:
                min_dist_minus = dist[j][0]
                min_idx_minus = j
            elif min_dist_minus > dist[j]:
                min_dist_minus = dist[j][0]
                min_idx_minus = j

        alpha_minus = np.abs(alpha[min_idx_minus])
        total_plus = np.sum(alpha[alpha > 0])
        # print("alpha,beta",x_test, alpha_minus, beta_plus, min_idx_minus, min_dist_minus, min_idx_plus, min_dist_plus)
        t2 = None
        i_star = None
        j_star = None
        for j in range(X.shape[0]):
            if alpha[j] <= 0:
                continue

            if tighter_bound:
                temp_max = None
                for k in range(X.shape[0]):
                    if alpha[k] >= 0:
                        continue
                    beta_plus = (np.log(np.abs(alpha[k])) - np.log(total_plus)) / self.gamma
                    temp1 = beta_plus + dist[j] - dist[k]
                    # if temp1 <= 0:
                    #    continue
                    x_i = X[j, :]
                    x_j = X[k, :]
                    term3 = -X[k, :] + X[j, :]
                    temp2 = 2 * np.matmul(v, np.transpose(term3))
                    #print("abc1", x_test, X[j, :], X[k, :], temp1, term3, temp2, alpha[k], alpha[j])
                    if temp2 <= 0:
                        temp2 = 0.0000000001

                    temp = temp1 / temp2
                    #print("abc2", i_star, j_star, temp)
                    if temp_max == None:
                        temp_max = temp
                        j_star = k
                    elif temp_max <= temp:
                        temp_max = temp
                        j_star = k
                    #print("abc3", i_star, j_star, temp, temp1, temp2, temp_max)
                if t2 == None:
                    t2 = temp_max
                    i_star = j
                elif t2 >= temp_max:
                    t2 = temp_max
                    i_star = j
            else:
                beta_plus = (np.log(np.abs(alpha_minus)) - np.log(total_plus)) / self.gamma
                temp1 = beta_plus + dist[j] - dist[min_idx_minus]
                temp2 = 2 * np.linalg.norm(X[min_idx_minus, :] - X[j, :])
                term3 = -X[min_idx_minus, :] + X[j, :]
                temp2 = 2 * np.matmul(v, np.transpose(term3))
                # If term2 <=0, there is no limit on t
                if temp2 <= 0:
                    temp2 = 0.00000000001
                temp = temp1 / temp2
                print(x_test, X[j, :], X[min_idx_minus, :], temp, t2)
                if t2 == None:
                    t2 = temp
                    i_star = j
                elif t2 >= temp:
                    t2 = temp
                    i_star = j
        if t2 == None:
            t2 = 1
        #print("i_star:", X[i_star, :])
        #print("j_star:", X[j_star, :])
        return t2

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6]):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots(figsize=(5,5))
        #colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
        label = ['occupied', 'free']
        print(occupied.shape)
        s = [0 if a == 0 else 60 for a in alpha]
        ax.scatter(X[:, 0], X[:, 1], color='b', s=s)

        ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
        ax.scatter(free[:, 0], free[:, 1], color="green", s=20, label="free")
        ax.legend(loc=4, framealpha=1)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        CS = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r")
        ax.clabel(CS, inline=1, fontsize=10)
        #print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
        plt.show()

    # Plotting decision boundary
    def plot_decision_boundary(self, X, Y, alpha, x_min= [-9.5, -10.5], x_max=[7, 6], name = ''):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots(figsize=(5,5))
        #colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
        label = ['occupied', 'free']
        print(occupied.shape)
        s_free = [0 if a >= 0 else 60 for a in alpha]
        s_occ = [0 if a <= 0 else 60 for a in alpha]
        #support_free = ax.scatter(X[:, 0], X[:, 1], color='b', s=s_free)
        #support_occ = ax.scatter(X[:, 0], X[:, 1], color='r', s=s_occ)
        #s = [0 if a == 0 else 60 for a in alpha]
        #ax.scatter(X[:, 0], X[:, 1], color='b', s=s)

        #ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
        #ax.scatter(free[:, 0], free[:, 1], color="green", s=20, label="free")

        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        CS1 = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r")
        #ax.clabel(CS, inline=1, fontsize=10)

        y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, X_grid)
        CS2 = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape),levels=[0],  cmap="Greys_r", linestyles= 'dashed')
        #ax.clabel(CS, inline=1, fontsize=10)

        y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, X, check_radius=True)
        for i in range(len(radius)):
            if radius[i] <= 0 or (i is not 24 and i is not 47):
                continue
            #ax.scatter(X[i,0], X[i,1], color='xkcd:orange', s=80,  marker="^")
            circle = plt.Circle(X[i,:], radius[i], color='xkcd:orange',fill=False, label="free ball")
            #ax.add_artist(circle)
        ax.legend(loc=4, framealpha=1)
        #print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")


        # Check a line
        #A = np.array([2,2])
        A = np.array([0, -0.8])
        tight_bound = True
        show_t2 = True
        for i in range(0, 30):
            #if i is not 9:
            #    continue
            theta = 2*np.pi*i/30
            v = 6*np.array([np.cos(theta), np.sin(theta)])
            B = A + v
            t1 = self.check_line(X,Y,alpha, A, v,tighter_bound = tight_bound)
            t2 = self.check_line(X,Y,alpha, B, -v, tighter_bound = tight_bound)
            t = t1 + t2
            t1 = min(t1,1)
            t2 = min(t2,1)
            t = max(t,t1)
            #p = np.linspace(0,1, 100)
            #line_seg = [A[0] + t*v[0], A[1] + t*v[1]]
            #ax.plot(line_seg[:,0], line_seg[:,1])

            if t1 >0 and t1<1:
                if not tight_bound:
                    scatt1 = ax.scatter(A[0] + t1 * v[0], A[1] + t1 * v[1], color='xkcd:red', s=60, marker="o")
                    if t2 > 0 and t2 < 1 and show_t2:
                        scatt2 = ax.scatter(B[0] -t2*v[0], B[1] - t2*v[1], color='xkcd:red', s=60, marker="x")
            if t>0 and t <1:
                #print(i, t)
                arr1 = ax.arrow(A[0], A[1], v[0] - 0.2*np.cos(theta), v[1] - 0.2*np.sin(theta), head_width=0.2,
                             head_length=0.2, fc="xkcd:green", ec="xkcd:green", linewidth=1.5)
                if tight_bound:
                    scatt1 = ax.scatter(A[0] + t1 * v[0], A[1] + t1 * v[1], color='xkcd:red', s=60, marker="o")
                    if t2 > 0 and t2 < 1 and show_t2:
                        scatt2 = ax.scatter(B[0] -t2 * v[0], B[1] - t2 * v[1], color='xkcd:red', s=60, marker="x")

            if t >=1:
                    # print(i, t)
                    #ax.scatter(A[0] + v[0], A[1] + v[1], color='xkcd:magenta', s=80, marker="o")
                    arr1 = ax.arrow(A[0], A[1], v[0] - 0.2*np.cos(theta), v[1] - 0.2*np.sin(theta), head_width=0.2,
                             head_length=0.2, fc="xkcd:green", ec="xkcd:green", linewidth=1.5)
                #ax.scatter(A[0] + (1-t2) * v[0], A[1] + (1-t2) * v[1], color='xkcd:blue', s=80, marker="o")
                #ax.arrow(A[0], A[1], v[0], v[1], head_width=0.2,
                #          head_length=0.2, fc="xkcd:blue", ec="xkcd:blue")
        h1, _ = CS1.legend_elements()
        h2, _ = CS2.legend_elements()
        ax.xaxis.set_tick_params(labelsize=12)
        ax.yaxis.set_tick_params(labelsize=12)
        ax.set_xlim((x_min[0], x_max[0]))
        ax.set_ylim((x_min[1], x_max[1]))
        #l1 = ax.legend([h1[0], h2[0], arr1, scatt1, scatt2],
        #          ['true boundary', 'inflated boundary',
        #           'line segment AB', 'intersection at $t_{uA}$', 'intersection at $t_{uB}$'], loc=3, framealpha=1,
        #          prop={'size': 10})
        l1 = ax.legend([h1[0], h2[0], arr1],
                  ['true boundary', 'inflated boundary',
                   'line segment AB'], loc=3, framealpha=1,
                  prop={'size': 10})
        l2 = ax.legend([scatt1, scatt2],
                  ['intersection at $t_{uA}$', 'intersection at $t_{uB}$'], loc=4, framealpha=1,
                 prop={'size': 10})
        plt.gca().add_artist(l1)
        plt.savefig(name, bbox_inches='tight', pad_inches=0)
        plt.show()

    def plot_decision_boundary_ball(self, X, Y, alpha, x_min= [-5, -8], x_max=[9, 6], name = ''):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots(figsize=(5,5))
        #colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
        label = ['occupied', 'free']
        print(occupied.shape)
        s = [0 if a == 0 else 60 for a in alpha]
        #ax.scatter(X[:, 0], X[:, 1], color='b', s=s)

        #ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
        #ax.scatter(free[:, 0], free[:, 1], color="green", s=20, label="free")

        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        CS1 = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r", label = "true boundary")
        #ax.clabel(CS, inline=1, fontsize=10)

        y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, X_grid)
        CS2 = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape),levels=[0],  cmap="Greys_r", linestyles= 'dashed', label="upper bound")
        #ax.clabel(CS, inline=1, fontsize=10)

        #y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, X, check_radius=True, tight_bound=True)
        #for i in range(len(radius)):
        #    if radius[i] <= 0 or (i is not 24 and i is not 47):
        #        continue
        #    ax.scatter(X[i,0], X[i,1], color='xkcd:orange', s=80,  marker="^")
        #    circle = plt.Circle(X[i,:], radius[i], color='xkcd:orange',fill=False, label="free ball")
        #    ax.add_artist(circle)
        #ax.legend(loc=4, framealpha=1)

        #####print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        ######ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")


        # Check a line
        #A = np.array([2,2])
        '''
        A = np.array([2, 6])
        B = np.array([-4, 3])
        self.check_line_curve_ball(A, B, X, Y, alpha, ax, color='xkcd:orange')
        A = np.array([1, -1])
        B = np.array([5,2])
        self.check_line_curve_ball(A, B, X, Y, alpha, ax, color='xkcd:blue')
        '''
        A = np.array([0, -4])
        v = np.array([-1.5, -1.2])
        a = np.array([0.15, 0.3])
        ret, ball1 = self.check_second_order_curve(A, v, a, 7, X, Y, alpha, ax, color='xkcd:red')

        A = np.array([3,0])
        v = np.array([-1.5, -1])
        a = np.array([0.155, 0.25])
        ret, ball2 = self.check_second_order_curve(A, v, a, 7, X, Y, alpha, ax, color='xkcd:green')
        ax.xaxis.set_tick_params(labelsize=12)
        ax.yaxis.set_tick_params(labelsize=12)
        ax.set_xlim((x_min[0], x_max[0]))
        ax.set_ylim((x_min[1], x_max[1]))
        h1, _ = CS1.legend_elements()
        h2, _ = CS2.legend_elements()
        l1 = ax.legend([h1[0], h2[0],],
                       ['true boundary', 'inflated boundary'], loc=3, framealpha=1,
                       prop={'size': 10})
        l2 = ax.legend([h1[0], h2[0], ball1, ball2],
                       ['true boundary', 'inflated boundary', 'colliding curve', 'free curve'], loc=4, framealpha=1,
                       prop={'size': 10})
        #plt.gca().add_artist(l1)
        plt.savefig(name, bbox_inches='tight', pad_inches=0)
        plt.show()


    def check_second_order_curve(self, A, v, a, t_max, X, Y, alpha, ax, tighter_bound = True, color='xkcd:red'):
        A = A.reshape([1,2])
        v = v.reshape([1,2])
        a = a.reshape([1,2])
        t_array = np.linspace(0, t_max)
        t_array = t_array.reshape([len(t_array), 1])
        x_t = np.matmul(np.ones([len(t_array), 1]), A) + np.matmul(t_array, v) + np.matmul(np.square(t_array), a)
        ax.plot(x_t[:,0], x_t[:,1], color=color)
        coeff = [1, 0, 0, 0, -4]

        t = 0
        A_t = A + t * v + t*t*a
        y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, A_t, check_radius=True,
                                                               tight_bound=tighter_bound)
        ax.scatter(A_t[0, 0], A_t[0, 1], color=color, s=80, marker="^")
        epsilon = 0.01
        ball_count = 0
        ret = False
        while (radius[0] > epsilon):
            ball_count = ball_count + 1
            print("t= ", t, "r=", radius, "ball number ", ball_count)

            # coeffs
            coeff = np.zeros([1,5])
            A_diff_t = A - A_t
            coeff[0, 0] = np.linalg.norm(a[0, :]) ** 2
            coeff[0, 1] = 2 * (a[0, 0] * v[0, 0] + a[0, 1] * v[0, 1])
            coeff[0, 2] = np.linalg.norm(v[0, :]) ** 2 + 2 * (A_diff_t[0, 0] * a[0, 0] + A_diff_t[0, 1] * a[0, 1])
            coeff[0, 3] = 2 * (A_diff_t[0, 0] * v[0, 0] + A_diff_t[0, 1] * v[0, 1])
            coeff[0, 4] = np.linalg.norm(A_diff_t[0, :]) ** 2 - radius ** 2
            # solve for roots
            coeff_poly = coeff.flatten()
            intersect_t = self.solve_curve_ball_intersection(coeff_poly, t_min=t)
            circle = plt.Circle(A_t[0, :], radius[0], color=color, fill=False, label="free ball", linewidth=1.5)
            ax.add_artist(circle)
            # update t
            if intersect_t is None:
                break
            t = intersect_t
            if (t > t_max):
                ret = True
                break




            A_t = A + t * v + t*t*a
            ax.scatter(A_t[0, 0], A_t[0, 1], color=color, s=80, marker="x")
            y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, A_t, check_radius=True,
                                                                   tight_bound=tighter_bound)
        return ret, circle


        return None

    def solve_curve_ball_intersection(self, coeff, t_min=0):
        root = np.roots(coeff)
        root_complex = np.iscomplex(root)
        root_min = None
        for i in range(len(root)):
            if root_complex[i]:
                continue
            if root[i] > t_min and root_min == None:
                root_min = root[i]
                continue
            if root[i] >  t_min and root_min > root[i]:
                root_min = root[i]
        return np.real(root_min)

    def check_line_curve_ball(self, A, B, X, Y, alpha, ax, tighter_bound = True, color='xkcd:orange'):
        A = A.reshape([1,2])
        B = B.reshape([1,2])
        ax.arrow(A[0, 0], A[0, 1], B[0, 0] - A[0, 0], B[0, 1] - A[0, 1], head_width=0.2,
                 head_length=0.2, fc=color, ec=color)
        t = 0
        A_t = A + t * (B - A)
        AB = np.linalg.norm(A[0, :] - B[0, :])
        y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, A_t, check_radius=True,
                                                               tight_bound=tighter_bound)
        ax.scatter(A_t[0, 0], A_t[0, 1], color=color, s=80, marker="^")
        epsilon = 0.01
        ball_count = 0
        ret = False
        while (radius[0] > epsilon):
            ball_count = ball_count + 1
            print("t= ", t, "r=", radius, "ball number ", ball_count)
            t = t + radius / AB

            circle = plt.Circle(A_t[0, :], radius[0], color=color, fill=False, label="free ball", linewidth=1)
            ax.add_artist(circle)

            if (t > 1):
                ret = True
                break
            A_t = A + t * (B - A)
            ax.scatter(A_t[0, 0], A_t[0, 1], color=color, s=80, marker="x")
            y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, A_t, check_radius=True,
                                                                   tight_bound=tighter_bound)
        return ret


    def predict_probability(self, X, Y, alpha, X_test):
        f_predict_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test)

            f_vec = np.multiply(alpha, G_row)
            f_predict_plus[i][0] = np.sum(np.multiply(f_vec, np.int64(alpha > 0)))
            f_predict_minus[i][0] = -np.sum(np.multiply(f_vec, np.int64(alpha < 0)))
        print(sum(np.int64(alpha > 0 )))
        print(sum(np.int64(alpha < 0)))
        ratio = f_predict_plus / f_predict_minus
        print("ratio" + str(ratio))
        print("max" + str(ratio.max()))
        print("min" + str(ratio.min()))
        f_predict = np.log(ratio) #1/ (1 + f_predict_plus / f_predict_minus)
        print(f_predict.max())
        print(f_predict.min())
        return f_predict

    # Plotting decision boundary
    def plot_probability(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data = True):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        colors = ['red' if l == -1. else 'green' for l in Y]
        if show_data:
            s = [0 if a == 0 else 60 for a in alpha]
            ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
            ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        f_predict = self.predict_probability(X, Y, alpha, X_grid)
        #print(f_predict)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape),  cmap="Greys_r")
        ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
        plt.show()




class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max=100, G=None, alpha=None, F=None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)

        if G is None:
            G = kernels.gram_matrix(X, kernel = self.kernel)
        if F is None:
            F = np.matmul(G, alpha)

        F = np.matmul(G, alpha)
        for iter in range(iter_max):

            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)
            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i][0])
                if y_predict == 0.0:
                    y_predict = 1.0

                if y_predict != Y[i][0]:
                    correct_prediction = False
                    alpha[i][0] += Y[i][0]
                    F = F + Y[i][0] * (G[:, i].reshape([X.shape[0], 1]))

            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break

        return alpha, F

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test)
            f_predict[i][0] = np.sum(np.multiply(alpha, G_row))

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6]):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]

        s = [0 if a == 0 else 60 for a in alpha]
        ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
        ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r")
        ax.set_title("Kernel Perceptron (" + str(self.kernel.__name__) + ")")

        plt.show()







