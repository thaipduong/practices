import numpy as np
import matplotlib.pyplot as plt

prefix = "/home/thaiduong/fastronmap_data/"

ground_truth_map = np.load("/home/thaiduong/ground_truth_map.npy")
cell_pos = np.sum(ground_truth_map > 0)
cell_neg = np.sum(ground_truth_map <= 0)
cell_all = cell_pos + cell_neg

size_data = np.load(prefix + "fastronsize.npz")
support_size = size_data['support_size']
total_size = size_data['total_size']
gmap_observed_size = size_data['gmap_observed_size']
pose_all = size_data['pose_all']
acc_all = size_data['acc_all']
false_pos_all = size_data['false_pos_all']
false_neg_all = size_data['false_neg_all']
support_vec_seq = size_data['support_vec_seq']
check = acc_all - false_neg_all - false_pos_all
print(np.argmin(check))

pose_all[:, 0] = (pose_all[:, 0] + 2.125) / 0.25
pose_all[:, 1] = (pose_all[:, 1] + 12.625) / 0.25

##################### Calculate distance
robot_poses_all = np.load(prefix + "robot_poses.npy")
robot_poses = np.copy(robot_poses_all)
robot_poses[:, 0] = (robot_poses[:, 0] + 2.125) / 0.25
robot_poses[:, 1] = (robot_poses[:, 1] + 12.625) / 0.25
dist_all = []
obs_idx = np.array(np.where(ground_truth_map>0))
obs_idx = np.transpose(obs_idx)
for i in range(len(robot_poses)):
    cur_pos_meter = [robot_poses[i, 0], robot_poses[i, 1]]
    cur_pos_idx = np.array([int(np.around(cur_pos_meter[1])), int(np.around(cur_pos_meter[0]))])
    if cur_pos_idx[0] < 0 or cur_pos_idx[0] >= 128:
        print("WRONG POSE INDEX X")
        print(cur_pos_idx)
    if cur_pos_idx[1] < 0 or cur_pos_idx[1] >= 256:
        print("WRONG POSE INDEX Y")
        print(cur_pos_idx)

    diff = obs_idx - cur_pos_idx
    dist_obs = np.linalg.norm(diff, axis=1)
    dist = 0.25*np.min(dist_obs)
    dist_all.append(dist)

f, ax = plt.subplots(figsize=(8,4))
t = 0.01*np.arange(len(dist_all))
# gmap = np.transpose(gmap)
ax.plot(dist_all, 'r',label="distance to closest obstacle")
ax.plot(0.25*np.ones(len(dist_all)), 'g--', label="robot\'s radius")
ax.set_xlabel("time steps")
ax.set_ylabel("distance (m)")
ax.set_ylim(0,4)
#ax.set_aspect(aspect=0.5)
ax.legend(fancybox=True, framealpha=1, loc = 1, fontsize='large')
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("distance")
# ground_truth = inflate_map(ground_truth)
plt.savefig(prefix + "buildMap/figures/distance_safe_path.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()

fastronmap = np.load(prefix + "fastronmap.npy")
fastronmap = np.transpose(fastronmap)  # fastronmap.reshape([128,256])
fastronmap[fastronmap == 50] = 1
# fastronmap = fastronmap[1:,:]


f, ax = plt.subplots(figsize=(8,4))
# gmap = np.transpose(gmap)
ax.imshow(fastronmap, origin='lower')
ax.set_title("our map")
#plt.show()

upperboundmap = np.load(prefix + "upperbound_map.npy")
upperboundmap = np.transpose(upperboundmap)  # upperboundmap.reshape([128,256])
upperboundmap[upperboundmap == 50] = 1
# upperboundmap = upperboundmap[1:,:]

#dist_all = np.load(prefix + "distance_all.npy")


fastronmap_data = np.load(prefix + "fastronmap.npy")
fastronmap_data = np.transpose(fastronmap)  # fastronmap.reshape([128,256])
fastronmap_data[fastronmap_data == 50] = 1
diff = np.transpose(fastronmap_data) - ground_truth_map
error = np.sum(np.abs(diff))
false_pos = np.sum(np.abs(diff[diff > 0]))
false_neg = np.sum(np.abs(diff[diff <= 0]))
print("false_pos:", false_pos, "false_neg:", false_neg, "error:", error)

f, ax = plt.subplots(figsize=(8,4))
ax.plot(support_size, label='support vectors\'s size')
ax.plot(total_size, label='accumulated training set\'s size')
ax.plot(gmap_observed_size, label='gmapping\'s observed size')
ax.legend()
# plt.show()
# print("Loaded size data")


# plt.show()
# plt.hist(gmap.flatten())
# plt.show()

f, ax = plt.subplots(figsize=(8,4))
# gmap = np.transpose(gmap)
ax.imshow(fastronmap, origin='lower')
ax.set_title("our map")
# plt.show()

f, ax = plt.subplots(figsize=(8,4))
# gmap = np.transpose(gmap)
ax.imshow(upperboundmap, origin='lower')
ax.set_title("our upper bound map")
print("Loaded gmapp")

f, ax = plt.subplots(figsize=(8,4))
# gmap = np.transpose(gmap)
ground_truth_map[ground_truth_map==0] = 0.2
ax.imshow(ground_truth_map, origin='lower', cmap='gray_r', vmin=0, vmax=1)
ax.plot(robot_poses[1:len(robot_poses)-1, 0], robot_poses[1:len(robot_poses)-1, 1], color='b', label="robot path")
ax.scatter(robot_poses[0, 0], robot_poses[0, 1], color='r', s=50, label="start")
ax.scatter(robot_poses[-1, 0], robot_poses[-1, 1], color='g', s=50, label="goal")
#for i in range(len(robot_poses)):
#    circle = plt.Circle([robot_poses[i,0], robot_poses[i,1]], 4*dist_all[i], color='xkcd:pink', fill=True, alpha=0.5)
#    ax.add_artist(circle)

#ax.set_title("ground truth map", fontdict={'fontsize': 20, 'fontweight': 'medium'})
ax.legend(fancybox=True, framealpha=1, loc = 4, fontsize='large')
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
plt.savefig(prefix + "buildMap/figures/safepath_experiment.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()
print("Loaded gmapp")
