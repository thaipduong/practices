import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
import kernels
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

"""
class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max, G = None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)
        print("alpha: \n" + str(alpha))
        if G is None:
            G = kernels.gram_matrix(X, kernel=self.kernel)
        for iter in range(iter_max):
            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)

            for i in ind_list:
                y_predict = np.sign(np.sum(np.multiply(np.multiply(alpha, Y), G[i].reshape([X.shape[0], 1]))))
                # print(y_predict)
                if y_predict == 0.0:
                    y_predict = 1.0
                # print(y_predict)
                # print(Y[i][0])
                if y_predict != Y[i][0]:
                    alpha[i][0] += 1.0

        return alpha

    def predict(self, X, Y, alpha, X1, X2):
        y_predict = np.zeros([X2.shape[0], X1.shape[0]])
        for j in range(X1.shape[0]):
            for k in range(X2.shape[0]):
                x_test = np.array([X1[j], X2[k]])
                G_row = np.zeros([X.shape[0], 1])

                for i in range(X.shape[0]):
                    G_row[i] = self.kernel(X[i, :], x_test)
                G_row.reshape([X.shape[0], 1])
                y_predict[j][k] = np.sum(np.multiply(np.multiply(alpha, Y), G_row))

        return y_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha):
        x1 = np.linspace(-6, 6)
        x2 = np.linspace(-6, 6)
        # plot_data(X,Y)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]
        ax.scatter(X[:, 0], X[:, 1], color=colors)
        ax.contour(x1, x2,
                   self.predict_y(X, Y, alpha, x1, x2), levels=[0], cmap="Greys_r")
        plt.show()
"""


class Fastron(object):
    def __init__(self, kernel = kernels.rbf_kernel, update_argminF=False, remove_redundant=True):
        self.kernel = kernel
        self.update_argminF = update_argminF
        self.remove_redundant = remove_redundant
        self.gamma =2

    def train(self, X, Y, iter_max=2000, G=None, alpha=None, F=None):
        if alpha is None:
            alpha = np.zeros([X.shape[0], 1], dtype=np.float64)

        previous_F = True
        if G is None:
            G = kernels.gram_matrix(X, kernel=self.kernel, gamma=self.gamma)
        if F is None:
            F = np.matmul(G, alpha)
            previous_F = False

        print("iter_max = " + str(iter_max))

        for iter in range(iter_max):
            r_plus = 1.5
            r_minus = 1

            if self.update_argminF and previous_F:
                ind_list = [np.argmin(F * Y)]
            else:
                ind_list = [i for i in range(X.shape[0])]
            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i][0])
                if y_predict == 0.0:
                    y_predict = 1.0

                if y_predict != Y[i][0]:
                    correct_prediction = False
                    r = r_plus if Y[i][0] == 1 else r_minus
                    delta_alpha = r * Y[i][0] - y_predict
                    alpha[i][0] += delta_alpha
                    F = F + delta_alpha * (G[:, i].reshape([X.shape[0], 1]))
                    #print("i = " + str(i))

            # Update support points
            if self.remove_redundant:
                margin = Y * (F - alpha) * np.int64(alpha != 0)

                for m in range(len(margin)):
                    if margin[m] >= -0.:
                        #print("Remove m = " + str(m) + ", margin = " + str(margin[m]) + ", alpha = " + str(
                        #    alpha[m][0]) + ", F = " + str(F[m][0]) + ", Y = " + str(Y[m][0]))
                        F = F - alpha[m][0] * (G[:, m].reshape([X.shape[0], 1]))
                        alpha[m][0] = 0
            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break
        #Print out support points
        margin = Y * (F - alpha) * np.int64(alpha != 0)
        for m in range(len(alpha)):
            if alpha[m][0] != 0:
                print("Support points m = " + str(m) + ", margin = " + str(margin[m]) + ", alpha = " + str(alpha[m][0]) + ", F = " + str(F[m][0]) + ", X = " + str(X[m, :]) + ", Y = " + str(Y[m][0]))

        return alpha, F

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            f_predict[i][0] = np.sum(np.multiply(alpha, G_row))

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    def check_free_radius(self, X, alpha, x_test, tighter_bound = True):
        G_row = np.zeros([X.shape[0], 1])
        dist = np.zeros([X.shape[0], 1])
        t = None
        min_dist_plus = None
        min_idx_plus = None
        for j in range(X.shape[0]):
            G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            #print(X[j, :] - x_test)
            dist[j] = np.linalg.norm(X[j, :] - x_test) ** 2
            if alpha[j] <= 0:
                continue
            if min_dist_plus == None:
                min_dist_plus = dist[j][0]
                min_idx_plus = j
            elif min_dist_plus > dist[j]:
                min_dist_plus = dist[j][0]
                min_idx_plus = j

        for j in range(X.shape[0]):
            if alpha[j] <= 0 or j == min_idx_plus:
                continue
            temp1 = dist[j] - dist[min_idx_plus]
            temp2 = 2*np.linalg.norm(X[j, :] - X[min_idx_plus, :])
            temp = temp1/temp2
            if t == None:
                t = temp
            elif t > temp:
                t = temp
        #print(x_test, t)

        min_dist_minus = None
        min_idx_minus = None
        for j in range(X.shape[0]):
            if alpha[j] >= 0:
                continue
            if min_dist_minus == None:
                min_dist_minus = dist[j][0]
                min_idx_minus = j
            elif min_dist_minus > dist[j]:
                min_dist_minus = dist[j][0]
                min_idx_minus = j

        alpha_minus = np.abs(alpha[min_idx_minus][0])
        beta_plus = np.sum(alpha[alpha > 0])
        #print("alpha,beta",x_test, alpha_minus, beta_plus, min_idx_minus, min_dist_minus, min_idx_plus, min_dist_plus)
        t2 = None
        for j in range(X.shape[0]):
            if alpha[j] <= 0:
                continue

            if tighter_bound:
                temp_max = None
                for k in range(X.shape[0]):
                    if alpha[k] >= 0:
                        continue
                    temp1 = np.log(np.abs(alpha[k])) - np.log(beta_plus) + dist[j] - dist[k]
                    temp2 = 2 * np.linalg.norm(X[k, :] - X[j, :])
                    temp = temp1 / temp2
                    #print(x_test, X[j, :], X[k, :], temp, t2)
                    if temp_max == None:
                        temp_max = temp
                    elif temp_max < temp:
                        temp_max = temp
                if t2 == None:
                    t2 = temp_max
                elif t2 > temp_max:
                    t2 = temp_max
            else:
                temp1 = np.log(alpha_minus) - np.log(beta_plus) + dist[j] - dist[min_idx_minus]
                temp2 = 2*np.linalg.norm(X[min_idx_minus, :] - X[j, :])
                temp = temp1/temp2
                #print(x_test, X[j, :], X[min_idx_minus, :], temp, t2)
                if t2 == None:
                    t2 = temp
                elif t2 > temp:
                    t2 = temp

        temp1 = np.log(alpha_minus) - np.log(beta_plus) + dist[min_idx_plus] - dist[min_idx_minus]
        temp2 = 2*np.linalg.norm(X[min_idx_minus, :] - X[min_idx_plus, :])
        temp = temp1/temp2
        if t> temp:
            t = temp

        return t2



    def collision_checking(self, X, Y, alpha, X_test, check_radius = False):
        f_predict_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_upperbound = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_upperbound_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_upperbound_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)

        minus_apha = alpha[alpha < 0]
        plus_apha = alpha[alpha > 0]
        plus_apha_total = np.sum(plus_apha)
        minus_alpha_total = np.abs(np.sum(minus_apha))
        radius = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            total_dist = 0
            for j in range(X.shape[0]):
                if alpha[j] >= 0:
                    continue
                dist = np.linalg.norm(X[j, :] - x_test)**2
                total_dist = total_dist + dist*np.abs(alpha[j])
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
                #G_row[j] = self.kernel(X[j, :], x_test, gamma=alpha[j]/plus_alpha_total) if alpha[j] > 0 else 0
            total_dist = total_dist / minus_alpha_total
            #print("total_dist", total_dist)
            f_predict_minus[i][0] = np.max(G_row)#minus_alpha_total*np.exp(-self.gamma*total_dist)
            minus_idx_min = np.argmax(G_row)
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                if alpha[j] <= 0:
                    continue
                G_row[j] = self.kernel(X[j, :], x_test, gamma=self.gamma)
            f_predict_plus[i][0] = np.max(G_row)
            plus_idx_min = np.argmax(G_row)
            f_predict_upperbound[i][0] = plus_apha_total*f_predict_plus[i][0] + alpha[minus_idx_min]*f_predict_minus[i][0]
            f_predict_upperbound_plus[i][0] = plus_apha_total*f_predict_plus[i][0]
            f_predict_upperbound_minus[i][0] = -alpha[minus_idx_min]*f_predict_minus[i][0]
            if check_radius:
                radius[i] = self.check_free_radius(X, alpha, x_test)



        y_predict = np.sign(f_predict_upperbound)
        print(np.max(f_predict_upperbound), np.min(f_predict_upperbound))
        print(np.max(f_predict_plus), np.min(f_predict_plus), plus_apha_total)
        print(np.max(f_predict_minus), np.min(f_predict_minus), minus_alpha_total)
        return y_predict, f_predict_upperbound, f_predict_upperbound_plus, f_predict_upperbound_minus, radius

    def belong_to_laser_scan(self, x1, x2):
        if x2 < -6:
            return False
        if x2 > 6:
            return False
        if x2 > 5 and x1 < 1:
            return False
        if x1 < -8:
            return False
        if x1 >8:
            return False
        if x1 == 0:
            return False
        if -4 < x1 and x2 < 1:
            return False
        if x2 == 1 and x1 >0:
            return False
        return True

    def belong_to_laser_scan4(self, x1, x2):
        if x1 == -1:
            return False
        if x1 == -4 and x2 < 2:
            return False
        if x2 < -6:
            return False
        if x2 > 6:
            return False
        if x2 > 5 and x1 < 1:
            return False
        if x1 < -8:
            return False
        if x1 >8:
            return False
        if x1 == 0:
            return False
        if -4 < x1 and x2 < 1:
            return False
        if x2 == 1 and x1 >0:
            return False
        if x2 == -6 and x1 >=-7:
            return False
        if x2 == 1 and -4 < x1:
            return False
        if x2 == 2 and 0 < x1:
            return False
        if x1 == 1 and x2 <6:
            return False
        if x1 == 8 and x2 < 5:
            return False
        return True

    def belong_to_laser_scan2(self, x1, x2):

        if x2 == -9 and -9 < x1 < -7:
            return True
        if x2 == -8 and -10 < x1 < -5:
            return True
        if x2 == -7 and -10 < x1 < -2:
            return True
        if x2 == -6 and -9 < x1 < -0:
            return True
        if x2 == -5 and -9 < x1 < 2:
            return True
        if x2 == -4 and -9 < x1 < 5:
            return True
        if x2 == -3 and -9 < x1 < 5:
            return True
        if x2 == -2 and -9 < x1 < 6:
            return True
        if x2 == -1 and -9 < x1 < 6:
            return True
        if x2 == 0 and -9 < x1 < 7:
            return True
        if x2 == 1 and -9 < x1 < 8:
            return True
        if x2 == 2 and -9 < x1 < 9:
            return True
        if x2 == 3 and -9 < x1 < 9:
            return True
        if x2 == 4 and -9 < x1 < 10:
            return True
        if x2 == 5 and -9 < x1 < 11:
            return True
        if x2 == 6 and -3 < x1 < 11:
            return True
        if x2 == 7 and -4 < x1 < 1:
            return True
        if x2 == 8 and -3 < x1 < 1:
            return True
        if x2 == 7 and  8 < x1 < 11:
            return True
        if x2 == 8 and 9 < x1 < 11:
            return True
        return False

    def belong_to_laser_scan3(self, x1, x2):

        if x2 == -9 and -9 < x1 < -7:
            return True
        if x2 == -8 and -10 < x1 < -5:
            return True
        if x2 == -7 and -10 < x1 < -2:
            return True
        if x2 == -6 and -8 < x1 < -0:
            return True
        if x2 == -5 and -8 < x1 < 2:
            return True
        if x2 == -4 and -8 < x1 < 5:
            return True
        if x2 == -3 and -8 < x1 < 5:
            return True
        if x2 == -2 and -8 < x1 < 6:
            return True
        if x2 == -1 and -8 < x1 < 6:
            return True
        if x2 == 0 and -8 < x1 < 7:
            return True
        if x2 == 1 and -8 < x1 < 8:
            return True
        if x2 == 2 and -8 < x1 < 9:
            return True
        if x2 == 3 and -8 < x1 < 9:
            return True
        if x2 == 4 and -8 < x1 < 10:
            return True
        if x2 == 5 and -2 < x1 < 3:
            return True
        if x2 == 5 and 8 < x1 < 11:
            return True
        if x2 == 6 and -3 < x1 < 1:
            return True
        if x2 == 6 and 8 < x1 < 11:
            return True
        if x2 == 7 and -4 < x1 < 1:
            return True
        if x2 == 8 and -3 < x1 < 1:
            return True
        if x2 == 7 and  8 < x1 < 11:
            return True
        if x2 == 8 and 9 < x1 < 11:
            return True
        return False

    def plot_lidar(self, X, Y, alpha, x_min= [-10, -10], x_max=[10, 10], workspace = True, name=''):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots(figsize=(5,5))
        ax.set_xlim([x_min[0], x_max[0]])
        ax.set_ylim([x_min[1], x_max[1]])
        #colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(X)) if (Y[i] == -1.)])
        label = ['occupied', 'free']
        print(occupied.shape)
        s = [0 if a == 0 else 60 for a in alpha]
        colors = ['b' if a < 0 else 'r' for a in alpha]
        s_free = [0 if a >= 0 else 60 for a in alpha]
        s_occ = [0 if a <= 0 else 60 for a in alpha]
        #support_free = ax.scatter(X[:, 0], X[:, 1], color='b', s=s_free)
        #support_occ = ax.scatter(X[:, 0], X[:, 1], color='r', s=s_occ)
        #laser_free = np.array([free[i,:] for i in range(len(free)) if (free[i,1] > -4 and free[i,1] <4 and free[i,0] > -3 and free[i,0] <4) or (free[i,0] > 4 and free[i,0] <6 and free[i,1] > -1 and free[i,1] <2.5) ])
        s_free = [20 if self.belong_to_laser_scan(a[0], a[1]) else 0 for a in free]
        #ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
        #ax.scatter(free[:, 0], free[:, 1], color="blue", s=s_free, label="free")

        #ax.scatter(laser_free[:, 0], laser_free[:, 1], color="green", s=20, label="free")
        #ax.legend(loc=4, framealpha=1)

        #ax.clabel(CS, inline=1, fontsize=10)
        #print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")

        robot_radius = 1.1
        if workspace:
            robot = plt.Circle([4,-4], robot_radius, color='xkcd:green', fill=True,  alpha=0.5)
            ax.add_artist(robot)
        robot_center = ax.scatter(4, -3.8, marker='^', s=120, color="b", label='robot center')
        #t = np.linspace(-6.5, 4, 100)

        #ax.plot(t, -3.66666 - 0.66666*t/7.33333, linestyle=':', color="g");
        #t = np.linspace(4, 6, 10)

        #ax.plot(t, -4 + 7.33333*(t-4)/2, linestyle=':', color="g");

        #t = np.linspace(0,10, 100)
        max_range = 12.7
        t_max = max_range * np.ones([37, 1])
        t_max[5] = 7.0 + 2.8
        t_max[6] = 7.05 + 2.8
        t_max[7] = 7.0 + 2.8
        t_max[8] = 7.2 + 2.8
        t_max[9] = 7.3 + 2.8
        t_max[10] = 7.5 + 2.8
        t_max[11] = 7.9 + 2.8
        t_max[12] = 8.2 + 2.8
        t_max[13] = 8.7 + 2.8
        t_max[14] = 8.75 + 3.1
        t_max[15] = 8.7 + 2.6

        t_max[16] = 7.5 + 2.8
        t_max[17] = 7.15 + 2.6

        t_max[26] = 8
        t_max[27] = 7.9
        t_max[28] = 7.9
        t_max[29] = 7.9
        t_max[30] = 7.9
        t_max[31] = 8.0
        t_max[32] = 8.1
        t_max[33] = 8.2



        for i in range(0,len(t_max),2):
            theta = 1.05*np.pi - (i-3)*0.65*np.pi/30
            t = np.linspace(0, t_max[i], 100)
            x1 = 4 + np.cos(theta)* t
            x2 = -4 + np.sin(theta)* t
            if i == 0:
                ax.plot(x1, x2, linestyle=':', color="b", label='laser scan')
            else:
                ax.plot(x1, x2, linestyle=':', color="b")
            if t_max[i] < max_range and workspace is not True:
                circle = plt.Circle([x1[-1], x2[-1]], robot_radius, color='xkcd:pink', fill=True, alpha=0.5)
                ax.add_artist(circle)

        t = np.linspace(0, 1, 100)

        x1 = -6*np.ones(t.shape)
        x2 = -4 + t*7
        obs = ax.plot(x1, x2, color="k", linewidth=8, label='obstacle');

        x1 = -6 + 3*t
        x2 = 3*np.ones(t.shape)
        ax.plot(x1, x2, color="k", linewidth=8);

        x2 = 4*np.ones(t.shape)
        x1 = 2.9 + 3.4*t
        ax.plot(x1, x2, color="k", linewidth=8);

        #y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        #CS = ax.contour(x0mesh, x1mesh,
         #          f_predict.reshape(x0mesh.shape), levels=[0], cmap="cool_r", label='boundary')
        #h1, _ = CS.legend_elements()
        #h2, _ = robot.legend_elements()
        #ax.legend([obs, laser, robot, robot_center], ['obstacles', 'laser scans', 'robot', 'robot center'], loc=2, framealpha=1, prop={'size': 9})
        ax.legend(loc=2, framealpha=1, prop={'size': 9})
        #ax.set_rasterized(True)
        ax.xaxis.set_tick_params(labelsize=12)
        ax.yaxis.set_tick_params(labelsize=12)
        ax.legend(loc=4, framealpha=1, prop={'size': 12})
        plt.savefig(name, bbox_inches='tight', pad_inches=0)
        plt.show()

    # Plotting decision boundary
    def plot_data(self, X, Y, alpha, x_min=[-10, -10], x_max=[10, 10], name="", augmented = 0):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots(figsize=(5, 5))
        # colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i, :] for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(X)) if (Y[i] == -1.)])
        label = ['occupied', 'free']
        print(occupied.shape)
        s = [0 if a == 0 else 60 for a in alpha]
        colors = ['b' if a < 0 else 'r' for a in alpha]
        s_free = [0 if a >= 0 else 60 for a in alpha]
        s_occ = [0 if a <= 0 else 60 for a in alpha]
        #support_free = ax.scatter(X[:, 0], X[:, 1], color='b', s=s_free)
        #support_occ = ax.scatter(X[:, 0], X[:, 1], color='r', s=s_occ)
        # laser_free = np.array([free[i,:] for i in range(len(free)) if (free[i,1] > -4 and free[i,1] <4 and free[i,0] > -3 and free[i,0] <4) or (free[i,0] > 4 and free[i,0] <6 and free[i,1] > -1 and free[i,1] <2.5) ])
        if augmented == 3:
            s_free = []
            #[a if self.belong_to_laser_scan3(a[0], a[1]) else 0 for a in free]
            for a in free:
                if self.belong_to_laser_scan3(a[0], a[1]):
                    s_free.append([a[0], a[1]])

        if augmented == 1:
            s_free = []
            for a in free:
                if self.belong_to_laser_scan(a[0], a[1]):
                    s_free.append([a[0], a[1]])

        if augmented == 2:
            s_free = []
            for a in free:
                if self.belong_to_laser_scan2(a[0], a[1]):
                    s_free.append([a[0], a[1]])

        if augmented == 4:
            s_free = []
            for a in free:
                if self.belong_to_laser_scan4(a[0], a[1]):
                    s_free.append([a[0], a[1]])
        s_free = np.array(s_free)
        ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
        if augmented == 4:
            ax.scatter(s_free[:, 0], s_free[:, 1], color="blue", s=20, label="augmented")
        else:
            ax.scatter(s_free[:, 0], s_free[:, 1], color="blue", s=20, label="free")

        t = np.linspace(0, 1, 100)

        x1 = -6*np.ones(t.shape)
        x2 = -4 + t*6.4
        #obs = ax.plot(x1, x2, color="k", linewidth=8, label='obstacle');

        x1 = -6 + 3*t
        x2 = 3*np.ones(t.shape)
        #ax.plot(x1, x2, color="k", linewidth=8);

        x2 = 4 * np.ones(t.shape)
        x1 = 2.9 + 3.4 * t
        #ax.plot(x1, x2, color="k", linewidth=8);
        # ax.scatter(laser_free[:, 0], laser_free[:, 1], color="green", s=20, label="free")
        # ax.legend(loc=4, framealpha=1)

        # ax.clabel(CS, inline=1, fontsize=10)
        # print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        # ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
        robot = ax.scatter(4, -4, marker='^', s=120, color="b", label='robot center')
        ax.set_xlim((x_min[0] - 0.5, x_max[0] + 0.5))
        ax.set_ylim((x_min[1] - 0.5, x_max[1] + 0.5))
        ax.xaxis.set_tick_params(labelsize=12)
        ax.yaxis.set_tick_params(labelsize=12)
        ax.legend(loc=4, framealpha=1, prop={'size': 12})
        plt.savefig(name,bbox_inches='tight', pad_inches = 0)
        plt.show()


    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-10, -10], x_max=[10, 10], name = ''):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots(figsize=(5,5))
        #colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(X)) if (Y[i] == -1.)])
        label = ['occupied', 'free']
        print(occupied.shape)
        s = [0 if a == 0 else 60 for a in alpha]
        colors = ['b' if a < 0 else 'r' for a in alpha]
        s_free = [0 if a >= 0 else 60 for a in alpha]
        s_occ = [0 if a <= 0 else 60 for a in alpha]
        support_free = ax.scatter(X[:, 0], X[:, 1], color='b', s=s_free)
        support_occ = ax.scatter(X[:, 0], X[:, 1], color='r', s=s_occ)
        #laser_free = np.array([free[i,:] for i in range(len(free)) if (free[i,1] > -4 and free[i,1] <4 and free[i,0] > -3 and free[i,0] <4) or (free[i,0] > 4 and free[i,0] <6 and free[i,1] > -1 and free[i,1] <2.5) ])
        s_free = [20 if self.belong_to_laser_scan(a[0], a[1]) else 0 for a in free]
        #ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
        #ax.scatter(free[:, 0], free[:, 1], color="blue", s=s_free, label="free")

        #ax.scatter(laser_free[:, 0], laser_free[:, 1], color="green", s=20, label="free")
        #ax.legend(loc=4, framealpha=1)

        #ax.clabel(CS, inline=1, fontsize=10)
        #print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
        robot = ax.scatter(4, -4, marker='^', s=120, color="b", label='robot center')

        #t = np.linspace(-6.5, 4, 100)

        #ax.plot(t, -3.66666 - 0.66666*t/7.33333, linestyle=':', color="g");
        #t = np.linspace(4, 6, 10)

        #ax.plot(t, -4 + 7.33333*(t-4)/2, linestyle=':', color="g");

        #t = np.linspace(0,10, 100)
        max_range = 12.7
        t_max = max_range * np.ones([33, 1])
        t_max[2] = 7.0 + 2.8
        t_max[3] = 7.05 + 2.8
        t_max[4] = 7.0 + 2.8
        t_max[5] = 7.2 + 2.8
        t_max[6] = 7.3 + 2.8
        t_max[7] = 7.5 + 2.8
        t_max[8] = 7.9 + 2.8
        t_max[9] = 8.2 + 2.8
        t_max[10] = 8.7 + 2.8
        t_max[11] = 8.75 + 3.1
        t_max[12] = 8.7 + 2.6

        t_max[13] = 7.5 + 2.8
        t_max[14] = 7.15 + 2.6

        t_max[23] = 8
        t_max[24] = 7.9
        t_max[25] = 7.9
        t_max[26] = 7.8
        t_max[27] = 7.8
        t_max[28] = 8.0
        t_max[29] = 8.1
        t_max[30] = 8.2
        robot_radius = 1.1
        for i in range(0, len(t_max), 1):
            theta = 1.05*np.pi - i*0.65*np.pi/30
            t = np.linspace(0, t_max[i], 100)
            x1 = 4 + np.cos(theta)* t
            x2 = -4 + np.sin(theta)* t
            #if i == 0:
            #    ax.plot(x1, x2, linestyle=':', color="b", label='laser scan')
            #else:
            #    ax.plot(x1, x2, linestyle=':', color="b")
            #if t_max[i] < max_range:
            #    circle = plt.Circle([x1[-1], x2[-1]], robot_radius, color='xkcd:red', fill=True, alpha=0.5)
            #    ax.add_artist(circle)

        t = np.linspace(0, 1, 100)

        x1 = -6*np.ones(t.shape)
        x2 = -4 + t*6.4
        #obs = ax.plot(x1, x2, color="k", linewidth=8, label='obstacle');

        x1 = -6 + 3*t
        x2 = 3*np.ones(t.shape)
        #ax.plot(x1, x2, color="k", linewidth=8);

        x2 = 4 * np.ones(t.shape)
        x1 = 2.9 + 3.4 * t
        ##ax.plot(x1, x2, color="k", linewidth=8);
        #ax.plot(x1, x2, color="k", linewidth=8);

        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        CS = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="cool_r", label='boundary')
        h1, _ = CS.legend_elements()
        #h2, _ = robot.legend_elements()
        ax.legend([support_free, support_occ, h1[0], robot], ['negative support vectors', 'positive support vectors', 'boundary', 'robot center'], loc=4, framealpha=1, prop={'size': 10})
        ax.xaxis.set_tick_params(labelsize=12)
        ax.yaxis.set_tick_params(labelsize=12)
        plt.savefig(name,bbox_inches='tight', pad_inches = 0)
        plt.show()




    def plot_line_check(self, X, Y, alpha, x_min= [-10, -10], x_max=[10, 10], name = ''):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots(figsize=(5,5))
        #colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
        label = ['occupied', 'free']
        print(occupied.shape)
        #s = [0 if a == 0 else 60 for a in alpha]
        #ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
        laser_free = np.array([free[i,:] for i in range(len(free)) if (free[i,1] > -4 and free[i,1] <4 and free[i,0] > -3 and free[i,0] <4) or (free[i,0] > 4 and free[i,0] <6 and free[i,1] > -1 and free[i,1] <2.5) ])
        #ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20)

        s_free = [0 if a >= 0 else 60 for a in alpha]
        s_occ = [0 if a <= 0 else 60 for a in alpha]
        support_free = ax.scatter(X[:, 0], X[:, 1], color='b', s=s_free)
        support_occ = ax.scatter(X[:, 0], X[:, 1], color='r', s=s_occ)

        #ax.scatter(laser_free[:, 0], laser_free[:, 1], color="green", s=20, label="free")

        #ax.legend(loc=4, framealpha=1)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        CS1 = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r")

        y_predict, f_predict, f_predict_plus, f_predict_minus, radius = self.collision_checking(X, Y, alpha, X_grid)
        CS2 = ax.contour(x0mesh, x1mesh,
                        f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r", linestyles= 'dashed')
        #ax.clabel(CS, inline=1, fontsize=10)
        #print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
        robot = ax.scatter(-1, -1, marker='^', s=100, color="b")

        #plt.legend(framealpha=1)
        #t = np.linspace(-2,1, 100)

        #ax.plot(t, 1 - (t+2), linestyle='--', color="g");

        arr1 = plt.arrow(-1, -1, -3.5, 9, head_width=0.2,
                  head_length=0.2, fc="xkcd:orange", ec="xkcd:orange")
        arr2 = plt.arrow(-1, -1, 9.5, 2, head_width=0.2,
                  head_length=0.2, fc="xkcd:green", ec="xkcd:green")
        #t = np.linspace(4, 6, 10)

        #ax.plot(t, -4 + 7.33333*(t-4)/2, linestyle='--', color="g");
        h1, _ = CS1.legend_elements()
        h2, _ = CS2.legend_elements()
        ax.legend([support_free, support_occ, h1[0], h2[0], arr1, arr2, robot],
                  ['negative support vectors','positive support vectors','perceptron boundary', 'inflated boundary', 'line 1', 'line 2', 'robot center'], loc=4, framealpha=1,
                  prop={'size': 10})
        ax.xaxis.set_tick_params(labelsize=12)
        ax.yaxis.set_tick_params(labelsize=12)
        #plt.ylabel(' ', size='x-large')
        #plt.xlabel(' ', size='x-large')
        plt.savefig(name + "upper_bound_example.pdf", bbox_inches='tight', pad_inches=0)
        plt.show()

    def plot_line_bound(self, X, Y, alpha, name=''):
        plt.figure(figsize=(5,5))
        t = np.linspace(0,1, 100)
        t = t.reshape(len(t), 1)
        x1 = -1- 2.5*t
        x2 = -1+ 10*t
        x_t = np.hstack([x1, x2])
        print(x_t.shape)
        y_predict, f_predict, f_predict_plus, f_predict_minus, radius = self.collision_checking(X, Y, alpha, x_t, check_radius=False)
        plt.plot(t, f_predict_plus - f_predict_minus, label="upper bound $U(x_t)$", linestyle="--")
        f_predict, f_predict_plus, f_predict_minus = self.predict_probability(X, Y, alpha, x_t)
        plt.plot(t, f_predict_plus - f_predict_minus, label="true score $F(x_t)$")
        plt.plot(t, np.zeros(t.shape), linestyle=":", label="zero")
        plt.legend(framealpha=1, prop={'size': 11})
        plt.ylabel('score', size='x-large')
        #plt.xlabel('t', size='x-large')
        plt.savefig(name+"upper_bound_along_path1.pdf",bbox_inches='tight', pad_inches = 0)
        plt.show()

        plt.figure(figsize=(5, 5))
        y_predict, f_predict, f_predict_plus, f_predict_minus, radius = self.collision_checking(X, Y, alpha, x_t,
                                                                                                check_radius=False)
        f_predict = np.log(f_predict_plus) - np.log(f_predict_minus)
        plt.plot(t, f_predict, label="upper bound $U(x_t)$", linestyle="--")
        f_predict, f_predict_plus, f_predict_minus = self.predict_probability(X, Y, alpha, x_t)
        f_predict = np.log(f_predict_plus) - np.log(f_predict_minus)
        plt.plot(t, f_predict, label="true score $F(x_t)$")
        plt.legend(framealpha=1, prop={'size': 10})
        plt.ylabel('score (log scale)', size='x-large')
        #plt.xlabel('t', size='x-large')
        plt.show()
        plt.figure(figsize=(5, 5))
        x1 = -1 + 10.5*t
        x2 = -1 + 3*t
        x_t = np.hstack([x1, x2])
        print(x_t.shape)

        plt.figure(figsize=(5, 5))
        y_predict, f_predict, f_predict_plus, f_predict_minus, radius = self.collision_checking(X, Y, alpha, x_t, check_radius=False)
        plt.plot(t, f_predict_plus - f_predict_minus, label="upper bound $U(x_t)$", linestyle="--")
        f_predict, f_predict_plus, f_predict_minus = self.predict_probability(X, Y, alpha, x_t)
        plt.plot(t, f_predict_plus - f_predict_minus, label="true score $F(x_t)$")
        plt.plot(t, np.zeros(t.shape), linestyle=":", label="zero")
        plt.legend(framealpha=1, prop={'size': 11})
        plt.ylabel('score', size='x-large')
        #plt.xlabel('t', size='x-large')
        plt.savefig(name + "upper_bound_along_path2.pdf", bbox_inches='tight', pad_inches=0)
        plt.show()

        y_predict, f_predict, f_predict_plus, f_predict_minus, radius = self.collision_checking(X, Y, alpha, x_t,
                                                                                                check_radius=False)
        plt.figure(figsize=(5, 5))
        f_predict = np.log(f_predict_plus) - np.log(f_predict_minus)
        plt.plot(t, f_predict, label="upper bound $U(x_t)$", linestyle="--")
        f_predict, f_predict_plus, f_predict_minus = self.predict_probability(X, Y, alpha, x_t)
        f_predict = np.log(f_predict_plus) - np.log(f_predict_minus)
        plt.plot(t, f_predict, label="true score $F(x_t)$")
        plt.legend(framealpha=1, prop={'size': 11})
        plt.ylabel('score (log scale)', size='x-large')
        #plt.xlabel('t', size='x-large')
        plt.show()

    # Plotting decision boundary
    def plot_decision_boundary(self, X, Y, alpha, x_min= [-10, -10], x_max=[10, 10]):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots()
        #colors = ['red' if l == 1. else 'green' for l in Y]
        occupied = np.array([X[i,:]  for i in range(len(Y)) if Y[i] == 1.])
        free = np.array([X[i, :] for i in range(len(Y)) if Y[i] == -1.])
        label = ['occupied', 'free']
        print(occupied.shape)
        s = [0 if a == 0 else 60 for a in alpha]
        ax.scatter(X[:, 0], X[:, 1], color='b', s=s)

        ax.scatter(occupied[:, 0], occupied[:, 1], color="red", s=20,  label="occupied")
        ax.scatter(free[:, 0], free[:, 1], color="green", s=20, label="free")

        y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, X_grid)
        CS = ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape),levels=[0],  cmap="Greys_r")
        ax.clabel(CS, inline=1, fontsize=10)

        y_predict, f_predict, radius = self.collision_checking(X, Y, alpha, X, check_radius=True)
        for i in range(len(radius)):
            if radius[i] <= 0 or (i is not 31 and i is not 66):
                continue
            ax.scatter(X[i,0], X[i,1], color='xkcd:orange', s=80,  marker="^")
            circle = plt.Circle(X[i,:], radius[i], color='xkcd:orange',fill=False, label="free ball")
            ax.add_artist(circle)
        ax.legend(loc=4, framealpha=1)
        #print("f_predict: " + str(f_predict.reshape(len(x0), len(x1))))
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
        plt.show()


    def predict_probability(self, X, Y, alpha, X_test):
        f_predict_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test, self.gamma)

            f_vec = np.multiply(alpha, G_row)
            f_predict_plus[i][0] = np.sum(np.multiply(f_vec, np.int64(alpha > 0)))
            f_predict_minus[i][0] = -np.sum(np.multiply(f_vec, np.int64(alpha < 0)))
        print(sum(np.int64(alpha > 0 )))
        print(sum(np.int64(alpha < 0)))
        ratio = f_predict_plus / f_predict_minus
        print("ratio" + str(ratio))
        print("max" + str(ratio.max()))
        print("min" + str(ratio.min()))
        f_predict = f_predict_plus - f_predict_minus#np.log(ratio) #1/ (1 + f_predict_plus / f_predict_minus)
        print(f_predict.max())
        print(f_predict.min())
        return f_predict, f_predict_plus, f_predict_minus

    # Plotting decision boundary
    def plot_probability(self, X, Y, alpha, x_min= [-10, -10], x_max=[10, 10], fig=None, ax=None, show_data = True):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        colors = ['red' if l == -1. else 'green' for l in Y]
        if show_data:
            s = [0 if a == 0 else 60 for a in alpha]
            ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
            ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        f_predict = self.predict_probability(X, Y, alpha, X_grid)
        #print(f_predict)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape),  cmap="Greys_r")
        ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")
        plt.show()




class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max=100, G=None, alpha=None, F=None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)

        if G is None:
            G = kernels.gram_matrix(X, kernel = self.kernel)
        if F is None:
            F = np.matmul(G, alpha)

        F = np.matmul(G, alpha)
        for iter in range(iter_max):

            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)
            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i][0])
                if y_predict == 0.0:
                    y_predict = 1.0

                if y_predict != Y[i][0]:
                    correct_prediction = False
                    alpha[i][0] += Y[i][0]
                    F = F + Y[i][0] * (G[:, i].reshape([X.shape[0], 1]))

            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break

        return alpha, F

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test)
            f_predict[i][0] = np.sum(np.multiply(alpha, G_row))

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-10, -10], x_max=[10, 10]):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]

        s = [0 if a == 0 else 60 for a in alpha]
        ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
        ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r")
        ax.set_title("Kernel Perceptron (" + str(self.kernel.__name__) + ")")

        plt.show()







