import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

def remove_interior(ground_truth):
    ground_truth[5:8, 59] = 0
    ground_truth[5:8, 63] = 0

    ground_truth[29:32, 59] = 0
    ground_truth[29:32, 63] = 0

    ground_truth[48:51, 82] = 0
    ground_truth[48:51, 86] = 0
    ground_truth[48:51, 89] = 0
    ground_truth[48:51, 93] = 0

    ground_truth[33:38, 64] = 0
    ground_truth[33:38, 84] = 0
    ground_truth[33:38, 92] = 0
    ground_truth[33:38, 112] = 0

    ground_truth[85, 82:85] = 0
    ground_truth[81, 82:85] = 0

    ground_truth[74, 104:107] = 0
    ground_truth[70, 104:107] = 0

    ground_truth[28, 107:117] = 0
    ground_truth[25:32, 110] = 0
    ground_truth[25:32, 113] = 0

    ground_truth[61:66, 64] = 0
    ground_truth[61:66, 84] = 0
    ground_truth[61:66, 92] = 0
    ground_truth[61:66, 112] = 0

    ground_truth[8, 107:117] = 0
    ground_truth[5:12, 110] = 0
    ground_truth[5:12, 113] = 0

    ground_truth[85, 126:133] = 0
    ground_truth[81, 126:133] = 0
    ground_truth[78:89, 129] = 0

    ground_truth[85, 229:236] = 0
    ground_truth[81, 229:236] = 0
    ground_truth[78:89, 232] = 0

    ground_truth[85, 185:188] = 0
    ground_truth[81, 185:188] = 0

    ground_truth[74, 152:159] = 0
    ground_truth[70, 152:159] = 0
    ground_truth[67:78, 155] = 0

    ground_truth[74, 207:210] = 0
    ground_truth[70, 207:210] = 0

    ground_truth[74, 255:261] = 0
    ground_truth[70, 255:261] = 0
    ground_truth[67:78, 258] = 0

    ground_truth[48:51, 144] = 0
    ground_truth[48:51, 148] = 0
    ground_truth[48:51, 151] = 0
    ground_truth[48:51, 155] = 0

    ground_truth[28, 188:198] = 0
    ground_truth[25:32, 191] = 0
    ground_truth[25:32, 195] = 0

    ground_truth[8, 188:198] = 0
    ground_truth[5:12, 191] = 0
    ground_truth[5:12, 195] = 0

    ground_truth[48:51, 228] = 0
    ground_truth[48:51, 232] = 0
    ground_truth[48:51, 236] = 0
    ground_truth[48:51, 239] = 0

    ground_truth[61:66, 149] = 0
    ground_truth[61:66, 168] = 0

    ground_truth[61:66, 233] = 0
    ground_truth[61:66, 253] = 0

    ground_truth[33:38, 233] = 0
    ground_truth[33:38, 253] = 0

    ground_truth[33:38, 149] = 0
    ground_truth[33:38, 168] = 0
    ground_truth[33:38, 177] = 0
    ground_truth[33:38, 196] = 0

    ground_truth[4, 57:67] = 0
    ground_truth[4, 107:117] = 0
    ground_truth[4, 188:198] = 0
    ground_truth[4, 226:231] = 0

    ground_truth[4, 67] = 1
    ground_truth[4, 117] = 1
    ground_truth[4, 198] = 1
    ground_truth[4, 231] = 1

    ground_truth[32, 57:67] = 0
    ground_truth[32, 107:117] = 0
    ground_truth[32, 188:198] = 0
    ground_truth[66, 152:159] = 0
    ground_truth[66, 255:261] = 0
    ground_truth[66, 104:107] = 0
    ground_truth[89, 229:233] = 0

    ground_truth[90:95, 56] = 0
    ground_truth[106:114, 56] = 0



prefix = "/home/thaiduong/fastron_sim_data/data/"


##################################################################################


sampling_check = [[2.83746, 5.55357, 13.3909, 25.3476, 48.8511, 64.3386, 79.8505, 93.3633, 104.996, 115.306, 124.249, 132.246, 139.489 ], \
                  [1.43039, 2.7913, 6.70887, 12.7116, 23.2012, 32.2381, 40.0067, 48.0456, 52.6135, 57.7289, 62.4571, 66.5, 69.9252 ], \
                  [1.03309, 1.88531, 4.53412, 8.53896, 15.5351, 21.4942, 26.6831, 31.2524, 35.2575, 38.8114, 41.7552, 44.4393, 46.8535 ], \
                  [0.757449, 1.44444, 3.42763, 6.47117, 11.7032, 16.2911, 20.1797, 23.6732, 26.4438, 29.1833, 31.6462, 33.1667, 35.0392 ], \
                  [0.606736, 1.14976, 2.71605, 5.15201, 9.36104, 12.9825, 16.3074, 19.0898, 21.2067, 23.2358, 25.4062, 26.7062, 27.9908 ],\
                  [0.424657, 0.770224, 1.78391, 3.34567, 6.05747, 8.39387, 10.4761, 12.2525, 13.9247, 15.5053, 16.7058, 17.6976, 18.7014 ], \
                  [0.315282, 0.578466, 1.45218, 2.52684, 4.68406, 6.43851, 7.98544, 9.34167, 10.5159, 13.3717, 12.3821, 13.2075, 13.9187 ], \
                  [0.101658, 0.152701, 0.305456, 0.541887, 0.956253, 1.32899, 1.62323, 1.8925, 2.13473, 2.34676, 2.53613, 2.69493, 2.84743 ], \
                  [0.0702524, 0.0975088, 0.175775, 0.297055, 0.507848, 0.696536, 0.857148, 1.00226, 1.1249, 1.24645, 1.33738, 1.41856, 1.49961 ]]
our_check =  [12.6342, 13.1852, 13.27, 13.8047, 14.4824, 15.117, 15.5631, 15.8264, 16.4001, 16.3817, 16.364, 16.6675, 16.6462 ]


f, ax = plt.subplots(figsize=(11,5))
length = [0.1, 0.2, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
width = 2
#ax.plot(length, sampling_check[0], 'g--', marker='>', label='$\Delta$ = 0.001m')
ax.plot(length, sampling_check[1], 'g--', marker='^', linewidth=width,markersize=9,label='$\Delta$ = 0.002m')
ax.plot(length, sampling_check[2], 'g--', marker='>', linewidth=width,markersize=9,label='$\Delta$ = 0.003m')
#ax.plot(length, sampling_check[3], 'g--', marker='o',label='$\Delta$ = 0.004m')
ax.plot(length, sampling_check[4], 'g--', marker='x', linewidth=width,markersize=9,label='$\Delta$ = 0.005m')
ax.plot(length, sampling_check[5], 'g--', marker='v', linewidth=width,markersize=9,label='$\Delta$ = 0.01m')
ax.plot(length, sampling_check[6], 'g--', marker='<', linewidth=width,markersize=9,label='$\Delta$ = 0.05m')
ax.plot(length, sampling_check[7], 'g--', marker='o', linewidth=width,markersize=9,label='$\Delta$ = 0.1m')
ax.plot(length, our_check, 'r', marker='o', linewidth=width,markersize=9,label='KM-SA')
ax.set_xlabel("Length(m)", size='x-large')
ax.set_ylabel("time($\mu$s)", size='x-large')
ax.xaxis.set_tick_params(labelsize=16)
ax.yaxis.set_tick_params(labelsize=16)
ax.legend(facecolor='xkcd:silver',fontsize='x-large')
plt.savefig(prefix + "/figures/compare_sampling.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()
##################################################################################
octomap = np.load(prefix + "octomap.npy")
octomap = np.transpose(octomap)
octomap[octomap > 0] = 1
octomap[octomap < 0] = 0
##################################################################################
##################################################################################
A = np.loadtxt(prefix + 'fla_warehouse1.cfg')
A = A.reshape(343, 127, 27)
ground_truth_map = A[:, :, 1]
ground_truth_map = np.transpose(ground_truth_map)
remove_interior(ground_truth_map)
ground_truth = np.copy(ground_truth_map)
#ground_truth = np.zeros([ground_truth_map.shape[0] + 1, ground_truth_map.shape[1]])
#ground_truth[1:,:] = ground_truth_map[:,:]


#plt.show()

##################################################################################
upperboundmap = np.load(prefix + "upperbound_map.npy")
upperboundmap = np.transpose(upperboundmap) #upperboundmap.reshape([128,256])
upperboundmap[upperboundmap == 50] = 1
#upperboundmap = upperboundmap[1:,:]

###################################################################################
fastronmap = np.load(prefix + "fastronmap.npy")
fastronmap = np.transpose(fastronmap)#fastronmap.reshape([128,256])
fastronmap[fastronmap == 50] = 1
#fastronmap = fastronmap[1:,:]

####################################################################################
##################################################################################
upperboundmap_approx = np.load(prefix + "upperbound_approx_map.npy")
upperboundmap_approx = np.transpose(upperboundmap_approx) #upperboundmap.reshape([128,256])
upperboundmap_approx[upperboundmap_approx == 50] = 1
#upperboundmap = upperboundmap[1:,:]

###################################################################################
fastronmap_approx = np.load(prefix + "fastronmap_approx.npy")
fastronmap_approx = np.transpose(fastronmap_approx)#fastronmap.reshape([128,256])
fastronmap_approx[fastronmap_approx == 50] = 1
#fastronmap = fastronmap[1:,:]


def compare(map1, map2):
    error = 0
    for i in range(map1.shape[0]):
        for j in range(map1.shape[1]):
            correct = False
            if map1[i,j] == map2[i, j]:
                correct = True
            else:
                count = 0
                for k in [-1, 0, 1]:
                    for l in [-1, 0, 1]:
                        if 0<= i + k < map1.shape[0] and 0<= j + l < map1.shape[1]:
                            if map1[i,j] == map2[i+k, j+l]:
                                count = count + 1
                if 0< count:
                    correct = True
            if not correct:
                error = error + 1
    return error

def compare_tpr(map1, map2):
    error = 0
    count_true = 0
    for i in range(map1.shape[0]):
        for j in range(map1.shape[1]):
            if map1[i, j] < 0.5:
                continue
            count_true = count_true + 1
            correct = False
            if map1[i,j] == map2[i, j]:
                correct = True
            else:
                count = 0
                for k in [-2, -1, 0, 1, 2]:
                    for l in [-2, -1, 0, 1, 2]:
                        if 0<= i + k < map1.shape[0] and 0<= j + l < map1.shape[1]:
                            if map1[i,j] == map2[i+k, j+l]:
                                count = count + 1
                if 0< count:
                    correct = True
            if not correct:
                error = error + 1
    print("count_true ", count_true)
    return error


#ax.set_title("our map")
#plt.show()

########################OUR MAP VS OCTOMAP #######################################

i = 0
j = 0
fastronmap = fastronmap[5+i:124+i, 4+j:329+j]
#diff = fastronmap - octomap
#error = compare(fastronmap, octomap)#np.sum(np.abs(diff))
fastronmap = fastronmap.astype(float)
fastronmap_approx = fastronmap_approx[5+i:124+i, 4+j:329+j]
#diff = fastronmap - octomap
fastronmap_approx = fastronmap_approx.astype(float)


ground_truth_map = ground_truth_map[3+i:122+i, 8+j:333+j]
ground_truth_map[117, :] = 1
ground_truth_map[:, 322:324] = 1
ground_truth_map = ground_truth_map.astype(float)
true_count = np.sum(ground_truth_map)
error = compare(ground_truth_map, fastronmap)
#diff = ground_truth_map - fastronmap
#error = np.sum(np.abs(diff))
print("fastronmap")
print("error", 100*error/(128*335))

diff = ground_truth_map*(ground_truth_map - fastronmap)
error = compare_tpr(ground_truth_map, fastronmap)#np.sum(np.abs(diff))
print("tpr", 100*error/(true_count))

print("fastronmap_approx")
error = compare(ground_truth_map, fastronmap_approx )#np.sum(np.abs(diff))
print("error", 100*error/(128*335))
diff = ground_truth_map*(ground_truth_map - fastronmap_approx)
error = compare_tpr(ground_truth_map, fastronmap_approx)#error = np.sum(np.abs(diff))
print("tpr", 100*error/(true_count))

error = compare(ground_truth_map, octomap)
#diff = ground_truth_map - octomap
#error = np.sum(np.abs(diff))
print("octomap")
print("error", 100*error/(128*335))
diff = ground_truth_map*(ground_truth_map - octomap)
error = compare_tpr(ground_truth_map, octomap)#error = np.sum(np.abs(diff))
print("tpr", 100*error/(true_count))



upperboundmap = upperboundmap[5+i:124+i, 4+j:329+j]
upperboundmap = upperboundmap.astype(float)
diff  = ground_truth_map - upperboundmap
error = np.sum(np.abs(diff))
print("uppber bound  no SA")
print("error:", 100*error/(128*335))
diff = ground_truth_map*(ground_truth_map - upperboundmap)
error = compare_tpr(ground_truth_map, upperboundmap)#error = np.sum(np.abs(diff))#
print("tpr", 100*error/(true_count))


upperboundmap_approx = upperboundmap_approx[5+i:124+i, 4+j:329+j]
upperboundmap_approx = upperboundmap_approx.astype(float)
diff  = ground_truth_map - upperboundmap_approx
error = np.sum(np.abs(diff))
print("uppber bound SA")
print(100*error/(128*335))
diff = ground_truth_map*(ground_truth_map - upperboundmap_approx)
error = compare_tpr(ground_truth_map, upperboundmap_approx)#error = np.sum(np.abs(diff))#
print("tpr", 100*error/(true_count))
print("true count", true_count)

#########################################################################################
robot_poses_all = np.load(prefix + "robot_poses.npy")
robot_poses = np.copy(robot_poses_all)
#
robot_poses = robot_poses/0.25


#########################################################################################

################################
f, ax = plt.subplots(figsize=(6,4))
#gmap = np.transpose(gmap)
fastronmap_plot = np.copy(fastronmap_approx)
fastronmap_plot[fastronmap_plot==0] = 0.2
ax.imshow(fastronmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,83, 0, 32])
#ax.set_xlabel("length(m)", size='x-large')
#ax.set_ylabel("width(m)", size='x-large')
ax.xaxis.set_tick_params(labelsize=12)
ax.yaxis.set_tick_params(labelsize=12)
plt.savefig(prefix + "/figures/fastron_map.pdf",bbox_inches='tight', pad_inches = 0)
#################################
f, ax = plt.subplots(figsize=(6,4))
ax.imshow(octomap, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,83, 0, 32])
ax.xaxis.set_tick_params(labelsize=12)
ax.yaxis.set_tick_params(labelsize=12)
#################################
f, ax = plt.subplots(figsize=(6,4))
#gmap = np.transpose(gmap)
ground_truth_plot = ground_truth_map.astype(float)
ground_truth_plot[ground_truth_plot==0] = 0.2
print(ground_truth_plot.shape)
ax.imshow(ground_truth_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,83, 0, 32])
ax.plot((83/325)*(39+robot_poses[:,0]), (32/119)*(47+robot_poses[:,1]), color='b')#, label="robot path")
#pose_all = 0.25*pose_all
#ax.plot(pose_all[1:len(pose_all)-1, 0], pose_all[1:len(pose_all)-1, 1], color='b', label="robot path")
ax.scatter((83/325)*(39+robot_poses[0,0]), (32/119)*(47+robot_poses[0,1]), color='r', s=50, label="start")
ax.scatter((83/325)*(39+robot_poses[-1,0]), (32/119)*(47+robot_poses[-1,1]), color='g', s=50, label="goal")
ax.xaxis.set_tick_params(labelsize=12)
ax.yaxis.set_tick_params(labelsize=12)
#ax.set_xlabel("length(m)", size='x-large')
#ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("gmapping")
#ax.set_title("ground truth map")
ax.legend(loc = 3,facecolor='xkcd:silver')#, fontsize='large')
plt.savefig(prefix + "/figures/groundtruth_map.pdf",bbox_inches='tight', pad_inches = 0)
##################################
f, ax = plt.subplots(figsize=(6,4))
#gmap = np.transpose(gmap)
upperboundmap_plot = upperboundmap_approx.astype(float)
upperboundmap_plot[upperboundmap_plot==0] = 0.2
ax.imshow(upperboundmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,83, 0, 32])
ax.xaxis.set_tick_params(labelsize=12)
ax.yaxis.set_tick_params(labelsize=12)
#ax.set_xlabel("length(m)", size='x-large')
#ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("our upper bound map")
plt.savefig(prefix + "/figures/fastron_upperbound.pdf",bbox_inches='tight', pad_inches = 0)
print("Loaded gmapp")
plt.show()
####################################


###################################################################################
support_vec = np.load(prefix + "support_vec_data.npz")
support_points = support_vec['support_vec']
support_weights = support_vec['support_weights']

support_points[:, 0] = (support_points[:, 0] + 2.125)
support_points[:, 1] = (support_points[:, 1] + 12.625)
first_neg_vec = None
first_pos_vec = None
f, ax = plt.subplots(figsize=(8,4))
for i in range(len(support_points)):
    c = 'b' if support_weights[i] < 0 else 'r'
    if first_neg_vec is None and support_weights[i] < 0:
        first_neg_vec = support_points[i, :]
    if first_pos_vec is None and support_weights[i] > 0:
        first_pos_vec = support_points[i, :]
    ax.scatter(support_points[i,0], support_points[i,1], color=c, s = 5)
#ax.scatter(first_neg_vec[0], first_neg_vec[1], color='b', s = 5, label = "negative support vectors")
#ax.scatter(first_pos_vec[0], first_pos_vec[1], color='r', s = 5, label = "positive support vectors")
#ax.legend(loc=2, bbox_to_anchor=(0.16, 0.92), facecolor='xkcd:silver')
#ax.set_xlabel("length(m)", size='x-large')
#ax.set_ylabel("width(m)", size='x-large')
ax.set_facecolor('lightgrey')
plt.savefig(prefix + "/figures/support_vec.pdf",bbox_inches='tight', pad_inches = 0)
#ax.set_ylim(-1, 32)
plt.show()



'''

ground_truth_map = np.load("/home/thaiduong/ground_truth_map.npy")
cell_pos = np.sum(ground_truth_map>0)
cell_neg = np.sum(ground_truth_map<=0)
cell_all = cell_pos + cell_neg


size_data = np.load(prefix + "fastronsize.npz")
support_size = size_data['support_size']
total_size = size_data['total_size']
gmap_observed_size = size_data['gmap_observed_size']
pose_all = size_data['pose_all']
acc_all = size_data['acc_all']
false_pos_all = size_data['false_pos_all']
false_neg_all = size_data['false_neg_all']
support_vec_seq = size_data['support_vec_seq']
check = acc_all - false_neg_all - false_pos_all
print(np.argmin(check))

pose_all[:, 0] = (pose_all[:, 0] + 2.125)/0.25
pose_all[:, 1] = (pose_all[:, 1] + 12.625)/0.25


f, ax = plt.subplots()
ax.plot(1-acc_all/cell_all, label='acc_all')
ax.plot(1-false_pos_all/cell_neg, label='false_pos_all')
ax.plot(1-false_neg_all/cell_pos, label='false_neg_all')
#ax.plot(check, label='check')
ax.legend()
plt.show()

A = np.loadtxt(prefix + 'fla_warehouse1.cfg')
A = A.reshape(343, 127, 27)
ground_truth_map = A[:, :, 1]
ground_truth_map = np.transpose(ground_truth_map)
remove_interior(ground_truth_map)
ground_truth = np.zeros([ground_truth_map.shape[0] + 1, ground_truth_map.shape[1]])
ground_truth[1:,:] = ground_truth_map[:,:]

#ground_truth = inflate_map(ground_truth)



gmap = np.load(prefix + "gmapping.npy")
gmap=np.transpose(gmap)
gmap = gmap[:,64:320]
gmap[gmap <= 50] = 0
gmap[gmap > 50] = 1
#gmap = gmap[1:,:]




upperboundmap = np.load(prefix + "upperbound_map.npy")
upperboundmap = np.transpose(upperboundmap) #upperboundmap.reshape([128,256])
upperboundmap[upperboundmap == 50] = 1
#upperboundmap = upperboundmap[1:,:]

size = gmap.shape[0] * gmap.shape[1]
diff_fastron_gmap = np.sum(np.abs(fastronmap[:,:] - gmap[:,:]))
diff_upperbound_gmap = np.sum(np.abs(upperboundmap[:,:] - gmap[:,:]))
diff_upperbound_fastron = np.sum(np.abs(upperboundmap[:,:] - fastronmap[:,:]))

print("Size: ", size, "Accuracy vs Gmapping: fastron_map:", np.around(100*(1 - diff_fastron_gmap/size), 2), "% upper bound:",
      np.around(100*(1- diff_upperbound_gmap/size), 2), "%", " uppoer vs fastron:", np.around(100*(1 - diff_upperbound_fastron/size), 2), "%")


max_match = 0
max_match_upper = 0
max_match_gmap = 0
max_truncated_groundtruth = None
max_i = 0
ground_truth[5,:] = ground_truth[4, :]
ground_truth[4, :] = 0
ground_truth[122,:] = ground_truth[124,:]
ground_truth[124,:] = 0

for i in range(ground_truth.shape[1]-256):
    truncated_groundtruth = ground_truth[:, i:i+256]
    diff_fastron = np.sum(np.abs(fastronmap[:, :] - truncated_groundtruth[:, :]))
    diff_gmap = np.sum(np.abs(gmap[:, :] - truncated_groundtruth[:, :]))
    diff_upperbound = np.sum(np.abs(upperboundmap[:, :] - truncated_groundtruth[:, :]))
    accuracy = 1 - diff_fastron/size
    if max_match < accuracy:
        max_match = accuracy
        max_match_upper = 1- diff_upperbound/size
        max_match_gmap = 1 - diff_gmap/size
        max_truncated_groundtruth = truncated_groundtruth
        max_i = i



#print("Size: ", size, "max loc:", i, "Accuracy: fastron_map:", np.around(100*max_match, 3), "% upper bound:", np.around(100*max_match_upper, 3), "% gmap: ", np.around(100*max_match_gmap, 3), "%")

ground_truth_map = ground_truth[:,max_i:max_i+256]
ground_truth_map[5, 20:30] = 0
ground_truth_map[5, 70:80] = 0
ground_truth_map[5, 151:161] = 0
ground_truth_map[5, 189:194] = 0
ground_truth_map[67, 67:70] = 0
ground_truth_map[67, 218:224] = 0

diff_fastron_groundtruth = np.sum(np.abs(fastronmap[:,:] - ground_truth_map[:,:]))
diff_gmap_groundtruth = np.sum(np.abs(gmap[:,:] - ground_truth_map[:,:]))
diff_upperbound_groundtruth = np.sum(np.abs(upperboundmap[:,:] - ground_truth_map[:,:]))
print("Size: ", size, "Accuracy vs Gmapping: fastron_map:", np.around(100*(1 - diff_fastron_gmap/size), 2), "% upper bound:",
      np.around(100*(1- diff_upperbound_gmap/size), 2), "%", " upper vs fastron:", np.around(100*(1 - diff_upperbound_fastron/size), 2), "%",
      "fastron vs ground truth:",  np.around(100*(1 - diff_fastron_groundtruth/size), 2), "%","gmap vs ground truth:",  np.around(100*(1 - diff_gmap_groundtruth/size), 2), "%"
,"diff_upperbound_groundtruth vs ground truth:",  np.around(100*(1 - diff_upperbound_groundtruth/size), 2), "%")

print(1-np.sum(ground_truth_map)/size)
np.save("/home/thaiduong/ground_truth_map.npy", ground_truth_map)

fastronmap_data = np.load(prefix + "fastronmap.npy")
fastronmap_data = np.transpose(fastronmap)#fastronmap.reshape([128,256])
fastronmap_data[fastronmap_data == 50] = 1
diff = np.transpose(fastronmap_data) - ground_truth_map
error = np.sum(np.abs(diff))
false_pos = np.sum(np.abs(diff[diff > 0]))
false_neg = np.sum(np.abs(diff[diff <= 0]))
print("false_pos:", false_pos, "false_neg:", false_neg, "error:", error)

f, ax = plt.subplots(figsize=(8,4))
ax.plot(support_size, 'g',label='number of support vectors', linewidth=2)
#ax.plot(total_size, label='accumulated training set\'s size')
ax.plot(gmap_observed_size, 'r--', label='number of gmapping\'s observed cells', linewidth=2)
ax.set_xlabel("time steps", size='x-large')
ax.set_ylabel("count", size='x-large')
ax.legend(loc = 2, fontsize='large')
plt.savefig(prefix + "/figures/support_vec_count.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()
#print("Loaded size data")


f, ax = plt.subplots(figsize=(8,4))
gmap_plot = gmap.astype(float)
gmap_plot[gmap_plot==0] = 0.2
ax.imshow(gmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,64, 0, 32])
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("gmapping")
plt.savefig(prefix + "/figures/gmap.pdf",bbox_inches='tight', pad_inches = 0)
#plt.show()
#plt.hist(gmap.flatten())
plt.show()








f, ax = plt.subplots(figsize=(8,4))
#gmap = np.transpose(gmap)
ground_truth_plot = ground_truth_map.astype(float)
ground_truth_plot[ground_truth_plot==0] = 0.2
ax.imshow(ground_truth_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,64, 0, 32])
pose_all = 0.25*pose_all
ax.plot(pose_all[1:len(pose_all)-1, 0], pose_all[1:len(pose_all)-1, 1], color='b', label="robot path")
ax.scatter(pose_all[0, 0], pose_all[0, 1], color='r', s=50, label="start")
ax.scatter(pose_all[-1, 0], pose_all[-1, 1], color='g', s=50, label="goal")
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("gmapping")
#ax.set_title("ground truth map")
plt.savefig(prefix + "/figures/groundtruth_map.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()
print("Loaded gmapp")



'''