import numpy as np

def linear_kernel(x1, x2, gamma = 1):
    return np.inner(x1, x2) + 1

def rbf_kernel(x1, x2, gamma = 4):
    k = np.exp(-gamma*(np.linalg.norm(x1 - x2)**2))
    return k

def gram_matrix(X, kernel = rbf_kernel, gamma = 1):
    G = np.zeros([X.shape[0], X.shape[0]])
    for i in range (X.shape[0]):
        for j in range (X.shape[0]):
            G[i][j] = kernel(X[i,:], X[j,:], gamma=gamma)
    print("G: " + str(G))
    return G