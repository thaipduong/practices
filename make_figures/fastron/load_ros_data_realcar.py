import numpy as np
import matplotlib.pyplot as plt

prefix = "/home/thaiduong/fastronmap_data/succeededrosbag/test6/data/"





##############################
support_vec = np.load(prefix + "support_vec_data.npz")
support_points = support_vec['support_vec']
support_weights = support_vec['support_weights']

support_points[:, 0] = (support_points[:, 0] + 2.125)
support_points[:, 1] = (support_points[:, 1] + 9.625)
#support_points = np.flip(support_points, axis = 1)
first_neg_vec = None
first_pos_vec = None
f, ax = plt.subplots(figsize=(7.5,5))
for i in range(len(support_points)):
    if 0<= support_points[i, 0] <= 1000:
        if 0 <= support_points[i,1] <= 1000:
            c = 'b' if support_weights[i] < 0 else 'r'
            if first_neg_vec is None and support_weights[i] < 0:
                first_neg_vec = support_points[i, :]
            if first_pos_vec is None and support_weights[i] > 0:
                first_pos_vec = support_points[i, :]
            ax.scatter(48-2*9.625-support_points[i,1], support_points[i,0], color=c, s = 3)
ax.scatter(48-2*9.625-first_neg_vec[1], first_neg_vec[0], color='b', s = 5, label = "negative support vectors")
ax.scatter(48-2*9.625-first_pos_vec[1], first_pos_vec[0], color='r', s = 5, label = "positive support vectors")
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_xticklabels([35, 30, 25, 20, 15, 10, 5, 0])
ax.legend(loc=3, facecolor='xkcd:silver', fontsize='large')
plt.savefig(prefix + "figures/realcar_supportvec.pdf",bbox_inches='tight', pad_inches = 0)
#ax.set_ylim(-1, 32)
plt.show()




ground_truth_map = np.load("/home/thaiduong/ground_truth_map.npy")
cell_pos = np.sum(ground_truth_map>0)
cell_neg = np.sum(ground_truth_map<=0)
cell_all = cell_pos + cell_neg


size_data = np.load(prefix + "fastronsize.npz")
support_size = size_data['support_size']
total_size = size_data['total_size']
pose_all = size_data['pose_all']
support_vec_seq = size_data['support_vec_seq']
support_time = size_data['support_time']

pose_all[:, 0] = (pose_all[:, 0] + 2.125)/0.25
pose_all[:, 1] = (pose_all[:, 1] + 9.625)/0.25






fastronmap = np.load(prefix + "fastronmap.npy")
fastronmap = np.transpose(fastronmap)#fastronmap.reshape([128,256])
fastronmap[fastronmap == 50] = 1
#fastronmap = fastronmap[1:,:]


robot_poses_all = np.load(prefix + "robot_poses.npy")
robot_poses = np.copy(robot_poses_all)
#robot_poses[:, 0] = (robot_poses[:, 0] + 2.125) / 0.25
#robot_poses[:, 1] = (robot_poses[:, 1] + 9.625) / 0.25
dist_all = []
obs_idx = np.array(np.where(ground_truth_map>0))
obs_idx = np.transpose(obs_idx)
for i in range(len(robot_poses)):
    cur_pos_meter = [robot_poses[i, 0], robot_poses[i, 1]]
    cur_pos_idx = np.array([int(np.around(cur_pos_meter[1])), int(np.around(cur_pos_meter[0]))])
    if cur_pos_idx[0] < 0 or cur_pos_idx[0] >= 192:
        print("WRONG POSE INDEX X")
        print(cur_pos_idx)
    if cur_pos_idx[1] < 0 or cur_pos_idx[1] >= 128:
        print("WRONG POSE INDEX Y")
        print(cur_pos_idx)


f, ax = plt.subplots(figsize=(7.5,5))
#gmap = np.transpose(gmap)
fastronmap_plot = np.transpose(fastronmap)
fastronmap_plot[fastronmap_plot==0] = 0.0
fastronmap_plot = np.flip(fastronmap_plot, axis = 1)
ax.imshow(fastronmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[-2*9.625, 48-2*9.625, -2.125,32-2.125])
ax.plot(48-3*9.625-robot_poses[1:len(robot_poses)-1, 1], robot_poses[1:len(robot_poses)-1, 0], color='b', label="robot path")
ax.scatter(48-3*9.625-robot_poses[0, 1], robot_poses[0, 0], color='r', s=50, label="start")
ax.scatter(48-3*9.625-robot_poses[-1, 1], robot_poses[-1, 0], color='g', s=50, label="end")
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_xticklabels([35, 30, 25, 20, 15, 10, 5, 0])
#ax.set_title("our map")
ax.legend(loc = 3,facecolor='xkcd:silver', fontsize='large')
plt.savefig(prefix + "figures/realcar_fastronmap.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()

upperboundmap = np.load(prefix + "upperbound_map.npy")
upperboundmap = np.transpose(upperboundmap) #upperboundmap.reshape([128,256])
upperboundmap[upperboundmap == 50] = 1
#upperboundmap = upperboundmap[1:,:]


fastronmap_data = np.load(prefix + "fastronmap.npy")
fastronmap_data = np.transpose(fastronmap)#fastronmap.reshape([128,256])
fastronmap_data[fastronmap_data == 50] = 1



f, ax = plt.subplots(figsize=(7.5,5))
#gmap = np.transpose(gmap)
upperboundmap_plot = np.transpose(upperboundmap)
upperboundmap_plot[upperboundmap_plot==0] = 0.2
upperboundmap_plot = np.flip(upperboundmap_plot, axis = 1)
ax.imshow(upperboundmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[-2*9.625, 48-2*9.625, -2.125,32-2.125])
ax.plot(48-3*9.625-robot_poses[1:len(robot_poses)-1, 1], robot_poses[1:len(robot_poses)-1, 0], color='b', label="robot path")
ax.scatter(48-3*9.625-robot_poses[0, 1], robot_poses[0, 0], color='r', s=50, label="start")
ax.scatter(48-3*9.625-robot_poses[-1, 1], robot_poses[-1, 0], color='g', s=50, label="end")
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_xticklabels([35, 30, 25, 20, 15, 10, 5, 0])
#ax.set_title("our map")
ax.legend(loc = 3,facecolor='xkcd:silver', fontsize='large')
plt.savefig(prefix + "figures/realcar_upperbound.pdf",bbox_inches='tight', pad_inches = 0)




f, ax = plt.subplots(figsize=(7.5,5))
ax.plot(support_time/2, support_size, 'g',label='number of support vectors', linewidth = 4)
ax.set_xlabel("time(s)", size='x-large')
ax.set_ylabel("count", size='x-large')
ax.legend(facecolor='xkcd:silver',fontsize='large')
plt.savefig(prefix + "figures/realcar_supportvec_size.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()

f, ax = plt.subplots(figsize=(7.5,5))
for i in range(0):#len(support_size)):
    print(i, len(support_size), support_time[i])
    ax.clear()
    ax.plot(support_time[0:i]/2, support_size[0:i], 'g',label='number of support vectors', linewidth = 4)
    ax.set_xlabel("time(s)", size='x-large')
    ax.set_ylabel("count", size='x-large')
    ax.legend(facecolor='xkcd:silver',fontsize='large')
    plt.savefig(prefix + "figures/support_size/realcar_supportvec_size"+ str(i) + ".png",bbox_inches='tight', pad_inches = 0)
    plt.pause(0.1)
plt.show()


astar_data = np.load(prefix + "astar.npz")
astar_info = astar_data['astar_info']
astar_time = astar_data['astar_time']
avg_time = 1000*astar_info[:,0]/astar_info[:,2]
#avg_time[avg_time>4.0] = 4.0
f, ax = plt.subplots(figsize=(7.5,5))
ax.plot(astar_time/2, avg_time, 'g',label='A* time per expanded node')
ax.set_xlabel("time(s)", size='x-large')
ax.set_ylabel("time(ms)", size='x-large')
ax.legend(facecolor='xkcd:silver',fontsize='large')
plt.savefig(prefix + "figures/realcar_astartime_pernode.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()

f, ax = plt.subplots(figsize=(7.5,5))
ax.plot(astar_time/2, astar_info[:,0], 'g',label='A* time')
ax.set_xlabel("time(s)", size='x-large')
ax.set_ylabel("time(s)", size='x-large')
ax.legend(facecolor='xkcd:silver',fontsize='large')
plt.savefig(prefix + "figures/realcar_astartime.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()



support_data = np.load(prefix + "support_stats.npz")
support_info = support_data['support_info']
stime = support_data['support_time']
#avg_time = 1000*astar_info[:,0]/astar_info[:,2]
#avg_time[avg_time>4.0] = 4.0
f, ax = plt.subplots(figsize=(7.5,5))
ax.plot(stime/2, support_info[:,0], 'g',label='Map update time')
ax.set_xlabel("time(s)", size='x-large')
ax.set_ylabel("time(s)", size='x-large')
ax.legend(facecolor='xkcd:silver',fontsize='large')
plt.savefig(prefix + "figures/realcar_map_update.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()