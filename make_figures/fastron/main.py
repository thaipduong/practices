import numpy as np
import datagen
import perceptron
import kernels

# Parameters
# Environment Size
x_min = [-10, -10]
x_max = [10,10]
# Training test size
data_points_count = 400
# Generate grid environment
grid = True
grid_size = [21, 21]
# Fastron params for rbf_kernel only
update_argminF = True
remove_redundant = True
# iteration max
iter_max = 200
"""
########################################################################################################################
# Fastron with linear kernel
classifier = perceptron.Fastron(kernel=kernels.linear_kernel)
#classifier = perceptron.KernelPerceptron(kernel=kernels.linear_kernel)
######################################################################
# Linear data (slope = 1, offset = 2). Good boundary
X, Y = datagen.gen_linear_data(a = 1, b = 2, x_min=x_min, x_max=x_max,
                               data_points_count = data_points_count,
                               grid = grid, grid_size = grid_size)
alpha1, F1 =  classifier.train(X, Y, iter_max=iter_max)
print(alpha1.reshape(1,alpha1.shape[0]))
classifier.plot_boundary(X, Y, alpha1)

######################################################################
# Data from environment with obstacle(s). Bad boundary
# Obstacle limits [x0_min, x0_max, x1_min, x1_max]
obs_list = np.array([[-3, -1, -3, 5]])

X, Y = datagen.gen_obstacle_data(obs_list, x_min=x_min, x_max=x_max,
                                 data_points_count = data_points_count,
                                 grid = grid, grid_size = grid_size)
alpha1, F1 = classifier.train(X, Y, iter_max=iter_max)
print(alpha1.reshape(1,alpha1.shape[0]))
classifier.plot_boundary(X, Y, alpha1)

# Obstacle(s) changed. Still bad boundary
obs_list = np.array([[0, 5, 2, 4], [-5, -1, -3, -1]])
Y = datagen.get_label(X, obs_list)
alpha2, F2 = classifier.train(X, Y, iter_max=iter_max)
print(alpha2.reshape(1,alpha2.shape[0]))
classifier.plot_boundary(X, Y, alpha2)

"""
########################################################################################################################
# Fastron with gaussian kernel
classifier = perceptron.Fastron(kernel=kernels.rbf_kernel,
                                update_argminF=update_argminF,
                                remove_redundant=remove_redundant)
#classifier = perceptron.KernelPerceptron(kernel=kernels.rbf_kernel)
######################################################################
# Linear data (slope = 1, offset = 0). Good boundary
#X, Y = datagen.gen_linear_data(a = 1, b = 2, x_min=x_min, x_max=x_max,
#                               data_points_count = data_points_count,
#                               grid = grid, grid_size = grid_size)
#alpha1, F1 =  classifier.train(X, Y, iter_max=iter_max)
#print(alpha1.reshape(1,alpha1.shape[0]))
#classifier.plot_boundary(X, Y, alpha1)
#classifier.plot_decision_boundary(X, Y, alpha1)
#classifier.plot_probability(X, Y, alpha1)

######################################################################
# Data from environment with obstacle(s). Good boundary
# Obstacle limits [x0_min, x0_max, x1_min, x1_max]
obs_list = np.array([[-3, -1, -3, 5]])

X, Y = datagen.gen_obstacle_data(obs_list, x_min=x_min, x_max=x_max,
                                 data_points_count = data_points_count,
                                 grid = grid, grid_size = grid_size)
#alpha1, F1 = classifier.train(X, Y, iter_max=iter_max)
#print(alpha1.reshape(1,alpha1.shape[0]))
#classifier.plot_boundary(X, Y, alpha1)
#classifier.plot_decision_boundary(X, Y, alpha1)

# Obstacle(s) changed. Still good boundary.
# Note that Fastron is able to simplify the set of support points as the environment changes.
obs_list = np.array([[-8, -4, -6, 2], [-8, -1, 1, 5], [1, 8, 2, 6]])
Y = datagen.get_label(X, obs_list)
alpha2, F2 = classifier.train(X, Y, iter_max=iter_max)
print(alpha2.reshape(1,alpha2.shape[0]))

#obs_list = np.array([[-4, -3, -4, 2], [-4, -1, 1, 3], [1, 5, 2, 4]])
#obs_list = np.array([[-4, -3, -4, 2], [-4, 0, 1, 3], [1, 5, 2, 4]])
#obs_list = np.array([[-5, -1, -5, 1], [-5, 0, 0, 4], [0, 7, 1, 5]])
#Y = datagen.get_label(X, obs_list)
alpha2, F2 = classifier.train(X, Y, iter_max=iter_max)
classifier.plot_boundary(X, Y, alpha2, name='/home/thaiduong/fastronmap_data/technicalApproach/support_vecs_examples.pdf')
classifier.plot_data(X, Y, alpha2, name='/home/thaiduong/fastronmap_data/technicalApproach/samples_from_lidar_orig.pdf', augmented = 3)
classifier.plot_data(X, Y, alpha2, name='/home/thaiduong/fastronmap_data/technicalApproach/samples_from_lidar_augmented.pdf', augmented = 4)
classifier.plot_data(X, Y, alpha2, name='/home/thaiduong/fastronmap_data/technicalApproach/samples_from_lidar.pdf', augmented = 2)
classifier.plot_lidar(X, Y, alpha2, name='/home/thaiduong/fastronmap_data/technicalApproach/lidar_example.pdf')
classifier.plot_lidar(X, Y, alpha2, workspace=False, name='/home/thaiduong/fastronmap_data/technicalApproach/lidar_cspace_example.pdf')


classifier.plot_line_bound(X, Y, alpha2, name='/home/thaiduong/fastronmap_data/technicalApproach/')
classifier.plot_line_check(X, Y, alpha2, name='/home/thaiduong/fastronmap_data/technicalApproach/')
#classifier.plot_boundary(X, Y, alpha2)
#classifier.plot_decision_boundary(X, Y, alpha2)
#classifier.plot_probability(X, Y, alpha2)







