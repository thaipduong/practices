import numpy as np
import matplotlib.pyplot as plt

prefix = "/home/thaiduong/fastronmap_data/buildMap/"


def remove_interior(ground_truth):
    ground_truth[5:8, 59] = 0
    ground_truth[5:8, 63] = 0

    ground_truth[29:32, 59] = 0
    ground_truth[29:32, 63] = 0

    ground_truth[48:51, 82] = 0
    ground_truth[48:51, 86] = 0
    ground_truth[48:51, 89] = 0
    ground_truth[48:51, 93] = 0

    ground_truth[33:38, 64] = 0
    ground_truth[33:38, 84] = 0
    ground_truth[33:38, 92] = 0
    ground_truth[33:38, 112] = 0

    ground_truth[85, 82:85] = 0
    ground_truth[81, 82:85] = 0

    ground_truth[74, 104:107] = 0
    ground_truth[70, 104:107] = 0

    ground_truth[28, 107:117] = 0
    ground_truth[25:32, 110] = 0
    ground_truth[25:32, 113] = 0

    ground_truth[61:66, 64] = 0
    ground_truth[61:66, 84] = 0
    ground_truth[61:66, 92] = 0
    ground_truth[61:66, 112] = 0

    ground_truth[8, 107:117] = 0
    ground_truth[5:12, 110] = 0
    ground_truth[5:12, 113] = 0

    ground_truth[85, 126:133] = 0
    ground_truth[81, 126:133] = 0
    ground_truth[78:89, 129] = 0

    ground_truth[85, 229:236] = 0
    ground_truth[81, 229:236] = 0
    ground_truth[78:89, 232] = 0

    ground_truth[85, 185:188] = 0
    ground_truth[81, 185:188] = 0

    ground_truth[74, 152:159] = 0
    ground_truth[70, 152:159] = 0
    ground_truth[67:78, 155] = 0

    ground_truth[74, 207:210] = 0
    ground_truth[70, 207:210] = 0

    ground_truth[74, 255:261] = 0
    ground_truth[70, 255:261] = 0
    ground_truth[67:78, 258] = 0

    ground_truth[48:51, 144] = 0
    ground_truth[48:51, 148] = 0
    ground_truth[48:51, 151] = 0
    ground_truth[48:51, 155] = 0

    ground_truth[28, 188:198] = 0
    ground_truth[25:32, 191] = 0
    ground_truth[25:32, 195] = 0

    ground_truth[8, 188:198] = 0
    ground_truth[5:12, 191] = 0
    ground_truth[5:12, 195] = 0

    ground_truth[48:51, 228] = 0
    ground_truth[48:51, 232] = 0
    ground_truth[48:51, 236] = 0
    ground_truth[48:51, 239] = 0

    ground_truth[61:66, 149] = 0
    ground_truth[61:66, 168] = 0

    ground_truth[61:66, 233] = 0
    ground_truth[61:66, 253] = 0

    ground_truth[33:38, 233] = 0
    ground_truth[33:38, 253] = 0

    ground_truth[33:38, 149] = 0
    ground_truth[33:38, 168] = 0
    ground_truth[33:38, 177] = 0
    ground_truth[33:38, 196] = 0

    ground_truth[4, 57:67] = 0
    ground_truth[4, 107:117] = 0
    ground_truth[4, 188:198] = 0
    ground_truth[4, 226:231] = 0

    ground_truth[4, 67] = 1
    ground_truth[4, 117] = 1
    ground_truth[4, 198] = 1
    ground_truth[4, 231] = 1

    ground_truth[32, 57:67] = 0
    ground_truth[32, 107:117] = 0
    ground_truth[32, 188:198] = 0
    ground_truth[66, 152:159] = 0
    ground_truth[89, 229:233] = 0

    ground_truth[90:95, 56] = 0
    ground_truth[106:114, 56] = 0

def erase_box(ground_truth, xmin, xmax, ymin, ymax):
    ground_truth[xmin:xmax+1, ymin:ymax+1] = 0

#def remove_iterior

def inflate_map(ground_truth, radius=1):
    inflated_map = np.copy(ground_truth)
    for i in range(ground_truth.shape[0]):
        for j in range(ground_truth.shape[1]):
            if ground_truth[i,j] == 1:
                for k in range(-radius, radius + 1):
                    for l in range(-radius, radius + 1):
                        if 0<= i+k < ground_truth.shape[0] and 0<= j+l < ground_truth.shape[1]:
                            inflated_map[i+k, j+l] = 1
    # Remove interior
    inflated_map2 = np.copy(inflated_map)
    for i in range(ground_truth.shape[0]):
        for j in range(ground_truth.shape[1]):
            if inflated_map[i,j] == 1:
                interior = True
                for k in range(-1, 1 + 1):
                    for l in range(-1, 1 + 1):
                        if 0 <= i + k < ground_truth.shape[0] and 0 <= j + l < ground_truth.shape[1]:
                            if inflated_map[i+k, j+l] == 0:
                                interior = False
                if interior:
                    inflated_map2[i,j] = 0
    #np.savetxt(prefix + "inflated_groundtruth.gz", inflated_map, fmt='%d')
    return inflated_map






##############################
support_vec = np.load(prefix + "support_vec_data.npz")
support_points = support_vec['support_vec']
support_weights = support_vec['support_weights']

support_points[:, 0] = (support_points[:, 0] + 2.125)
support_points[:, 1] = (support_points[:, 1] + 12.625)
first_neg_vec = None
first_pos_vec = None
f, ax = plt.subplots(figsize=(8,4))
for i in range(len(support_points)):
    if 0<= support_points[i, 0] <= 64:
        if 0 <= support_points[i,1] <= 32:
            c = 'b' if support_weights[i] < 0 else 'r'
            if first_neg_vec is None and support_weights[i] < 0:
                first_neg_vec = support_points[i, :]
            if first_pos_vec is None and support_weights[i] > 0:
                first_pos_vec = support_points[i, :]
            ax.scatter(support_points[i,0], support_points[i,1], color=c, s = 5)
#ax.scatter(first_neg_vec[0], first_neg_vec[1], color='b', s = 5, label = "negative support vectors")
#ax.scatter(first_pos_vec[0], first_pos_vec[1], color='r', s = 5, label = "positive support vectors")
#ax.legend(loc=2, bbox_to_anchor=(0.16, 0.92), facecolor='xkcd:silver')
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
ax.set_facecolor('lightgrey')
plt.savefig(prefix + "/figures/support_vec.pdf",bbox_inches='tight', pad_inches = 0)
#ax.set_ylim(-1, 32)
plt.show()




ground_truth_map = np.load("/home/thaiduong/ground_truth_map.npy")
cell_pos = np.sum(ground_truth_map>0)
cell_neg = np.sum(ground_truth_map<=0)
cell_all = cell_pos + cell_neg


size_data = np.load(prefix + "fastronsize.npz")
support_size = size_data['support_size']
total_size = size_data['total_size']
gmap_observed_size = size_data['gmap_observed_size']
pose_all = size_data['pose_all']
acc_all = size_data['acc_all']
false_pos_all = size_data['false_pos_all']
false_neg_all = size_data['false_neg_all']
support_vec_seq = size_data['support_vec_seq']
check = acc_all - false_neg_all - false_pos_all
print(np.argmin(check))

pose_all[:, 0] = (pose_all[:, 0] + 2.125)/0.25
pose_all[:, 1] = (pose_all[:, 1] + 12.625)/0.25


f, ax = plt.subplots()
ax.plot(1-acc_all/cell_all, label='acc_all')
ax.plot(1-false_pos_all/cell_neg, label='false_pos_all')
ax.plot(1-false_neg_all/cell_pos, label='false_neg_all')
#ax.plot(check, label='check')
ax.legend()
plt.show()

A = np.loadtxt(prefix + 'fla_warehouse1.cfg')
A = A.reshape(343, 127, 27)
ground_truth_map = A[:, :, 1]
ground_truth_map = np.transpose(ground_truth_map)
remove_interior(ground_truth_map)
ground_truth = np.zeros([ground_truth_map.shape[0] + 1, ground_truth_map.shape[1]])
ground_truth[1:,:] = ground_truth_map[:,:]

#ground_truth = inflate_map(ground_truth)



gmap = np.load(prefix + "gmapping.npy")
gmap=np.transpose(gmap)
gmap = gmap[:,64:320]
gmap[gmap <= 50] = 0
gmap[gmap > 50] = 1
#gmap = gmap[1:,:]


fastronmap = np.load(prefix + "fastronmap.npy")
fastronmap = np.transpose(fastronmap)#fastronmap.reshape([128,256])
fastronmap[fastronmap == 50] = 1
#fastronmap = fastronmap[1:,:]


f, ax = plt.subplots(figsize=(8,4))
#gmap = np.transpose(gmap)
fastronmap_plot = np.copy(fastronmap)
fastronmap_plot[fastronmap_plot==0] = 0.2
ax.imshow(fastronmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,64, 0, 32])
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
plt.savefig(prefix + "/figures/fastron_map.pdf",bbox_inches='tight', pad_inches = 0)
#ax.set_title("our map")
plt.show()

upperboundmap = np.load(prefix + "upperbound_map.npy")
upperboundmap = np.transpose(upperboundmap) #upperboundmap.reshape([128,256])
upperboundmap[upperboundmap == 50] = 1
#upperboundmap = upperboundmap[1:,:]

size = gmap.shape[0] * gmap.shape[1]
diff_fastron_gmap = np.sum(np.abs(fastronmap[:,:] - gmap[:,:]))
diff_upperbound_gmap = np.sum(np.abs(upperboundmap[:,:] - gmap[:,:]))
diff_upperbound_fastron = np.sum(np.abs(upperboundmap[:,:] - fastronmap[:,:]))

print("Size: ", size, "Accuracy vs Gmapping: fastron_map:", np.around(100*(1 - diff_fastron_gmap/size), 2), "% upper bound:",
      np.around(100*(1- diff_upperbound_gmap/size), 2), "%", " uppoer vs fastron:", np.around(100*(1 - diff_upperbound_fastron/size), 2), "%")


max_match = 0
max_match_upper = 0
max_match_gmap = 0
max_truncated_groundtruth = None
max_i = 0
ground_truth[5,:] = ground_truth[4, :]
ground_truth[4, :] = 0
ground_truth[122,:] = ground_truth[124,:]
ground_truth[124,:] = 0

for i in range(ground_truth.shape[1]-256):
    truncated_groundtruth = ground_truth[:, i:i+256]
    diff_fastron = np.sum(np.abs(fastronmap[:, :] - truncated_groundtruth[:, :]))
    diff_gmap = np.sum(np.abs(gmap[:, :] - truncated_groundtruth[:, :]))
    diff_upperbound = np.sum(np.abs(upperboundmap[:, :] - truncated_groundtruth[:, :]))
    accuracy = 1 - diff_fastron/size
    if max_match < accuracy:
        max_match = accuracy
        max_match_upper = 1- diff_upperbound/size
        max_match_gmap = 1 - diff_gmap/size
        max_truncated_groundtruth = truncated_groundtruth
        max_i = i



#print("Size: ", size, "max loc:", i, "Accuracy: fastron_map:", np.around(100*max_match, 3), "% upper bound:", np.around(100*max_match_upper, 3), "% gmap: ", np.around(100*max_match_gmap, 3), "%")

ground_truth_map = ground_truth[:,max_i:max_i+256]
ground_truth_map[5, 20:30] = 0
ground_truth_map[5, 70:80] = 0
ground_truth_map[5, 151:161] = 0
ground_truth_map[5, 189:194] = 0
ground_truth_map[67, 67:70] = 0
ground_truth_map[67, 218:224] = 0

diff_fastron_groundtruth = np.sum(np.abs(fastronmap[:,:] - ground_truth_map[:,:]))
diff_gmap_groundtruth = np.sum(np.abs(gmap[:,:] - ground_truth_map[:,:]))
diff_upperbound_groundtruth = np.sum(np.abs(upperboundmap[:,:] - ground_truth_map[:,:]))
print("Size: ", size, "Accuracy vs Gmapping: fastron_map:", np.around(100*(1 - diff_fastron_gmap/size), 2), "% upper bound:",
      np.around(100*(1- diff_upperbound_gmap/size), 2), "%", " upper vs fastron:", np.around(100*(1 - diff_upperbound_fastron/size), 2), "%",
      "fastron vs ground truth:",  np.around(100*(1 - diff_fastron_groundtruth/size), 2), "%","gmap vs ground truth:",  np.around(100*(1 - diff_gmap_groundtruth/size), 2), "%"
,"diff_upperbound_groundtruth vs ground truth:",  np.around(100*(1 - diff_upperbound_groundtruth/size), 2), "%")

print(1-np.sum(ground_truth_map)/size)
np.save("/home/thaiduong/ground_truth_map.npy", ground_truth_map)

fastronmap_data = np.load(prefix + "fastronmap.npy")
fastronmap_data = np.transpose(fastronmap)#fastronmap.reshape([128,256])
fastronmap_data[fastronmap_data == 50] = 1
diff = np.transpose(fastronmap_data) - ground_truth_map
error = np.sum(np.abs(diff))
false_pos = np.sum(np.abs(diff[diff > 0]))
false_neg = np.sum(np.abs(diff[diff <= 0]))
print("false_pos:", false_pos, "false_neg:", false_neg, "error:", error)

f, ax = plt.subplots(figsize=(8,4))
ax.plot(support_size, 'g',label='number of support vectors', linewidth=2)
#ax.plot(total_size, label='accumulated training set\'s size')
ax.plot(gmap_observed_size, 'r--', label='number of gmapping\'s observed cells', linewidth=2)
ax.set_xlabel("time steps", size='x-large')
ax.set_ylabel("count", size='x-large')
ax.legend(loc = 2, fontsize='large')
plt.savefig(prefix + "/figures/support_vec_count.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()
#print("Loaded size data")


f, ax = plt.subplots(figsize=(8,4))
gmap_plot = gmap.astype(float)
gmap_plot[gmap_plot==0] = 0.2
ax.imshow(gmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,64, 0, 32])
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("gmapping")
plt.savefig(prefix + "/figures/gmap.pdf",bbox_inches='tight', pad_inches = 0)
#plt.show()
#plt.hist(gmap.flatten())
plt.show()


f, ax = plt.subplots(figsize=(8,4))
#gmap = np.transpose(gmap)
upperboundmap_plot = upperboundmap.astype(float)
upperboundmap_plot[upperboundmap_plot==0] = 0.2
ax.imshow(upperboundmap_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,64, 0, 32])
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("our upper bound map")
plt.savefig(prefix + "/figures/fastron_upperbound.pdf",bbox_inches='tight', pad_inches = 0)
print("Loaded gmapp")
plt.show()






f, ax = plt.subplots(figsize=(8,4))
#gmap = np.transpose(gmap)
ground_truth_plot = ground_truth_map.astype(float)
ground_truth_plot[ground_truth_plot==0] = 0.2
ax.imshow(ground_truth_plot, origin='lower', cmap='gray_r', vmin=0, vmax=1, interpolation='none', extent=[0,64, 0, 32])
pose_all = 0.25*pose_all
ax.plot(pose_all[1:len(pose_all)-1, 0], pose_all[1:len(pose_all)-1, 1], color='b', label="robot path")
ax.scatter(pose_all[0, 0], pose_all[0, 1], color='r', s=50, label="start")
ax.scatter(pose_all[-1, 0], pose_all[-1, 1], color='g', s=50, label="goal")
ax.set_xlabel("length(m)", size='x-large')
ax.set_ylabel("width(m)", size='x-large')
#ax.set_title("gmapping")
#ax.set_title("ground truth map")
plt.savefig(prefix + "/figures/groundtruth_map.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()
print("Loaded gmapp")



