import numpy as np

def check_obstacle(x, obs_list):

    for i in range(len(obs_list)):
        if (obs_list[i, 0] < x[0] <obs_list[i, 1]) and (obs_list[i, 2] < x[1] < obs_list[i, 3]):
            return True
    return False


def gen_obstacle_data(obs_list, x_min= [-7, -7], x_max=[7, 7], data_points_count = 400, grid = False, grid_size = [10, 10]):

    if grid:
        x0 = np.linspace(x_min[0], x_max[0], grid_size[0])
        x1 = np.linspace(x_min[1], x_max[1], grid_size[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh = x0mesh.flatten()
        x1mesh = x1mesh.flatten()
        X = np.vstack((x0mesh, x1mesh))
        X = np.transpose(X)

    else:

        x0 = x_min[0] + (x_max[0] - x_min[0]) * np.random.rand(data_points_count, 1)
        x1 = x_min[1] + (x_max[1] - x_min[1]) * np.random.rand(data_points_count, 1)
        X = np.hstack((x0, x1))
        #Y = -np.ones(len(X))

    Y = get_label(X, obs_list)
    return X, Y

def gen_linear_data(a, b, x_min= [-6, -6], x_max=[6, 6], data_points_count = 400, grid = False, grid_size = [10, 10]):

    if grid:
        x0 = np.linspace(x_min[0], x_max[0], grid_size[0])
        x1 = np.linspace(x_min[1], x_max[1], grid_size[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh = x0mesh.flatten()
        x1mesh = x1mesh.flatten()
        X = np.vstack((x0mesh, x1mesh))
        X = np.transpose(X)

    else:

        x0 = x_min[0] + (x_max[0] - x_min[0]) * np.random.rand(data_points_count, 1)
        x1 = x_min[1] + (x_max[1] - x_min[1]) * np.random.rand(data_points_count, 1)
        X = np.hstack((x0, x1))
        #Y = -np.ones(len(X))

    Y = np.array([1 if x[1] <= a*x[0] + b else -1 for x in X])
    return X, Y.reshape(Y.shape[0], 1)

def get_label(X, obs_list):
    Y = np.array([1 if check_obstacle(x, obs_list) else -1 for x in X])
    return Y.reshape(Y.shape[0], 1)
