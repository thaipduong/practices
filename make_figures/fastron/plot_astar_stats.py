import numpy as np
import matplotlib.pyplot as plt

prefix = "/home/thaiduong/fastron_sim_data2/data/"

astar_data = np.load(prefix + "astar.npz")
astar_info = astar_data['astar_info']
astar_time = astar_data['astar_time']
a = astar_info[:,2]
a[a==0.0] = a[a==0] + 1
avg_time = 1000000*astar_info[:,0]/a
avg_time[avg_time > 200] = 200.0

astar_data2 = np.load(prefix + "astar2.npz")
astar_info2 = astar_data2['astar_info2']
astar_time2 = astar_data2['astar_time2']
a = astar_info2[:,2]
a[a==0.0] = a[a==0] + 1
avg_time2 = 1000000*astar_info2[:,0]/a
avg_time2[avg_time2 > 200] = 200.0

astar_data3 = np.load(prefix + "astar3.npz")
astar_info3 = astar_data3['astar_info3']
astar_time3 = astar_data3['astar_time3']
a = astar_info3[:,2]
a[a==0.0] = a[a==0] + 1
avg_time3 = 1000000*astar_info3[:,0]/a
avg_time3[avg_time3 > 200] = 200.0

size_data = np.load(prefix + "fastronsize.npz")
support_size = size_data['support_size']
total_size = size_data['total_size']
pose_all = size_data['pose_all']
support_vec_seq = size_data['support_vec_seq']
support_time = size_data['support_time']


f, ax = plt.subplots(figsize=(7.5,5))
ax.plot(support_time, support_size, 'g',label='number of support vectors', linewidth = 4)
ax.set_xlabel("time(s)", size='x-large')
ax.set_ylabel("count", size='x-large')
ax.legend(facecolor='xkcd:silver',fontsize='large')
#plt.savefig(prefix + "figures/realcar_supportvec_size.pdf",bbox_inches='tight', pad_inches = 0)




#avg_time[avg_time>4.0] = 4.0
f, ax = plt.subplots(figsize=(7.5,5))
ax.plot(astar_time3, avg_time3, 'g',label='A* time using all positive support vecs')
ax.plot(astar_time2, avg_time2, 'r',label='A* time using sampling-based checking')
ax.plot(astar_time, avg_time, 'b',label='A* time using kNN approx')
ax.set_xlabel("time(s)", size='x-large')
ax.set_ylabel("time(us)", size='x-large')
ax.legend(facecolor='xkcd:silver',fontsize='large')
#plt.savefig(prefix + "figures/realcar_astartime_pernode.pdf",bbox_inches='tight', pad_inches = 0)
plt.show()