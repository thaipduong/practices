import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
import kernels


"""
class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max, G = None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)
        print("alpha: \n" + str(alpha))
        if G is None:
            G = kernels.gram_matrix(X, kernel=self.kernel)
        for iter in range(iter_max):
            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)

            for i in ind_list:
                y_predict = np.sign(np.sum(np.multiply(np.multiply(alpha, Y), G[i].reshape([X.shape[0], 1]))))
                # print(y_predict)
                if y_predict == 0.0:
                    y_predict = 1.0
                # print(y_predict)
                # print(Y[i][0])
                if y_predict != Y[i][0]:
                    alpha[i][0] += 1.0

        return alpha

    def predict(self, X, Y, alpha, X1, X2):
        y_predict = np.zeros([X2.shape[0], X1.shape[0]])
        for j in range(X1.shape[0]):
            for k in range(X2.shape[0]):
                x_test = np.array([X1[j], X2[k]])
                G_row = np.zeros([X.shape[0], 1])

                for i in range(X.shape[0]):
                    G_row[i] = self.kernel(X[i, :], x_test)
                G_row.reshape([X.shape[0], 1])
                y_predict[j][k] = np.sum(np.multiply(np.multiply(alpha, Y), G_row))

        return y_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha):
        x1 = np.linspace(-6, 6)
        x2 = np.linspace(-6, 6)
        # plot_data(X,Y)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]
        ax.scatter(X[:, 0], X[:, 1], color=colors)
        ax.contour(x1, x2,
                   self.predict_y(X, Y, alpha, x1, x2), levels=[0], cmap="Greys_r")
        plt.show()
"""


class Fastron(object):
    def __init__(self, kernel = kernels.rbf_kernel, update_argminF=False, remove_redundant=False):
        self.kernel = kernel
        self.update_argminF = update_argminF
        self.remove_redundant = remove_redundant


    def train(self, X, Y, iter_max=100, G=None, alpha=None, F=None, local_neighbors = None):
        if alpha is None:
            alpha = np.zeros([X.shape[0], 1], dtype=np.float64)

        previous_F = True
        if G is None:
            G = kernels.gram_matrix(X, kernel=self.kernel)
        if F is None:
            F = np.matmul(G, alpha)
            previous_F = False

        print("iter_max = " + str(iter_max))

        for iter in range(iter_max):
            r_plus = 1.5
            r_minus = 1
            print("local_neighbors: ", local_neighbors)
            if local_neighbors is not None:
                if self.update_argminF and previous_F:

                    local_F = F[local_neighbors]
                    local_Y = Y[local_neighbors]
                    print("local_F", local_F)
                    print("local_Y", local_Y)
                    print("local_F*local_Y", local_F*local_Y)
                    local_min_idx = np.argmin(local_F*local_Y)
                    ind_list = [local_neighbors[local_min_idx]]
                    print("fastron update min: ", ind_list)
                else:
                    ind_list = local_neighbors
                print("fastron update: ", ind_list)
            else:
                if self.update_argminF and previous_F:
                    ind_list = [np.argmin(F * Y)]
                else:
                    ind_list = [i for i in range(X.shape[0])]

            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i][0])
                if y_predict == 0.0:
                    y_predict = -1.0
                print("iter", iter, "i", i, "y_predict", y_predict, Y[i][0])
                if y_predict != Y[i][0]:

                    correct_prediction = False
                    r = r_plus if Y[i][0] == 1 else r_minus
                    delta_alpha = r * Y[i][0] - y_predict
                    alpha[i][0] += delta_alpha
                    F = F + delta_alpha * (G[:, i].reshape([X.shape[0], 1]))
                    #print("i = " + str(i))

            # Update support points
            if self.remove_redundant:
                margin = Y * (F - alpha) * np.int64(alpha != 0)

                for m in range(len(margin)):
                    temp = margin[m]
                    print(type(temp))
                    if margin[m][0] > 0:
                        #print("Remove m = " + str(m) + ", margin = " + str(margin[m]) + ", alpha = " + str(
                        #    alpha[m][0]) + ", F = " + str(F[m][0]) + ", Y = " + str(Y[m][0]))
                        F = F - alpha[m][0] * (G[:, m].reshape([X.shape[0], 1]))
                        alpha[m][0] = 0
            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break
        #Print out support points
        margin = Y * (F - alpha) * np.int64(alpha != 0)
        for m in range(len(alpha)):
            if alpha[m][0] != 0:
                print("Support points m = " + str(m) + ", margin = " + str(margin[m]) + ", alpha = " + str(alpha[m][0]) + ", F = " + str(F[m][0]) + ", X = " + str(X[m, :]) + ", Y = " + str(Y[m][0]))

        return alpha, F

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test)
            f_predict[i][0] = np.sum(np.multiply(alpha, G_row))

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data = False):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        colors = ['red' if l == -1. else 'green' for l in Y]
        if show_data:
            s = [0 if a == 0 else 60 for a in alpha]
            ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
            ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="cool_r", linewidths=3)
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")


    def predict_probability(self, X, Y, alpha, X_test, scale=1):
        f_predict_plus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        f_predict_minus = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test)

            f_vec = np.multiply(alpha, G_row)
            f_predict_plus[i][0] = np.sum(np.multiply(f_vec, np.int64(alpha > 0)))
            f_predict_minus[i][0] = -np.sum(np.multiply(f_vec, np.int64(alpha < 0)))
        print(sum(np.int64(alpha > 0 )))
        print(sum(np.int64(alpha < 0)))
        f_predict = 1/ (1 + scale*f_predict_minus / f_predict_plus)
        print(f_predict.max())
        print(f_predict.min())
        return f_predict

    # Plotting decision boundary
    def plot_probability(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6], fig=None, ax=None, show_data = False):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        if fig is None or ax is None:
            fig, ax = plt.subplots()

        colors = ['red' if l == -1. else 'green' for l in Y]
        if show_data:
            s = [0 if a == 0 else 60 for a in alpha]
            ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
            ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        f_predict = self.predict_probability(X, Y, alpha, X_grid)
        #print(f_predict)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), cmap="Greys_r")
        #ax.set_title("Fastron (" + str(self.kernel.__name__) + ")")




class KernelPerceptron(object):
    def __init__(self, kernel = kernels.rbf_kernel):
        self.kernel = kernel

    def train(self, X, Y, iter_max=100, G=None, alpha=None, F=None):
        alpha = np.zeros([X.shape[0], 1], dtype=np.float64)

        if G is None:
            G = kernels.gram_matrix(X, kernel = self.kernel)
        if F is None:
            F = np.matmul(G, alpha)

        F = np.matmul(G, alpha)
        for iter in range(iter_max):

            ind_list = [i for i in range(X.shape[0])]
            shuffle(ind_list)
            correct_prediction = True
            for i in ind_list:
                y_predict = np.sign(F[i][0])
                if y_predict == 0.0:
                    y_predict = 1.0

                if y_predict != Y[i][0]:
                    correct_prediction = False
                    alpha[i][0] += Y[i][0]
                    F = F + Y[i][0] * (G[:, i].reshape([X.shape[0], 1]))

            if correct_prediction:
                print("Finished at iter = " + str(iter))
                break

        return alpha, F

    def predict(self, X, Y, alpha, X_test):
        f_predict = np.zeros([X_test.shape[0], 1], dtype=np.float64)
        for i in range(X_test.shape[0]):
            x_test = X_test[i,:]
            G_row = np.zeros([X.shape[0], 1])
            for j in range(X.shape[0]):
                G_row[j] = self.kernel(X[j, :], x_test)
            f_predict[i][0] = np.sum(np.multiply(alpha, G_row))

        y_predict = np.sign(f_predict)
        return y_predict, f_predict

    # Plotting decision boundary
    def plot_boundary(self, X, Y, alpha, x_min= [-6, -6], x_max=[6, 6]):
        x0 = np.linspace(x_min[0], x_max[0])
        x1 = np.linspace(x_min[1], x_max[1])
        x0mesh, x1mesh = np.meshgrid(x0, x1)
        x0mesh_flattened = x0mesh.flatten()
        x1mesh_flattened = x1mesh.flatten()
        X_grid = np.vstack((x0mesh_flattened, x1mesh_flattened))
        X_grid = np.transpose(X_grid)
        f, ax = plt.subplots()
        colors = ['red' if l == -1. else 'green' for l in Y]

        s = [0 if a == 0 else 60 for a in alpha]
        ax.scatter(X[:, 0], X[:, 1], color='b', s=s)
        ax.scatter(X[:, 0], X[:, 1], color=colors, s=20)
        y_predict, f_predict = self.predict(X, Y, alpha, X_grid)
        ax.contour(x0mesh, x1mesh,
                   f_predict.reshape(x0mesh.shape), levels=[0], cmap="Greys_r")
        ax.set_title("Kernel Perceptron (" + str(self.kernel.__name__) + ")")
        plt.show()







